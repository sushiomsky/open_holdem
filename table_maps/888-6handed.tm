.osdb2

// OpenScrape 2.0.0

// 2014-11-12 05:37:28
// 32 bits per pixel

//
// sizes
//

z$clientsize       700  478

//
// strings
//

s$activemethod              2
s$buttonclickmethod         Single
s$handresetmethod           1
s$nchairs                   6
s$network                   pacific
s$potmethod                 2
s$sitename                  888
s$swagconfirmationmethod    Enter
s$swagdeletionmethod        Backspace
s$swagselectionmethod       Click Drag
s$swagtextmethod            3
s$t2type                    0.15
s$t3type                    0.25
s$titletext                 /
s$ttlimits                  ^* ^LH ^v/^V

//
// regions
//

r$c0cardface0        233 135 245 165        0    0 H0
r$c0cardface1        281 135 293 165        0    0 H0
r$c0cardface2        329 135 341 165        0    0 H0
r$c0cardface3        376 135 388 165        0    0 H0
r$c0cardface4        424 135 436 165        0    0 H0
r$c0handnumber       106 423 166 434   6912a6    0 T1
r$c0pot0             309 122 426 133   ffffff  200 T3
r$chatbox             10 454  29 465        0    0 N
r$i0button           294 388 297 391        0    0 N
r$i0label            335 386 370 400   d68205    0 T0
r$i0state            412 386 412 386   ffffff    0 C
r$i1button           431 388 434 391        0    0 N
r$i1label            431 386 540 400   444444 -120 T0
r$i1state            549 387 549 387   ffffff    0 C
r$i2button           569 388 572 391        0    0 N
r$i2label            569 386 678 400   444444 -120 T0
r$i2state            690 385 690 385   ffffff    0 C
r$i3edit             632 438 686 448        0    0 N
r$i3state            692 433 692 433        0    0 C
r$i4button           427 461 429 463        0    0 N
r$i4label            427 464 428 464   2ebd66    0 H0
r$i4state            427 463 427 463   ffffff    0 C
r$i5button           291 462 293 464        0    0 N
r$i5label            290 464 290 464   2ebd66    0 H0
r$i5state            289 460 289 460   ffffff    0 C
r$i6button           294 389 294 389        0    0 N
r$i6label            309 386 351 398        0    0 H0
r$i6state            312 388 312 388   ebae1b    0 C
r$p0active           469  58 488  68   717171    0 T0
r$p0balance          419  57 511  69    1dcfc    0 T0
r$p0bet              410 107 499 118   ffffff  200 T3
r$p0cardback         455  34 466  34        0    0 H0
r$p0cardface0rank    404   2 415  18        0  300 T2
r$p0cardface0suit    404  20 415  31        0  300 T2
r$p0cardface1rank    448   2 459  18        0  300 T2
r$p0cardface1suit    448  20 459  31        0  300 T2
r$p0dealer           501  87 504  90        0    0 H0
r$p0name             419  42 511  55   ffffff    0 T0
r$p0seated           474  33 475  33   b07f4f  -50 C
r$p1active           649 186 668 196   717171    0 T0
r$p1balance          598 185 690 197    1dcfc    0 T0
r$p1bet              464 196 553 207   ffffff  200 T3
r$p1cardback         635 162 646 162        0    0 H0
r$p1cardface0rank    584 129 595 145        0  300 T2
r$p1cardface0suit    584 147 595 158        0  300 T2
r$p1cardface1rank    628 129 639 145        0  300 T2
r$p1cardface1suit    628 147 639 158        0  300 T2
r$p1dealer           537 144 540 147        0    0 H0
r$p1name             598 169 690 182   ffffff    0 T0
r$p1seated           636 211 648 211   693603  -70 C
r$p2active           469 340 488 350   717171    0 T0
r$p2balance          419 339 511 351    1dcfc    0 T0
r$p2bet              409 270 498 281   ffffff  200 T3
r$p2cardback         455 316 466 316        0    0 H0
r$p2cardface0rank    404 283 415 299        0  300 T2
r$p2cardface0suit    404 301 415 312        0  300 T2
r$p2cardface1rank    448 283 459 299        0  300 T2
r$p2cardface1suit    448 301 459 312        0  300 T2
r$p2dealer           501 263 504 266        0    0 H0
r$p2name             419 323 511 336   ffffff    0 T0
r$p2seated           446 361 457 361   775d38  -40 C
r$p3active           258 340 277 350   717171    0 T0
r$p3balance          207 339 299 351    1dcfc    0 T0
r$p3bet              194 270 283 281   ffffff  200 T3
r$p3cardback         244 316 255 316        0    0 H0
r$p3cardface0rank    193 283 204 299        0  300 T2
r$p3cardface0suit    193 301 204 312        0  300 T2
r$p3cardface1rank    237 283 248 299        0  300 T2
r$p3cardface1suit    237 301 248 312        0  300 T2
r$p3dealer           289 263 292 266        0    0 H0
r$p3name             207 323 299 336   ffffff    0 T0
r$p3seated           235 361 246 361   755c38  -40 C
r$p4active            94 186 113 196   717171    0 T0
r$p4balance           44 185 136 197    1dcfc    0 T0
r$p4bet              144 196 233 207   ffffff  200 T3
r$p4cardback          80 162  91 162        0    0 H0
r$p4cardface0rank     29 129  40 145        0  300 T2
r$p4cardface0suit     29 147  40 158        0  300 T2
r$p4cardface1rank     73 129  84 145        0  300 T2
r$p4cardface1suit     73 147  84 158        0  300 T2
r$p4dealer           155 144 158 147        0    0 H0
r$p4name              44 169 136 182   ffffff    0 T0
r$p4seated            53 211  64 211   5a3003  -50 C
r$p5active           258  58 277  68   717171    0 T0
r$p5balance          208  56 300  68    1dcfc    0 T0
r$p5bet              194 107 283 118   ffffff  200 T3
r$p5cardback         244  34 255  34        0    0 H0
r$p5cardface0rank    193   2 204  18        0  300 T2
r$p5cardface0suit    193  20 204  31        0  300 T2
r$p5cardface1rank    237   2 248  18        0  300 T2
r$p5cardface1suit    237  20 248  31        0  300 T2
r$p5dealer           289  87 292  90        0    0 H0
r$p5name             208  43 300  56   ffffff    0 T0
r$p5seated           263  33 264  33   b07f4f  -50 C

//
// fonts
//

t0$- 1 1 1 1
t0$_ 1 1 1 1 1 1
t0$J 1 81 81 fe
t0$4 18 28 48 ff 8
t0$4 18 38 68 ff ff 8
t0$s 19 29 25 26
t0$s 19 39 2d 27 26
t0$s 19 3d 2d 2f 26
t0$c 1e 21 21 21
t0$o 1e 21 21 21 1e
t0$d 1e 21 21 21 1ff
t0$e 1e 29 29 29 1a
t0$d 1e 3f 21 21 1ff 1ff
t0$c 1e 3f 21 21 21
t0$o 1e 3f 21 21 3f 1e
t0$e 1e 3f 29 29 3b 1a
t0$l 1ff
t0$l 1ff 1ff
t0$k 1ff 1ff 1c 36 23 1
t0$h 1ff 1ff 20 20 3f 1f
t0$h 1ff 20 20 20 1f
t0$b 1ff 21 21 21 1e
t0$k 1ff 8 14 22 1
t0$x 21 12 c 12 21
t0$z 23 25 29 31
t0$5 2 f3 f1 91 9f 8e
t0$. 3
t0$v 30 c 3 c 30
t0$. 3 3
t0$w 3c 3 c 30 c 3 3c
t0$O 3c 42 81 81 81 42 3c
t0$C 3c 42 81 81 81 81
t0$G 3c 42 81 89 89 8f
t0$C 3c 7e c3 81 81 81 81
t0$u 3e 1 1 1 3f
t0$u 3e 3f 1 1 3f 3f
t0$6 3e 51 91 91 e
t0$6 3e 7f d1 91 9f e
t0$r 3f 10 20
t0$m 3f 20 20 1f 20 20 1f
t0$n 3f 20 20 20 1f
t0$n 3f 3f 20 20 3f 1f
t0$1 41 ff 1
t0$1 41 ff ff 1
t0$3 42 81 91 91 6e
t0$3 42 c3 91 91 ff 6e
t0$2 43 85 89 91 61
t0$2 43 c7 8d 99 f1 61
t0$S 61 91 91 91 8e
t0$a 6 29 29 29 1f
t0$a 6 2f 29 29 3f 1f
t0$8 6e 91 91 91 6e
t0$8 6e ff 91 91 ff 6e
t0$9 70 89 89 8a 7c
t0$9 70 f9 89 89 ff 7e
t0$9 70 f9 89 8b fe 7c
t0$A 7 3c c4 c4 3c 7
t0$q 78 84 84 84 ff
t0$g 78 85 85 85 fe
t0$� 78 fc 3ff 3ff 84 84
t0$� 78 fc 84 3ff 84
t0$g 78 fd 85 85 ff fe
t0$0 7e 81 81 81 7e
t0$0 7e ff 81 81 ff 7e
t0$6 7e ff 91 91 9f e
t0$7 80 80 87 bf f8 c0
t0$T 80 80 ff 80 80
t0$T 80 80 ff ff 80 80
t0$7 80 83 8c b0 c0
t0$7 80 83 8f bc f0 c0
t0$j 81 2fe
t0$I 81 ff 81
t0$Z 83 85 99 a1 c1
t0$i bf
t0$i bf bf
t0$Y c0 30 f 30 c0
t0$y c0 33 c 30 c0
t0$4 c 14 24 7f ff 4
t0$X c3 24 18 24 c3
t0$$ c4 1e4 7ff 7ff 13c 118
t0$$ c8 1e4 13f 7e4 13c 98
t0$V e0 1c 3 1c e0
t0$W e0 1c 3 1c e0 1c 3 1c e0
t0$Q f0 108 204 204 206 109 f1
t0$5 f2 91 91 91 8e
t0$5 f2 f3 91 91 9f 8e
t0$, f e
t0$U fe 1 1 1 1 fe
t0$t fe 21 21
t0$t fe ff 21 21
t0$H ff 10 10 10 10 ff
t0$L ff 1 1 1
t0$f ff 120 120
t0$K ff 18 24 42 81
t0$D ff 81 81 81 42 3c
t0$p ff 84 84 84 78
t0$P ff 88 88 88 70
t0$R ff 88 88 8c 72 1
t0$F ff 90 90 90 90
t0$B ff 91 91 91 6e
t0$E ff 91 91 91 81
t0$M ff c0 30 c 30 c0 ff
t0$N ff c0 30 c 3 ff
t0$R ff ff 88 8c fe 73 1
t0$F ff ff 90 90 90 90
t0$B ff ff 91 91 ff 6e
t2$d 10 38 7c fe 1ff fe 7c 38
t2$d 10 78 fe 1ff 1ff fc 38 10
t2$5 10c 7f8e 7f07 6303 6387 61fe 78
t2$A 1 1f 1ff 1ffc 7fb8 7c38 7fb8 1ffc 1ff 1f 1
t2$2 1807 781f 783f 60ff 73f3 7fc3 1f83
t2$h 1e0 3f8 1fc 1fe ff 1fc 3f8 1f0 c0
t2$9 1fce 7fef 6063 6063 3ffe 1ffc
t2$Q 1ff0 7ffc f83e f01e e06e e07e f83e 7ffe 1ff7 2
t2$6 1ffc 39ce 6303 6303 39fe 18fc
t2$d 20 70 f8 3fc 7ff 3fe f8 70 20
t2$3 380e 7807 6183 7383 3ffe 1c7c
t2$d 38 7c fe 3ff fe 7c 38
t2$4 38 f8 7f8 1f98 7fff 7fff 7fff 18
t2$J 3c 3e 7 7 7fff 7ffe
t2$s 3c 7e 1fe 3fc 3ff 3ff 3fc 1fe fe 3c
t2$s 3c fe 1fc 3fc 3fb 3f9 1fc fe 7c 18
t2$J 3e 3f f 7 7fff 7ffe 7ff8
t2$8 3e7e 7fef 6183 6183 3ffe 1e7c
t2$7 6000 6000 607f 63ff 7fe0 7e00 7800
t2$7 6000 603f 61ff 6fc0 7c00 7000
t2$h 60 f8 1fc fe 7f 7f fe 1fc f8
t2$d 70 f8 1fc 7ff 3fe 1fc 70 20
t2$4 78 1d8 f18 3e18 7fff 18 18
t2$5 78c 7f8f 7f87 6303 63cf 63fe 60fc
t2$Q 7f0 1ffc 380e 7007 7017 701f 381e 3fff ff3
t2$6 7f0 3ffe 7fff 6303 7387 7bff 39fe
t2$K 7fff 7fff 3e0 fc0 1ff0 7dfe 783f 600f 4001
t2$c 8 1e 3e 3e 1fc 3ff 3fc 1fe 3e 3e c
t2$8 838 3ffe 7fff 6183 73c7 7fff 3e7e
t2$s 8 7c fe 1fc 3f8 3fb 3fc 1fc fe 3c 8
t2$h c0 1f0 3f8 1fc ff 1fe 1fc 3f8 1f0
t2$h c0 1f0 3f8 3fc 1fe ff 1fe 3fc 3f8 1e0
t2$c c 1e 3e 1fe 3fc 3ff 1fc be 3e 1e 8
t2$3 c 380e 780f 6183 7fc7 7fff 1e7e
t2$T e00 1c00 7fff
t2$9 f00 3fce 7fef 6063 7067 7fff 1ffc 1c0
t2$T f00 3fff 7fff 7fff
t2$h f8 1fc fe 7f 7f fe 1fc f8 60
t2$Q ff8 3ffe 7c1f 780f 7037 703f 7c1f 3fff ffb 1
t2$A f ff ff8 7f38 7838 7f38 ff8 ff f
t3$4 18 28 48 c8 ff 8
t3$2 1 83 85 89 f1 1
t3$4 18 38 68 c8 ff 8
t3$, 1 e 8
t3$, 1 f 8
t3$$ 2 79 49 49 4f 6
t3$$ 2 79 59 49 4f 6
t3$3 2 81 91 91 ee
t3$3 2 81 91 91 ee c
t3$. 3
t3$6 3e 53 91 91 8e
t3$1 41 41 ff 1
t3$1 41 7f ff 1
t3$1 41 ff 1 1
t3$8 6 6e 91 91 99 6e
t3$8 6e 91 91 99 6e
t3$, 7
t3$9 70 89 89 8a 7c
t3$9 70 89 89 8a 7e
t3$9 70 89 89 8b 7e
t3$$ 71 49 49 4f
t3$$ 71 49 49 4f 6
t3$, 7 4
t3$� 78 84 3ff 84
t3$0 7e 81 81 c3 7e
t3$7 80 81 87 9c f0 c0
t3$7 81 86 98 e0 80
t3$7 81 86 98 e0 c0
t3$3 81 91 91 ee
t3$$ 8 1e4 124 125 13c 18
t3$2 83 85 89 71
t3$4 8 8 48 c8 ff 8
t3$5 f1 91 91 9e
t3$5 f1 91 91 9e c

//
// points
//

p1$  18    7
p1$  18   16
p1$  18   31
p1$  18   40
p1$  25   11
p1$  25   35
p1$  33    7
p1$  33   16
p1$  33   31
p1$  33   40

//
// hash
//

h0$Ac                 056cdaf0
h0$Qd                 0591e01b
h0$8h                 12fa88ca
h0$6d                 15b1f67a
h0$9c                 17a3565f
h0$sitin              18116a0f
h0$Kh                 20b21c0b
h0$6c                 238dfb27
h0$3h                 323d3fad
h0$Ts                 325d9876
h0$prefold            33653f0e
h0$Ad                 3e0c7899
h0$8c                 3ff8de34
h0$4d                 4500f2fe
h0$5h                 4a1e45b8
h0$5s                 4a7c7fbf
h0$dealer             51f28192
h0$3c                 5f67a703
h0$5c                 6133a9cf
h0$9d                 6684d508
h0$3s                 67122af6
h0$2d                 6a1b37f6
h0$Kc                 6b97ab26
h0$autopost           6f87910c
h0$7h                 7213708c
h0$6s                 7abfd8da
h0$Td                 844e6311
h0$Ah                 8aa4593f
h0$Jh                 8b099a80
h0$Js                 8c022237
h0$Ks                 8d407888
h0$Td                 8eb56251
h0$Jd                 931ad17f
h0$2s                 96d5d019
h0$4h                 99e0df8a
h0$7s                 9c65fbbd
h0$7d                 aa0c1149
h0$8s                 ba8f2f86
h0$As                 c55a1dbb
h0$9h                 ca8fd3c5
h0$3d                 cc4c70b7
h0$dealer2            cfc1d629
h0$8d                 d0c14694
h0$Kd                 d136c38a
h0$2c                 d29241db
h0$2h                 d4761293
h0$Th                 d90c15d5
h0$Qh                 db2b1e55
h0$Tc                 dc012d8e
h0$Qs                 dca8adb5
h0$Jc                 e51e1a1f
h0$cardback           e6f3b1b7
h0$sitout             e8f59493
h0$Qc                 ed51972e
h0$7c                 f3bd2c36
h0$4c                 f6266f8a
h0$4s                 f7e19666
h0$5d                 fab35c8e
h0$9s                 fd686c8d
h0$6h                 ffa4ee80
h1$Td                 50c3dd1c
h1$Td                 f1d21657

//
// images
//

i$3d               13  31 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ddddff003d3dff000000ff001414ff00acacfe00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00fbfbff003d3dff000101ff000000ff000000ff000000ff00cbcbff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00b9b9fe000000ff000a0aff00eeeefd005c5cff000404ff005c5cff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00cbcbff003d3dff003d3dff00ffffff00acacfe000101ff003d3dff00fdfdff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00ffffff009b9bfe000000ff003d3dff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00ffffff003d3dff000000ff008888fe00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff003d3dff000000ff003d3dff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00fdfdff002525ff000000ff002525ff00cbcbff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00fdfdff009b9bfe000000ff002525ff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff001414ff000000ff00cbcbff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff001414ff000000ff00cbcbff00ffffff00ffffff00ffffff00c9c9ff00
ffffff008888fe000000ff008888fe00ffffff00ffffff001414ff000000ff00ddddff00ffffff00ffffff00ffffff00c9c9ff00
ffffff009b9bfe000000ff002525ff00eeeefd009b9bfe000000ff002525ff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff002525ff000000ff000000ff000000ff000000ff009b9bfe00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ddddff003d3dff000101ff001414ff009b9bfe00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00ffffff00ffffff005c5cff00eaeaff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00ffffff00acacfe000404ff007474fe00fbfbff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00dadaff000000ff000000ff000000ff009b9bfe00ffffff00ffffff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00f5f5ff002525ff000000ff000000ff000000ff000000ff00d8d8fd00ffffff00ffffff00ffffff00c5c5fc00
ffffff00fdfdff003d3dff000101ff000101ff000000ff000000ff000000ff000a0aff00eaeaff00ffffff00fdfdff00c5c5fc00
ffffff00b9b9fe000404ff000101ff000000ff000000ff000000ff000000ff000000ff005c5cff00ffffff00fdfdff00c5c5fc00
ffffff00fbfbff005c5cff000000ff000000ff000000ff000000ff000000ff001414ff00eeeefd00ffffff00fdfdff00c5c5fc00
ffffff00fdfdff00f3f3ff003d3dff000000ff000000ff000000ff000404ff00e2e2fe00ffffff00ffffff00fdfdff00c5c5fc00
ffffff00ffffff00ffffff00e6e6ff000404ff000000ff000000ff00acacfe00ffffff00ffffff00ffffff00fdfdff00c5c5fc00
ffffff00ffffff00ffffff00ffffff00b9b9fe000101ff007474fe00ffffff00ffffff00ffffff00ffffff00fdfdff00c5c5fc00
ffffff00ffffff00ffffff00ffffff00ffffff007474fe00eaeaff00ffffff00ffffff00ffffff00ffffff00fdfdff00c5c5fc00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fdfdff00c5c5fc00
i$Jd               13  31 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00fdfdff005c5cff000000ff008888fe00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00fdfdff005c5cff000000ff008888fe00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00ffffff005c5cff000000ff008888fe00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00fdfdff005c5cff000000ff008888fe00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00fbfbff005c5cff000000ff008888fe00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00fdfdff005c5cff000000ff008888fe00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00ffffff005c5cff000000ff008888fe00ffffff00ffffff00ffffff00fdfdff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00fdfdff005c5cff000000ff008888fe00ffffff00ffffff00ffffff00fdfdff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00fbfbff005c5cff000000ff008888fe00ffffff00ffffff00ffffff00fbfbff00c7c7fd00
eeeefd000000ff001414ff00ffffff00fdfdff005c5cff000000ff008888fe00ffffff00ffffff00ffffff00fbfbff00c7c7fd00
eeeefd000000ff000000ff00ddddff00ffffff005c5cff000000ff008888fe00ffffff00ffffff00ffffff00fbfbff00c7c7fd00
ffffff001414ff000000ff008888fe00eaeaff002525ff000000ff00acacfe00ffffff00ffffff00ffffff00fbfbff00c7c7fd00
fbfbff005c5cff000000ff000000ff000000ff000000ff000000ff00ddddff00ffffff00ffffff00ffffff00f9f9ff00c7c7fd00
ffffff00acacfe000000ff000000ff000000ff000000ff005c5cff00ffffff00ffffff00ffffff00ffffff00f9f9ff00c5c5fc00
ffffff00fdfdff008888fe000000ff000000ff003d3dff00ffffff00ffffff00ffffff00ffffff00ffffff00f9f9ff00c5c5fc00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00f9f9ff00c5c5fc00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00f7f7ff00c5c5fc00
ffffff00ffffff00ffffff00f1f1ff005c5cff00fbfbff00ffffff00ffffff00ffffff00ffffff00ffffff00f7f7fd00c5c5fc00
ffffff00ffffff00ffffff007474fe000000ff00acacfe00ffffff00ffffff00ffffff00ffffff00ffffff00f7f7fd00c5c5fc00
ffffff00ffffff00acacfe000000ff000000ff000000ff00d4d4fe00ffffff00ffffff00ffffff00ffffff00f7f7fd00c5c5fc00
ffffff00dadaff000101ff000000ff000000ff000000ff002525ff00f1f1ff00ffffff00ffffff00ffffff00f5f5fd00c2c2fa00
eeeefd000a0aff000000ff000000ff000000ff000000ff000000ff003d3dff00f9f9ff00ffffff00ffffff00f5f5fd00c2c2fa00
7474fe000404ff000000ff000000ff000000ff000000ff000000ff000000ff00acacfe00ffffff00ffffff00f5f5fd00c2c2fa00
f1f1ff001414ff000000ff000000ff000000ff000000ff000000ff003d3dff00ffffff00ffffff00ffffff00f3f3fd00c2c2fa00
ffffff00e6e6ff000a0aff000000ff000000ff000404ff002525ff00f9f9ff00ffffff00ffffff00ffffff00f3f3fd00c2c2fa00
ffffff00ffffff00b9b9fe000000ff000000ff000404ff00ddddff00ffffff00ffffff00ffffff00ffffff00f3f3fd00c2c2fa00
ffffff00ffffff00ffffff008888fe000000ff00b9b9fe00ffffff00ffffff00ffffff00ffffff00ffffff00f1f1fd00c2c2fa00
ffffff00ffffff00fdfdff00f1f1ff007474fe00f7f7ff00ffffff00ffffff00ffffff00ffffff00ffffff00f1f1fd00c2c2fa00
ffffff00ffffff00ffffff00ffffff00fdfdff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00f1f1fa00c2c2fa00
i$Th               13  31 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ddddff000000ff008888fe00ffffff00ffffff008888fe000000ff003d3dff00eaeaff00ffffff00ffffff00c9c9ff00
ffffff009b9bfe000000ff008888fe00ffffff00eeeefd001414ff000000ff000000ff007474fe00ffffff00ffffff00c9c9ff00
fbfbff003d3dff000000ff008888fe00ffffff009b9bfe000000ff00acacfe003d3dff002525ff00ffffff00ffffff00c9c9ff00
5c5cff000000ff000000ff008888fe00ffffff005c5cff001414ff00ffffff008888fe000000ff00ddddff00ffffff00c9c9ff00
0404ff001414ff000000ff008888fe00ffffff005c5cff001414ff00ffffff009b9bfe000000ff00b9b9fe00ffffff00c9c9ff00
0000ff007474fe000000ff008888fe00fbfbff003d3dff002525ff00ffffff00acacfe000000ff00acacfe00ffffff00c9c9ff00
8888fe00acacfe000000ff008888fe00ffffff002525ff003d3dff00ffffff00acacfe000000ff00acacfe00ffffff00c9c9ff00
ffffff00acacfe000000ff008888fe00ffffff003d3dff002525ff00ffffff00acacfe000000ff00acacfe00ffffff00c9c9ff00
ffffff00acacfe000000ff008888fe00ffffff003d3dff003d3dff00fbfbff00acacfe000000ff00acacfe00ffffff00c9c9ff00
ffffff00acacfe000000ff008888fe00ffffff002525ff002525ff00ffffff00acacfe000000ff00acacfe00ffffff00c9c9ff00
ffffff00acacfe000000ff008888fe00ffffff005c5cff002525ff00ffffff009b9bfe000000ff00b9b9fe00ffffff00c9c9ff00
ffffff00acacfe000000ff008888fe00ffffff005c5cff001414ff00ffffff008888fe000000ff00ddddff00ffffff00c9c9ff00
ffffff00acacfe000000ff008888fe00ffffff009b9bfe000000ff00acacfe005c5cff002525ff00ffffff00ffffff00c9c9ff00
ffffff00acacfe000000ff008888fe00ffffff00eeeefd001414ff000000ff000000ff007474fe00ffffff00ffffff00c9c9ff00
ffffff00acacfe000000ff008888fe00ffffff00ffffff008888fe000000ff003d3dff00eeeefd00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fbfbff00fdfdff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00f7f7ff008888fe005c5cff00c9c9ff00ffffff00ddddff007474fe007474fe00e6e6ff00ffffff00ffffff00c7c7fd00
ffffff007474fe000000ff000000ff001414ff00c5c5fc003d3dff000000ff000000ff003d3dff00f7f7ff00ffffff00c7c7fd00
ffffff001414ff000101ff000000ff000000ff001414ff000000ff000000ff000000ff000404ff00c2c2fa00ffffff00c7c7fd00
ffffff001414ff000000ff000000ff000000ff000101ff000000ff000000ff000000ff000000ff00b9b9fe00ffffff00c7c7fd00
fbfbff005c5cff000000ff000000ff000000ff000000ff000000ff000000ff000000ff002525ff00eaeaff00ffffff00c7c7fd00
ffffff00c2c2fa000101ff000000ff000000ff000000ff000000ff000000ff000000ff009b9bfe00ffffff00ffffff00c7c7fd00
ffffff00ffffff009b9bfe000000ff000000ff000000ff000000ff000000ff007474fe00fbfbff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff007474fe000404ff000000ff000000ff005c5cff00f5f5ff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00ffffff005c5cff000000ff003d3dff00f1f1ff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00ffffff00f3f3ff003d3dff00d4d4fe00ffffff00ffffff00ffffff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00ffffff00ffffff00ffffff00d4d4fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c5c5fc00
i$7h               38  55 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00c9c9ff00c9c9ff00c9c9ff00c9c9ff00c9c9ff00c9c9ff00c9c9ff00c9c9ff00c9c9ff00c9c9ff00c9c9ff00c9c9ff00c9c9ff00c9c9ff00c9c9ff00c9c9ff00c9c9ff00c9c9ff00c9c9ff00c9c9ff00c9c9ff00c9c9ff00c9c9ff00c9c9ff00ffffff00
ffffff008888fe000000ff000000ff000000ff000000ff000000ff000000ff00cbcbff00ffffff00ffffff00ffffff00c9c9ff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00ffffff00
ffffff008888fe000000ff000000ff000000ff000000ff000000ff000000ff00cbcbff00ffffff00ffffff00ffffff00c9c9ff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00acacfe000000ff002525ff00ffffff00ffffff00ffffff00ffffff00c9c9ff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff003d3dff000000ff008888fe00ffffff00ffffff00ffffff00ffffff00c9c9ff00b9b9fe000101ff000000ff007474fe00acacfe000000ff000000ff009b9bfe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00b9b9fe000404ff000000ff007474fe00acacfe000000ff000000ff008888fe00cbcbff00ffffff00
ffffff00ffffff00ffffff00ffffff00c9c9ff000000ff002525ff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff005c5cff000404ff000000ff000000ff000101ff000000ff000000ff003d3dff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff005c5cff000404ff000000ff000000ff000404ff000000ff000101ff003d3dff00cbcbff00ffffff00
ffffff00ffffff00ffffff00ffffff007474fe000000ff007474fe00ffffff00ffffff00ffffff00ffffff00ffffff00cbcbff009b9bfe000000ff000000ff000000ff000000ff000000ff000404ff007474fe00fdfdff00fdfdff00fdfdff00fdfdff00fdfdff00fdfdff00ffffff009b9bfe000000ff000000ff000000ff000000ff000000ff000101ff007474fe00c9c9ff00ffffff00
ffffff00ffffff00ffffff00ffffff002525ff000000ff00b9b9fe00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00f5f5ff003d3dff000000ff000000ff000000ff000000ff001414ff00e6e6ff00fbfbff00fbfbff00fbfbff00fbfbff00fbfbff00fbfbff00fbfbff00f9f9ff003d3dff000000ff000000ff000000ff000000ff001414ff00e2e2fe00c9c9ff00ffffff00
ffffff00ffffff00ffffff00ddddff000000ff002525ff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00fbfbff00e2e2fe001414ff000101ff000000ff000404ff00b9b9fe00fbfbff00fbfbff00fbfbff00fbfbff00fbfbff00fbfbff00fbfbff00fbfbff00fbfbff00e6e6ff002525ff000000ff000000ff000101ff00b9b9fe00fbfbff00c9c9ff00ffffff00
ffffff00ffffff00ffffff009b9bfe000000ff005c5cff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00fbfbff00fbfbff00c5c5fc000101ff000000ff009b9bfe00fbfbff00fbfbff00fbfbff00fbfbff00fbfbff00fbfbff00fbfbff00fbfbff00fbfbff00fbfbff00f9f9ff00c7c7fd000404ff000000ff008888fe00fdfdff00f9f9ff00c9c9ff00ffffff00
ffffff00ffffff00ffffff005c5cff000101ff008888fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00f9f9ff00f9f9ff00f7f7ff008888fe003d3dff00f9f9ff00f9f9ff00f9f9ff00f9f9ff00f9f9ff00f9f9ff00f9f9ff00f9f9ff00f9f9ff00f9f9ff00f9f9ff00f9f9ff00f9f9ff008888fe003d3dff00fbfbff00f9f9ff00f9f9ff00c9c9ff00ffffff00
ffffff00ffffff00ffffff003d3dff000101ff00b9b9fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00f9f9ff00f9f9ff00f9f9ff00eaeaff00dfdffc00fbfbff00f9f9ff00f9f9ff00f9f9ff00f9f9ff00f9f9ff00f9f9ff00f9f9ff00f9f9ff00f9f9ff00f9f9ff00f9f9ff00f9f9ff00eeeefd00ddddff00fbfbff00f9f9ff00f9f9ff00c9c9ff00ffffff00
ffffff00ffffff00ffffff001414ff000101ff00ddddff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00f9f9ff00f9f9ff00f9f9ff00f9f9ff00f9f9ff00f9f9ff00f9f9ff00f9f9ff00f9f9ff00f9f9ff00f9f9ff00f9f9ff00f9f9ff00f9f9ff00f9f9ff00f9f9ff00f9f9ff00f9f9ff00f9f9ff00f9f9ff00f9f9ff00f9f9ff00f7f7ff00c9c9ff00ffffff00
ffffff00ffffff00eeeefd000000ff001414ff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00f7f7ff00f7f7ff00f7f7ff00f7f7ff00f7f7ff00f7f7ff00f7f7ff00f7f7ff007474fe000000ff002525ff00d0d0fb005c5cff000000ff003d3dff00f9f9ff00f7f7ff00f7f7ff00f7f7ff00f7f7ff00f7f7ff00f7f7ff00f7f7fd00c9c9ff00ffffff00
ffffff00ffffff00ddddff000000ff001414ff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00f7f7fd00f7f7fd00f7f7fd00f7f7fd00f7f7fd00f7f7fd00f7f7fd00f7f7fd001414ff000000ff000000ff001414ff000000ff000000ff000a0aff00f7f7fd00f7f7fd00f7f7fd00f7f7fd00f7f7fd00f7f7fd00f7f7fd00f5f5fd00c9c9ff00ffffff00
ffffff00ffffff00cbcbff000000ff003d3dff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00f5f5fd00f5f5fd00f5f5fd00f5f5fd00f5f5fd00f5f5fd00f5f5fd00f7f7fd002525ff000000ff000000ff000000ff000000ff000000ff000a0aff00f5f5fd00f5f5fd00f5f5fd00f5f5fd00f5f5fd00f5f5fd00f5f5fd00f5f5fd00c9c9ff00ffffff00
ffffff00ffffff00ffffff00ffffff00fbfbff00fdfdff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00f5f5fd00f5f5fd00f5f5fd00f5f5fd00f5f5fd00f5f5fd00f5f5fd00f5f5fd009b9bfe000101ff000000ff000000ff000000ff000000ff005c5cff00f5f5fd00f5f5fd00f5f5fd00f5f5fd00f5f5fd00f5f5fd00f5f5fd00f5f5fd00c9c9ff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00f3f3fd00f3f3fd00f3f3fd00f3f3fd00f3f3fd00f3f3fd00f3f3fd00f3f3fd00f3f3fd005c5cff000000ff000000ff000000ff002525ff00e6e6ff00f3f3fd00f3f3fd00f3f3fd00f3f3fd00f3f3fd00f3f3fd00f3f3fd00f1f1fd00c7c7fd00ffffff00
ffffff00ffffff00acacfe005c5cff009b9bfe00ffffff00ffffff008888fe005c5cff00c2c2fa00ffffff00ffffff00c7c7fd00f3f3fd00f3f3fd00f3f3fd00f3f3fd00f3f3fd00f3f3fd00f3f3fd00f3f3fd00f3f3fd00e5e5fb003d3dff000000ff001414ff00d0d0fb00f3f3fd00f3f3fd00f3f3fd00f3f3fd00f3f3fd00f3f3fd00f3f3fd00f3f3fd00f1f1fd00c7c7fd00ffffff00
ffffff00c5c5fc000101ff000101ff000000ff009b9bfe008888fe000101ff000404ff000a0aff00dadaff00ffffff00c7c7fd00f1f1fd00f1f1fd00f1f1fd00f1f1fd00f1f1fd00f1f1fd00f1f1fd00f1f1fd00f1f1fd00f1f1fd00d4d4fe001414ff009b9bfe00f1f1fd00f1f1fd00f1f1fd00f1f1fd00f1f1fd00f1f1fd00f1f1fd00f1f1fd00f1f1fd00f1f1fd00c7c7fd00ffffff00
ffffff005c5cff000000ff000000ff000000ff001414ff000a0aff000000ff000000ff000000ff007474fe00ffffff00c7c7fd00f1f1fd00f1f1fd00f1f1fd00f1f1fd00f1f1fd00f1f1fd00f1f1fd00f1f1fd00f1f1fd00f1f1fd00f1f1fa00c7c7fd00f3f3fd00f1f1fd00f1f1fd00f1f1fd00f1f1fd00f1f1fd00f1f1fd00f1f1fd00f1f1fd00f1f1fd00eeeefd00c7c7fd00ffffff00
ffffff005c5cff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000101ff007474fe00ffffff00c7c7fd00f1f1fd00f1f1fd00f1f1fd00f1f1fd00f1f1fd00f1f1fd00f1f1fd00f1f1fd00f1f1fd00f1f1fd00f1f1fd00f1f1fd00f1f1fd00f1f1fd00f1f1fd00f1f1fd00f1f1fd00f1f1fd00f1f1fd00f1f1fd00f1f1fd00f1f1fd00eeeefd00c7c7fd00ffffff00
ffffff009b9bfe000000ff000000ff000000ff000000ff000000ff000000ff000000ff000101ff00b9b9fe00ffffff00c5c5fc00d0d0fb003d3dff001414ff00acacfe00d0d0fb002525ff002525ff00b9b9fe00eeeefd00eeeefd00eeeefd00eeeefd00eeeefd00eeeefd00eeeefd00d0d0fb003d3dff001414ff00acacfe00d4d4fe002525ff002525ff00b9b9fe00c7c7fd00ffffff00
ffffff00f3f3ff003d3dff000000ff000000ff000000ff000000ff000000ff000404ff003d3dff00fbfbff00ffffff00c5c5fc007474fe000000ff000000ff001414ff002525ff000000ff000000ff005c5cff00ededfb00ededfb00ededfb00ededfb00ededfb00ededfb00eaeafb007474fe000000ff000000ff000a0aff002525ff000000ff000101ff003d3dff00c7c7fd00ffffff00
ffffff00ffffff00d4d4fe001414ff000000ff000000ff000000ff000000ff002525ff00eaeaff00ffffff00ffffff00c5c5fc007474fe000000ff000000ff000000ff000000ff000000ff000000ff005c5cff00e7e7fb00ededfb00ededfb00ededfb00ededfb00ededfb00ededfb007474fe000000ff000000ff000101ff000101ff000000ff000000ff005c5cff00c7c7fd00ffffff00
ffffff00ffffff00ffffff00c5c5fc000a0aff000000ff000000ff001414ff00dadaff00ffffff00ffffff00ffffff00c5c5fc00d0d0fb000a0aff000000ff000000ff000000ff000000ff000000ff00acacfe00eaeafb00eaeafb00eaeafb00eaeafb00eaeafb00eaeafb00eaeafb00d0d0fb001414ff000000ff000000ff000000ff000000ff000000ff00acacfe00c5c5fc00ffffff00
ffffff00ffffff00ffffff00ffffff00acacfe000000ff000a0aff00c5c5fc00ffffff00ffffff00ffffff00ffffff00c5c5fc00eaeafb009b9bfe000000ff000000ff000000ff000000ff007474fe00eaeafb00eaeafb00eaeafb00eaeafb00eaeafb00eaeafb00eaeafb00eaeafb00e7e7fb00acacfe000000ff000000ff000000ff000000ff007474fe00e5e5fb00c7c7fd00ffffff00
ffffff00ffffff00ffffff00ffffff00fdfdff007474fe008888fe00ffffff00ffffff00ffffff00ffffff00ffffff00c5c5fc00e7e7fb00ededfb007474fe000000ff000000ff005c5cff00eaeafb00eaeafb00eaeafb00eaeafb00eaeafb00eaeafb00eaeafb00eaeafb00eaeafb00eaeafb00e5e5fb008888fe000000ff000404ff003d3dff00e7e7fb00e5e5fb00c7c7fd00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00e2e2fe00e6e6ff00ffffff00ffffff00ffffff00ffffff00ffffff00c5c5fc00e7e7fb00e7e7fb00e7e7fb003d3dff001414ff00d8d8fd00e3e3fb00e7e7fb00e7e7fb00e7e7fb00e7e7fb00e7e7fb00e7e7fb00e7e7fb00e7e7fb00e7e7fb00e7e7fb00e5e5fb003d3dff001414ff00d8d8fd00eaeafb00e5e5fb00c5c5fc00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c5c5fc00e5e5fb00e7e7fb00e7e7fb00c5c5fc00acacfe00e5e5fb00e7e7fb00e7e7fb00e7e7fb00e7e7fb00e7e7fb00e7e7fb00e7e7fb00e7e7fb00e7e7fb00e7e7fb00e7e7fb00e7e7fb00c9c9fc009b9bfe00eaeafb00e7e7fb00e3e3fb00c5c5fc00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c5c5fc00e5e5fb00e5e5fb00e5e5fb00e5e5fb00e5e5fb00e5e5fb00e5e5fb00e5e5fb00e5e5fb00e5e5fb00e5e5fb00e5e5fb00e5e5fb00e5e5fb00e5e5fb00e5e5fb00e5e5fb00e5e5fb00e5e5fb00e7e7fb00e5e5fb00e5e5fb00e3e3fb00c5c5fc00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c5c5fc00e3e3f900e3e3f900e3e3f900e3e3f900e3e3f900e3e3f900e3e3f900e3e3f900e3e3f900e3e3f900e3e3f900e3e3f900e3e3f900e3e3f900e3e3f900e3e3f900e3e3f900e3e3f900e3e3f900e3e3f900e3e3f900e3e3f900e1e1f900c5c5fc00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c5c5fc00e3e3fb00e3e3fb00e3e3fb00e3e3fb00e3e3fb00e3e3fb00e3e3fb00e3e3fb00e3e3fb00e3e3fb00e3e3fb00e3e3fb00e3e3fb00e3e3fb00e3e3fb00e3e3fb00e3e3fb00e3e3fb00e3e3fb00e3e3fb00e3e3fb00e3e3fb00dfdff900c5c5fc00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c5c5fc00e1e1f900e1e1fb00e1e1fb00e1e1fb00e1e1fb00e1e1fb00e1e1fb00e1e1fb00e1e1fb00e1e1fb00e1e1fb00e1e1fb00e1e1fb00e1e1fb00e1e1fb00e1e1fb00e1e1fb00e1e1fb00e1e1fb00e1e1fb00e1e1fb00e3e3fb00dfdff900c5c5fc00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c5c5fc00e1e1f900e1e1f900e1e1f900e1e1f900e1e1f900e1e1f900e1e1f900e1e1f900e1e1f900e1e1f900e1e1f900e1e1f900e1e1f900e1e1f900e1e1f900e1e1f900e1e1f900e1e1f900e1e1f900e1e1f900e1e1f900e1e1f900ddddf900c5c5fc00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c2c2fa00dfdff900e1e1f900e1e1f900e1e1f900e1e1f900e1e1f900e1e1f900e1e1f900e1e1f900e1e1f900e1e1f900e1e1f900e1e1f900e1e1f900e1e1f900e1e1f900e1e1f900e1e1f900e1e1f900e1e1f900e1e1f900e1e1f900ddddf900c5c5fc00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c2c2fa00dfdff900dfdff900dfdff900dfdff900dfdff900dfdff900dfdff900dfdff900dfdff900dfdff900dfdff900dfdff900dfdff900dfdff900dfdff900dfdff900dfdff900dfdff900dfdff900dfdff900dfdff900dfdff900dbdbf900c5c5fc00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c2c2fa00ddddf900ddddf900ddddf900ddddf900ddddf900ddddf900ddddf900ddddf900ddddf900ddddf900ddddf900ddddf900ddddf900ddddf900ddddf900ddddf900ddddf900ddddf900ddddf900ddddf900ddddf900dfdff900dbdbf900c5c5fc00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c2c2fa00ddddf900ddddf900ddddf900b9b9fe009b9bfe00ddddf900ddddf900ddddf900ddddf900ddddf900ddddf900ddddf900ddddf900ddddf900ddddf900ddddf900ddddf900ddddf900bdbdf6009b9bfe00ddddf900ddddf900d9d9f900c5c5fc00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c2c2fa00dbdbf900ddddf900dbdbf9003d3dff001414ff00cdcdf900ddddf900ddddf900ddddf900ddddf900ddddf900ddddf900ddddf900ddddf900ddddf900ddddf900ddddf900ddddf9003d3dff000a0aff00cdcdf900ddddf900d9d9f900c5c5fc00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c2c2fa00dbdbf900dbdbf9007474fe000000ff000000ff003d3dff00ddddf900dbdbf900dbdbf900dbdbf900dbdbf900dbdbf900dbdbf900dbdbf900dbdbf900dbdbf900dbdbf9007474fe000000ff000000ff003d3dff00dbdbf900d7d7f700c5c5fc00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c2c2fa00d9d9f9008888fe000000ff000000ff000101ff000101ff005c5cff00ddddf900d9d9f700d9d9f700dbdbf900d9d9f700dbdbf900d9d9f700dbdbf900d9d9f7009b9bfe000000ff000000ff000000ff000000ff005c5cff00d7d7f700c5c5fc00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c2c2fa00bdbdf6000a0aff000000ff000000ff000000ff000101ff000404ff009b9bfe00d9d9f700d9d9f900d9d9f700d9d9f900d9d9f700d9d9f900d9d9f700bdbdf6000a0aff000000ff000000ff000000ff000000ff000000ff009b9bfe00c5c5fc00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00bdbdf6007474fe000000ff000000ff000000ff000000ff000000ff000000ff005c5cff00d9d9f700d9d9f900d9d9f900d9d9f900d9d9f900d9d9f900d7d7f7007474fe000000ff000000ff000000ff000000ff000000ff000101ff003d3dff00c5c5fc00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c2c2fa005c5cff000000ff000000ff001414ff002525ff000000ff000000ff003d3dff00d9d9f700d9d9f900d9d9f900d9d9f900d9d9f900d9d9f900d9d9f9005c5cff000000ff000000ff001414ff002525ff000000ff000000ff003d3dff00c9c8f500ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c2c2fa00c2c2fa003d3dff001414ff009b9bfe00c2c2fa002525ff002525ff00acacfe00d9d9f700d7d7f700d7d7f700d7d7f700d7d7f700d7d7f700d7d7f700c2c2fa003d3dff001414ff009b9bfe00c2c2fa002525ff002525ff00acacfe00c2c2fa00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c2c2fa00d7d7f700d7d7f700d9d9f700d7d7f700d7d7f700d7d7f700d9d9f700d7d7f700d7d7f700d7d7f700d7d7f700d7d7f700d7d7f700d7d7f700d7d7f700d7d7f700d7d7f700d7d7f700d7d7f700d7d7f700d7d7f700d7d7f700d2d2f800c5c5fc00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c2c2fa00d5d5f700d7d7f700d7d7f700d7d7f700d7d7f700d7d7f700d7d7f700d7d7f700d7d7f700d7d7f700d7d7f700d7d7f700d7d7f700d7d7f700d7d7f700d7d7f700d7d7f700d7d7f700d7d7f700d7d7f700d7d7f700d7d7f700d2d2f800c5c5fc00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c2c2fa00d5d5f700d5d5f700d5d5f700d5d5f700d5d5f700d5d5f700d5d5f700d5d5f700d5d5f700d5d5f700d5d5f700d5d5f700d5d5f700d5d5f700d5d5f700d5d5f700d5d5f700d5d5f700d5d5f700d5d5f700d5d5f700d7d7f700d2d2f800c5c5fc00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c2c2fa00d5d5f700d5d5f700d5d5f700d5d5f700d5d5f700d5d5f700d5d5f700d5d5f700d5d5f700d5d5f700d5d5f700d5d5f700d5d5f700d5d5f700d5d5f700d5d5f700d5d5f700d5d5f700d5d5f700d5d5f700d5d5f700d5d5f700d2d2f800c2c2fa00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c2c2fa00d5d5f700d5d5f700d5d5f700d5d5f700d5d5f700d5d5f700d5d5f700d5d5f700d5d5f700d5d5f700d5d5f700d5d5f700d5d5f700d5d5f700d5d5f700d5d5f700d5d5f700d5d5f700d5d5f700d5d5f700d5d5f700d5d5f700d2d2f800c2c2fa00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c2c2fa00d5d5f700d7d7f700d7d7f700d7d7f700d7d7f700d7d7f700d7d7f700d7d7f700d7d7f700d7d7f700d7d7f700d7d7f700d7d7f700d7d7f700d7d7f700d7d7f700d7d7f700d7d7f700d7d7f700d7d7f700d7d7f700d7d7f700d2d2f800c5c5fc00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00c9c9ff00c9c9ff00c9c9ff00c9c9ff00c9c9ff00c9c9ff00c9c9ff00c9c9ff00c9c9ff00c9c9ff00c9c9ff00c9c9ff00c9c9ff00c9c9ff00c9c9ff00c9c9ff00c9c9ff00c9c9ff00c9c9ff00c9c9ff00c9c9ff00c9c9ff00c9c9ff00c9c9ff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
i$Qd               13  31 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff009b9bfe002525ff000000ff000000ff008888fe00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff009b9bfe000000ff000000ff000000ff000000ff000404ff005c5cff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00eeeefd001414ff000000ff000000ff000000ff000000ff000000ff000101ff00cdcdff00ffffff00ffffff00c9c9ff00
ffffff008888fe000000ff001414ff00acacfe00ffffff00cbcbff002525ff000000ff005c5cff00ffffff00ffffff00c9c9ff00
fdfdff005c5cff000000ff008888fe00ffffff00ffffff00ffffff00acacfe000101ff002525ff00ffffff00ffffff00c9c9ff00
ffffff002525ff000000ff00ddddff00ffffff00ffffff00ffffff00fbfbff001414ff001414ff00ffffff00ffffff00c9c9ff00
ffffff000a0aff001414ff00ffffff00ffffff00ffffff00ffffff00ffffff002525ff000000ff00eeeefd00ffffff00c9c9ff00
ffffff001414ff001414ff00ffffff00ffffff00ffffff00ffffff00ffffff003d3dff000000ff00eeeefd00ffffff00c9c9ff00
ffffff001414ff000a0aff00ffffff00ffffff00ffffff00ffffff00fbfbff002525ff000000ff00eeeefd00ffffff00c9c9ff00
ffffff002525ff000000ff00ddddff00ffffff008888fe009b9bfe00ddddff000000ff001414ff00ffffff00ffffff00c9c9ff00
ffffff005c5cff000000ff007474fe00ffffff003d3dff000101ff003d3dff000000ff002525ff00fdfdff00ffffff00c9c9ff00
fdfdff008888fe000000ff001414ff00acacfe00cdcdff002525ff000000ff000000ff007474fe00ffffff00ffffff00c9c9ff00
ffffff00eeeefd001414ff000000ff000000ff000000ff000000ff000000ff000000ff00cbcbff00ffffff00ffffff00c9c9ff00
ffffff00ffffff009b9bfe000000ff000000ff000000ff000000ff000000ff000101ff003d3dff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff009b9bfe002525ff000000ff000000ff007474fe000000ff000101ff00acacfe00ffffff00c7c7fd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00cbcbff002525ff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00ffffff005c5cff00eeeefd00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00acacfe000000ff007474fe00fdfdff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00d8d8fd000000ff000000ff000000ff00acacfe00fdfdff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00f1f1ff002525ff000000ff000000ff000000ff000101ff00dadaff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
f9f9ff003d3dff000000ff000000ff000000ff000000ff000000ff001414ff00eaeaff00ffffff00ffffff00ffffff00c5c5fc00
b9b9fe000000ff000000ff000000ff000000ff000000ff000000ff000000ff007474fe00ffffff00ffffff00ffffff00c5c5fc00
fbfbff003d3dff000404ff000000ff000000ff000000ff000000ff001414ff00eeeefd00ffffff00ffffff00ffffff00c5c5fc00
ffffff00f7f7ff003d3dff000000ff000000ff000000ff000404ff00e6e6ff00ffffff00ffffff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00ddddff000404ff000000ff000000ff00acacfe00ffffff00ffffff00ffffff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00ffffff00b9b9fe000000ff007474fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00ffffff00ffffff007474fe00eeeefd00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c2c2fa00
i$2h               13  31 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00acacfe003d3dff000000ff001414ff00acacfe00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00cbcbff000000ff000000ff000000ff000000ff000000ff00cbcbff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff007474fe000101ff003d3dff00ffffff005c5cff000404ff005c5cff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff003d3dff000101ff009b9bfe00ffffff00b9b9fe000404ff003d3dff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00ffffff00b9b9fe000000ff000a0aff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00ffffff008888fe000000ff003d3dff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00ffffff002525ff000101ff005c5cff00fdfdff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff005c5cff000101ff000000ff00acacfe00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00b9b9fe000404ff000000ff003d3dff00fbfbff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff002525ff000000ff000000ff00b9b9fe00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff007474fe000000ff000101ff005c5cff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00eeeefd001414ff000000ff002525ff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff008888fe000000ff000000ff009b9bfe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff003d3dff000000ff000000ff000000ff000000ff000000ff001414ff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff001414ff000000ff000000ff000000ff000000ff000000ff001414ff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff009b9bfe005c5cff00acacfe00ffffff00f5f5ff008888fe005c5cff00cdcdff00ffffff00ffffff00c7c7fd00
ffffff00acacfe000101ff000101ff000000ff00acacfe007474fe000000ff000404ff001414ff00e6e6ff00ffffff00c7c7fd00
ffffff003d3dff000404ff000000ff000000ff001414ff000404ff000000ff000000ff000101ff009b9bfe00ffffff00c5c5fc00
ffffff003d3dff000101ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff008888fe00ffffff00c5c5fc00
ffffff008888fe000000ff000000ff000000ff000000ff000000ff000000ff000000ff000101ff00d4d4fe00ffffff00c5c5fc00
ffffff00e6e6ff002525ff000000ff000000ff000000ff000000ff000000ff000101ff005c5cff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00c9c9ff000a0aff000101ff000000ff000000ff000000ff003d3dff00f1f1ff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00ffffff00b9b9fe000404ff000000ff000000ff002525ff00e6e6ff00ffffff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00ffffff00fbfbff009b9bfe000000ff001414ff00d4d4fe00ffffff00ffffff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00ffffff00ffffff00fdfdff005c5cff009b9bfe00ffffff00ffffff00ffffff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00ffffff00ffffff00ffffff00ddddff00f1f1ff00ffffff00ffffff00ffffff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c5c5fc00
i$Qc               13  31 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff009a999a0025252600000000000000000088888800ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00ffffff009a999a00000000000000000000000000000000000000000065656500ffffff00ffffff00ffffff00e5e3e500
ffffff00efedef0012121200000000000000000000000000000000000000000000000000cccccc00ffffff00ffffff00e5e3e500
ffffff00888888000000000012121200aaaaab00ffffff00cccccc00252526000000000065656500ffffff00ffffff00e5e3e500
ffffff00565656000000000088888800ffffff00ffffff00ffffff00aaaaab000000000025252600ffffff00ffffff00e5e3e500
ffffff002525260000000000dddddd00ffffff00ffffff00ffffff00ffffff001212120012121200ffffff00ffffff00e3e3e300
ffffff001212120012121200ffffff00ffffff00ffffff00ffffff00ffffff002525260000000000efedef00ffffff00e3e3e300
ffffff001212120012121200ffffff00ffffff00ffffff00ffffff00ffffff003334340000000000efedef00ffffff00e3e3e300
ffffff001212120012121200ffffff00ffffff00ffffff00ffffff00fffdfd002525260000000000efedef00ffffff00e3e3e300
ffffff002525260000000000dddddd00ffffff00888888009a999a00dddddd000000000012121200ffffff00ffffff00e1e3e100
ffffff00565656000000000073737300ffffff004444450000000000333434000000000025252600fffdfd00ffffff00e3e1e300
ffffff00888888000000000012121200aaaaab00cccccc0025252600000000000000000073737300ffffff00ffffff00e3e1e300
ffffff00efedef0012121200000000000000000000000000000000000000000000000000cccccc00ffffff00ffffff00e3e1e300
ffffff00ffffff009a999a0000000000000000000000000000000000000000000000000044444500ffffff00ffffff00e1e1e100
ffffff00ffffff00ffffff009a999a00252526000000000000000000737373000000000000000000aaaaab00ffffff00e1e1e100
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00cccccc0025252600ffffff00ffffff00e1e1e100
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e1dfe100
ffffff00ffffff00ffffff00ffffff00d7d7d7004444450092929200fdfdfd00ffffff00ffffff00ffffff00ffffff00e1dfe100
ffffff00ffffff00ffffff00e7e7e70000000000010101000000000088888800ffffff00ffffff00ffffff00ffffff00dfdfdf00
ffffff00ffffff00ffffff00b2b2b20000000000000000000000000012121200fdfdfd00ffffff00ffffff00ffffff00dfdfdf00
ffffff00ffffff00ffffff00d4d4d40000000000000000000000000065656500ffffff00ffffff00ffffff00ffffff00dfdfe100
ffffff00e9e9e90065656500929292006565650000000000252526009a999a0065656500aaaaab00ffffff00ffffff00dfdddf00
f7f7f7000a0a0a0000000000000000001b1b1c00000000000e0e0f00030303000000000000000000b2b2b200ffffff00dfdddf00
aaaaab0000000000000000000000000006060600444445001b1b1c0000000000000000000000000044444500fdfdfd00dfdddf00
d9d9d90000000000000000000000000033343400a2a2a2007373730000000000000000000000000065656500ffffff00dddbdd00
ffffff00929292001212120025252600dfdfdf0056565600e3e1e300737373000e0e0f0033343400f3f5f300ffffff00dddddb00
ffffff00ffffff00ededed00f5f5f700ffffff0006060600d7d7d700ffffff00e7e7e700ffffff00ffffff00ffffff00dddbdd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dddbdd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00d9d9d900
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dbdbdb00
i$Qh               13  31 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff009b9bfe002525ff000000ff000000ff008888fe00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff009b9bfe000000ff000000ff000000ff000000ff000404ff005c5cff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00eeeefd001414ff000000ff000000ff000000ff000000ff000000ff000101ff00cdcdff00ffffff00ffffff00c9c9ff00
ffffff008888fe000000ff001414ff00acacfe00ffffff00cbcbff002525ff000000ff005c5cff00ffffff00ffffff00c9c9ff00
ffffff005c5cff000000ff008888fe00ffffff00ffffff00ffffff00acacfe000000ff002525ff00ffffff00ffffff00c9c9ff00
fbfbff002525ff000000ff00ddddff00ffffff00ffffff00ffffff00ffffff001414ff001414ff00ffffff00ffffff00c9c9ff00
ffffff000a0aff001414ff00ffffff00ffffff00ffffff00ffffff00ffffff002525ff000000ff00eeeefd00ffffff00c9c9ff00
ffffff001414ff001414ff00fdfdff00ffffff00ffffff00ffffff00ffffff002525ff000101ff00eeeefd00ffffff00c9c9ff00
ffffff001414ff000a0aff00ffffff00ffffff00ffffff00ffffff00ffffff002525ff000000ff00eeeefd00ffffff00c9c9ff00
ffffff002525ff000000ff00ddddff00ffffff008888fe009b9bfe00ddddff000000ff001414ff00ffffff00ffffff00c9c9ff00
fdfdff005c5cff000000ff007474fe00ffffff003d3dff000000ff003d3dff000000ff002525ff00ffffff00ffffff00c9c9ff00
ffffff008888fe000000ff001414ff00acacfe00cdcdff002525ff000000ff000000ff007474fe00ffffff00ffffff00c9c9ff00
ffffff00eeeefd001414ff000000ff000000ff000000ff000000ff000000ff000000ff00cbcbff00ffffff00ffffff00c9c9ff00
ffffff00ffffff009b9bfe000000ff000000ff000000ff000000ff000000ff000101ff003d3dff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00fdfdff009b9bfe002525ff000000ff000000ff007474fe000000ff000101ff00acacfe00ffffff00c7c7fd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00cbcbff002525ff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00c2c2fa005c5cff008888fe00ffffff00ffffff009b9bfe005c5cff00acacfe00ffffff00ffffff00c7c7fd00
ffffff00d4d4fe000a0aff000101ff000404ff008888fe009b9bfe000000ff000101ff000404ff00c7c7fd00ffffff00c7c7fd00
ffffff007474fe000000ff000000ff000000ff000a0aff000a0aff000000ff000000ff000000ff005c5cff00ffffff00c7c7fd00
ffffff007474fe000000ff000000ff000000ff000101ff000101ff000000ff000000ff000000ff005c5cff00ffffff00c7c7fd00
ffffff00b9b9fe000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff00acacfe00ffffff00c5c5fc00
ffffff00fbfbff003d3dff000000ff000000ff000000ff000000ff000000ff000000ff003d3dff00f3f3ff00ffffff00c5c5fc00
ffffff00ffffff00e6e6ff002525ff000000ff000000ff000000ff000000ff001414ff00dadaff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00ffffff00d4d4fe001414ff000000ff000000ff000a0aff00c9c9ff00ffffff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00ffffff00ffffff00c2c2fa000a0aff000404ff00acacfe00ffffff00ffffff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00ffffff00ffffff00ffffff007474fe007474fe00ffffff00ffffff00ffffff00ffffff00ffffff00c2c2fa00
ffffff00ffffff00ffffff00ffffff00ffffff00eaeaff00e6e6ff00ffffff00ffffff00ffffff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c5c5fc00
i$Jc               13  31 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00565656000000000088888800ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00ffffff00ffffff00ffffff00ffffff00565656000000000088888800ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00ffffff00ffffff00ffffff00ffffff00565656000000000088888800ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00ffffff00ffffff00ffffff00ffffff00565656000000000088888800ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00ffffff00ffffff00ffffff00ffffff00565656000000000088888800ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00ffffff00ffffff00ffffff00ffffff00565656000000000088888800ffffff00ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff00ffffff00ffffff00ffffff00565656000000000088888800ffffff00ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff00ffffff00ffffff00ffffff00565656000000000088888800ffffff00ffffff00ffffff00ffffff0092929200
ffffff00ffffff00ffffff00ffffff00ffffff00565656000000000088888800ffffff00ffffff00ffffff00fbf9f90073737300
efedef000000000012121200ffffff00ffffff00565656000000000088888800ffffff00ffffff00ffffff00fdfdfd0073737300
efedef000000000000000000dddddd00ffffff00565656000000000088888800ffffff00ffffff00ffffff00fdfdfd0073737300
ffffff00121212000000000088888800efedef002525260000000000aaaaab00ffffff00ffffff00ffffff00fdfdfd0073737300
ffffff00565656000000000000000000000000000000000000000000dddddd00ffffff00ffffff00ffffff00fbfbfb0073737300
ffffff00aaaaab000000000000000000000000000000000056565600ffffff00ffffff00ffffff00ffffff00fbfbfb0065656500
ffffff00ffffff0088888800000000000000000044444500ffffff00ffffff00ffffff00ffffff00ffffff00fbfbfb0065656500
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fbf9f90065656500
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fbfbfb0065656500
ffffff00ffffff00ffffff00ffffff00cccccc00444445009a999a00ffffff00ffffff00ffffff00ffffff00f9f9f90065656500
ffffff00ffffff00ffffff00dfdddf000000000001020300010101009a999a00ffffff00ffffff00ffffff00f9f9f90065656500
ffffff00ffffff00ffffff00a2a2a20000000000000000000000000025252600ffffff00ffffff00ffffff00f9f7f90065656500
ffffff00ffffff00ffffff00c3c3c40000000000000000000000000073737300ffffff00ffffff00ffffff00f9f9f70065656500
ffffff00e3e1e30065656500929292005656560001010100252526009a999a0065656500bbbbbb00ffffff00f7f9f70065656500
f3f3f3000101010000000000000000001b1b1c000101010012121200000000000000000000000000c3c3c400f7f7f70065656500
9a999a000000000000000000000000000a0a0a00444445001212120000000000000000000000000056565600f5f3f50065656500
d0d0d00000000000000000000000000044444500a2a2a200737373000000000000000000000000007d7d7d00f5f5f70065656500
ffffff00888888001212120025252600e5e3e50044444500e5e3e500656565000e0e0f0044444500f7f7f700f5f3f50065656500
ffffff00fdfdfd00edebed00fbf9f900fdfdfd0001030100e5e5e500fdfdfd00e7e7e700fdfdfd00ffffff00f5f5f50065656500
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00f3f3f30056565600
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00f3f5f30065656500
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00f3f3f30065656500
i$dealer2          4   4  
ffffff00ffffff00ffffff00ffffff00
ffffff00f6e6cf00e1aa5c00e2ad6400
ffffff00f1d8b500cf790400cf780400
ffffff00f0d6b500cd740400cc730400
i$8h               13  31 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00eeeefd003d3dff000101ff000000ff007474fe00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff003d3dff000101ff000000ff000000ff000000ff008888fe00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ddddff000101ff001414ff00ddddff008888fe000000ff002525ff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00acacfe000000ff003d3dff00fbfbff00eeeefd000000ff001414ff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00b9b9fe000000ff002525ff00ffffff00ddddff000000ff001414ff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff001414ff001414ff00ddddff008888fe000000ff005c5cff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00acacfe000000ff000000ff000000ff003d3dff00eaeaff00fdfdff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff008888fe000000ff000000ff000000ff000a0aff00cbcbff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ddddff000000ff001414ff00eeeefd009b9bfe000000ff003d3dff00fbfbff00ffffff00ffffff00ffffff00c9c9ff00
ffffff009b9bfe000101ff005c5cff00ffffff00ffffff000a0aff000000ff00ddddff00ffffff00ffffff00ffffff00c9c9ff00
ffffff008888fe000000ff007474fe00ffffff00ffffff001414ff000000ff00cbcbff00ffffff00ffffff00ffffff00c9c9ff00
ffffff008888fe000000ff005c5cff00ffffff00ffffff001414ff000000ff00ddddff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00b9b9fe000101ff001414ff00ddddff009b9bfe000000ff002525ff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff003d3dff000000ff000000ff000000ff000000ff008888fe00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00f1f1ff003d3dff000000ff000000ff008888fe00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00d0d0fb005c5cff008888fe00f5f5ff00ffffff00acacfe005c5cff009b9bfe00ffffff00ffffff00c5c5fc00
ffffff00e6e6ff001414ff000101ff000000ff007474fe00acacfe000000ff000000ff000404ff00acacfe00ffffff00c7c7fd00
ffffff009b9bfe000000ff000000ff000000ff000404ff001414ff000000ff000000ff000000ff003d3dff00ffffff00c7c7fd00
ffffff009b9bfe000000ff000000ff000000ff000000ff000000ff000000ff000000ff000101ff003d3dff00ffffff00c7c7fd00
ffffff00d0d0fb000101ff000000ff000000ff000000ff000000ff000000ff000000ff000101ff008888fe00ffffff00c7c7fd00
ffffff00ffffff005c5cff000000ff000000ff000000ff000000ff000000ff000000ff002525ff00e6e6ff00ffffff00c5c5fc00
ffffff00ffffff00f1f1ff003d3dff000000ff000000ff000000ff000000ff000a0aff00c7c7fd00ffffff00ffffff00c5c5fc00
ffffff00ffffff00ffffff00e6e6ff002525ff000000ff000000ff000404ff00acacfe00ffffff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00ffffff00ffffff00d4d4fe001414ff000000ff009b9bfe00ffffff00ffffff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00ffffff00ffffff00ffffff009b9bfe005c5cff00ffffff00ffffff00ffffff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00ffffff00ffffff00ffffff00f1f1ff00ddddff00ffffff00ffffff00ffffff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c5c5fc00
i$5d               13  31 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff003d3dff000000ff000000ff000000ff000000ff008888fe00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff002525ff000000ff000000ff000000ff000000ff008888fe00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff001414ff001414ff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00cbcbff000000ff002525ff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00acacfe000000ff005c5cff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff008888fe000000ff001414ff000000ff001414ff00acacfe00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff005c5cff000404ff000000ff000000ff000000ff001414ff00dadaff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff007474fe000101ff008888fe00ffffff003d3dff000000ff007474fe00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00acacfe000000ff003d3dff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00cbcbff000000ff000a0aff00fdfdff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00cbcbff000000ff002525ff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff002525ff000000ff00cbcbff00ffffff009b9bfe000000ff003d3dff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff003d3dff000404ff005c5cff00ffffff003d3dff000101ff008888fe00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00b9b9fe000000ff000101ff000101ff000101ff002525ff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff009b9bfe001414ff000000ff003d3dff00ddddff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00ffffff00f1f1ff007474fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00fbfbff005c5cff000000ff00b9b9fe00fdfdff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff009b9bfe000000ff000000ff000000ff00dadaff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00d8d8fd000000ff000000ff000000ff000101ff002525ff00f5f5ff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00e6e6ff000a0aff000000ff000000ff000000ff000000ff000404ff005c5cff00ffffff00ffffff00fdfdff00c7c7fd00
ffffff005c5cff000000ff000000ff000000ff000000ff000000ff000000ff000404ff00ffffff00ffffff00fdfdff00c5c5fc00
ffffff00e6e6ff001414ff000000ff000000ff000000ff000000ff000000ff005c5cff00ffffff00ffffff00fdfdff00c5c5fc00
ffffff00ffffff00e2e2fe000404ff000000ff000000ff000000ff003d3dff00fbfbff00ffffff00ffffff00fdfdff00c5c5fc00
ffffff00ffffff00ffffff00acacfe000000ff000000ff000404ff00e2e2fe00ffffff00ffffff00ffffff00fdfdff00c5c5fc00
ffffff00ffffff00ffffff00ffffff007474fe000000ff00b9b9fe00ffffff00ffffff00ffffff00ffffff00fdfdff00c5c5fc00
ffffff00ffffff00ffffff00fdfdff00eeeefd008888fe00ffffff00ffffff00ffffff00ffffff00ffffff00fbfbff00c5c5fc00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fbfbff00c5c5fc00
i$6h               13  31 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff008888fe000000ff000000ff008888fe00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff008888fe000000ff000000ff000000ff000000ff00acacfe00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff002525ff001414ff00ddddff008888fe000101ff003d3dff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00b9b9fe000000ff005c5cff00ffffff00cbcbff000000ff005c5cff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff008888fe000101ff008888fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff007474fe000000ff007474fe001414ff000000ff008888fe00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff007474fe000000ff000000ff000000ff000000ff000000ff00acacfe00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff007474fe000000ff002525ff00eeeefd008888fe000000ff003d3dff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff007474fe000000ff005c5cff00ffffff00ddddff000000ff001414ff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff007474fe000101ff008888fe00ffffff00ffffff001414ff000000ff00ddddff00ffffff00ffffff00ffffff00c9c9ff00
ffffff008888fe000000ff008888fe00ffffff00ffffff001414ff000000ff00ddddff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00b9b9fe000000ff005c5cff00ffffff00eeeefd000000ff001414ff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff001414ff001414ff00ddddff008888fe000000ff003d3dff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff008888fe000000ff000000ff000000ff000000ff00acacfe00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff007474fe000000ff000000ff009b9bfe00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00fdfdff009b9bfe005c5cff00b9b9fe00ffffff00eeeefd007474fe005c5cff00dadaff00ffffff00ffffff00c7c7fd00
ffffff009b9bfe000000ff000000ff000404ff00b9b9fe005c5cff000101ff000404ff002525ff00f3f3ff00ffffff00c7c7fd00
ffffff002525ff000000ff000000ff000000ff001414ff000000ff000000ff000000ff000404ff00b9b9fe00ffffff00c7c7fd00
ffffff002525ff000101ff000000ff000000ff000101ff000000ff000000ff000000ff000000ff00acacfe00ffffff00c7c7fd00
ffffff007474fe000000ff000000ff000000ff000000ff000000ff000000ff000000ff000a0aff00e6e6ff00ffffff00c7c7fd00
ffffff00d4d4fe001414ff000000ff000000ff000000ff000000ff000000ff000101ff007474fe00ffffff00ffffff00c5c5fc00
ffffff00ffffff00acacfe000404ff000000ff000000ff000000ff000000ff005c5cff00fbfbff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00ffffff009b9bfe000000ff000000ff000000ff003d3dff00f1f1ff00ffffff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00ffffff00ffffff007474fe000000ff002525ff00e6e6ff00ffffff00ffffff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00ffffff00ffffff00ffffff003d3dff00b9b9fe00ffffff00ffffff00ffffff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00ffffff00ffffff00ffffff00dadaff00f9f9ff00ffffff00ffffff00ffffff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c5c5fc00
i$Td               13  31 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ddddff000000ff008888fe00ffffff00ffffff008888fe000000ff003d3dff00eeeefd00ffffff00ffffff00c9c9ff00
ffffff009b9bfe000000ff008888fe00ffffff00eeeefd001414ff000000ff000000ff007474fe00ffffff00ffffff00c9c9ff00
ffffff003d3dff000000ff008888fe00ffffff009b9bfe000000ff00acacfe003d3dff002525ff00ffffff00ffffff00c9c9ff00
5c5cff000000ff000000ff008888fe00ffffff005c5cff001414ff00ffffff008888fe000000ff00ddddff00ffffff00c9c9ff00
0101ff001414ff000000ff008888fe00ffffff005c5cff001414ff00ffffff009b9bfe000000ff00b9b9fe00ffffff00c9c9ff00
0000ff007474fe000000ff008888fe00fdfdff003d3dff002525ff00ffffff00acacfe000000ff00acacfe00ffffff00c9c9ff00
8888fe00acacfe000000ff008888fe00ffffff002525ff003d3dff00ffffff00acacfe000000ff00acacfe00ffffff00c9c9ff00
ffffff00acacfe000000ff008888fe00ffffff003d3dff002525ff00ffffff00acacfe000000ff00acacfe00ffffff00c9c9ff00
ffffff00acacfe000000ff008888fe00ffffff002525ff003d3dff00ffffff00acacfe000000ff00acacfe00ffffff00c9c9ff00
ffffff00acacfe000000ff008888fe00fbfbff003d3dff003d3dff00fdfdff00acacfe000000ff00acacfe00ffffff00c9c9ff00
ffffff00acacfe000000ff008888fe00ffffff005c5cff001414ff00ffffff009b9bfe000000ff00b9b9fe00ffffff00c9c9ff00
ffffff00acacfe000000ff008888fe00fdfdff005c5cff001414ff00ffffff008888fe000000ff00ddddff00ffffff00c9c9ff00
ffffff00acacfe000000ff008888fe00ffffff009b9bfe000000ff00acacfe005c5cff002525ff00ffffff00ffffff00c9c9ff00
ffffff00acacfe000000ff008888fe00ffffff00eeeefd001414ff000000ff000000ff007474fe00ffffff00ffffff00c9c9ff00
ffffff00acacfe000000ff008888fe00ffffff00ffffff008888fe000000ff003d3dff00eaeaff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fdfdff00fbfbff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00d4d4fe007474fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00f7f7ff003d3dff000000ff00dadaff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff005c5cff000000ff000000ff001414ff00f1f1ff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00acacfe000101ff000000ff000000ff000000ff005c5cff00fbfbff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
d0d0fb000101ff000000ff000000ff000000ff000000ff000101ff007474fe00fbfbff00ffffff00ffffff00ffffff00c7c7fd00
3d3dff000000ff000000ff000000ff000000ff000000ff000000ff000101ff00c7c7fd00ffffff00ffffff00ffffff00c7c7fd00
d4d4fe000000ff000000ff000000ff000000ff000000ff000000ff008888fe00fdfdff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00c2c2fa000000ff000000ff000000ff000000ff007474fe00fdfdff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff007474fe000000ff000000ff002525ff00f1f1ff00fdfdff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00fdfdff003d3dff000000ff00ddddff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00ffffff00d8d8fd008888fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c5c5fc00
i$5c               13  31 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00444445000000000000000000000000000000000088888800ffffff00ffffff00ffffff00e5e3e500
ffffff00ffffff00ffffff00252526000000000000000000000000000000000088888800ffffff00ffffff00ffffff00e5e3e500
ffffff00ffffff00ffffff001212120012121200ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00ffffff00cccccc000000000033343400ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00ffffff00aaaaab000000000056565600ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00ffffff008888880000000000121212000000000012121200aaaaab00ffffff00ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff00656565000000000000000000000000000000000012121200dddddd00ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff00737373000000000088888800ffffff00444445000000000073737300ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00aaaaab000000000033343400ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00cccccc000000000012121200ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00cccccc000000000025252600fffdfd00ffffff00ffffff00e3e1e300
ffffff00ffffff002525260000000000cccccc00ffffff009a999a000000000044444500ffffff00ffffff00ffffff00e3e1e300
ffffff00ffffff00444445000000000065656500ffffff00444445000000000088888800ffffff00ffffff00ffffff00e3e1e300
ffffff00ffffff00bbbbbb000000000000000000000000000000000025252600ffffff00ffffff00ffffff00ffffff00e1e1e100
ffffff00ffffff00ffffff009a999a00121212000000000044444500dddddb00ffffff00ffffff00ffffff00ffffff00e1e1e100
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e1e1e100
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e1dfe100
ffffff00ffffff00ffffff00ffffff00fbf9f9007d7d7d0056565600e1dfe100ffffff00ffffff00ffffff00ffffff00e1dfe100
ffffff00ffffff00ffffff00ffffff0073737300000000000000000001010100f1f1f200ffffff00ffffff00ffffff00e1dfe100
ffffff00ffffff00ffffff00fdfdfd001b1b1c00000000000000000000000000bbbbbb00ffffff00ffffff00ffffff00dfdfdf00
ffffff00ffffff00ffffff00fdfdfd0056565600000000000000000000000000e1e1e100ffffff00ffffff00ffffff00dfdfdf00
ffffff00fdfdfd00a2a2a20073737300888888001b1b1c0003030300737373008888880073737300f1f1f200ffffff00dfdfdf00
ffffff009292920000000000000000000a0a0a0006060600000000001b1b1c00000000000000000025252600ffffff00dfdfdf00
ffffff001b1b1c0001010100000000000000000025252600444445000303030000000000000000000e0e0f00fdfdfd00dfdddf00
ffffff00444445000000000000000000000000007d7d7d00a2a2a2002525260000000000000000000a0a0a00fdfdfd00dfdddf00
ffffff00e7e7e700333434000e0e0f0092929200cccccc0073737300d0d0d0001b1b1c0012121200aaaaab00fdfdfd00dddddd00
ffffff00ffffff00fbf9f900e9e9e900fffdfd00aaaaab001b1b1c00ffffff00f5f3f500efefef00ffffff00fdfdfd00dddddd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fdfdfd00dbdddd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fdfdfd00dddbdd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fdfdfd00dddbdd00
i$5h               13  31 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff003d3dff000101ff000000ff000000ff000000ff008888fe00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff002525ff000000ff000000ff000000ff000000ff008888fe00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff001414ff001414ff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00cbcbff000000ff003d3dff00fbfbff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00acacfe000000ff005c5cff00fbfbff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff008888fe000000ff000a0aff000000ff001414ff00acacfe00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff005c5cff000000ff000000ff000000ff000000ff001414ff00ddddff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff007474fe000404ff008888fe00ffffff003d3dff000000ff007474fe00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00acacfe000000ff003d3dff00fbfbff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00cbcbff000000ff000a0aff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00cbcbff000000ff002525ff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff002525ff000000ff00cbcbff00ffffff009b9bfe000000ff003d3dff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff003d3dff000101ff005c5cff00ffffff003d3dff000404ff008888fe00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00b9b9fe000101ff000404ff000000ff000000ff002525ff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff009b9bfe001414ff000101ff003d3dff00ddddff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00f3f3ff007474fe005c5cff00d4d4fe00ffffff00d4d4fe007474fe007474fe00f1f1ff00ffffff00ffffff00c7c7fd00
f7f7ff005c5cff000101ff000404ff002525ff00c7c7fd002525ff000000ff000000ff005c5cff00f7f7ff00ffffff00c7c7fd00
b9b9fe000a0aff000000ff000000ff000000ff002525ff000000ff000000ff000000ff000404ff00c2c2fa00ffffff00c7c7fd00
b9b9fe000404ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000404ff00b9b9fe00fdfdff00c7c7fd00
eaeaff003d3dff000000ff000000ff000000ff000000ff000000ff000000ff000000ff003d3dff00eaeaff00fdfdff00c7c7fd00
ffffff00acacfe000000ff000000ff000000ff000000ff000000ff000000ff000000ff00acacfe00ffffff00fdfdff00c5c5fc00
ffffff00ffffff008888fe000000ff000000ff000000ff000000ff000000ff008888fe00ffffff00ffffff00fdfdff00c5c5fc00
ffffff00ffffff00ffffff007474fe000000ff000000ff000000ff005c5cff00ffffff00ffffff00ffffff00fdfdff00c5c5fc00
ffffff00ffffff00fdfdff00fbfbff003d3dff000404ff003d3dff00ffffff00ffffff00ffffff00ffffff00fdfdff00c5c5fc00
ffffff00ffffff00ffffff00ffffff00eaeaff003d3dff00eaeaff00ffffff00ffffff00ffffff00ffffff00fdfdff00c5c5fc00
ffffff00ffffff00ffffff00ffffff00ffffff00d4d4fe00ffffff00ffffff00ffffff00ffffff00ffffff00fdfdff00c5c5fc00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fbfbff00c5c5fc00
i$Ks               13  31 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
cccccc000000000033343400ffffff00ffffff00ffffff008888880000000000000000009a999a00ffffff00ffffff00e5e3e500
cccccc000000000033343400ffffff00ffffff00efedef00121212000000000056565600ffffff00ffffff00ffffff00e5e3e500
cccccc000000000033343400ffffff00ffffff00565656000000000025252600efeded00ffffff00ffffff00ffffff00e5e3e500
cccccc000000000033343400ffffff009a999a0000000000000000009a999a00ffffff00ffffff00ffffff00ffffff00e5e3e500
cccccc000000000033343400efeded00252526000000000056565600ffffff00ffffff00ffffff00ffffff00ffffff00e3e3e300
cccccc000000000033343400656565000000000000000000dddddd00ffffff00ffffff00ffffff00ffffff00ffffff00e3e3e300
cccccc000000000012121200000000000000000000000000aaaaab00ffffff00ffffff00ffffff00ffffff00ffffff00e3e3e300
cccccc00000000000000000000000000000000000000000044444500ffffff00ffffff00ffffff00ffffff00ffffff00e3e3e300
cccccc000000000000000000252526009a999a000000000000000000cccccc00ffffff00ffffff00ffffff00ffffff00e3e3e300
cccccc0000000000000000009a999a00ffffff00252526000000000073737300ffffff00ffffff00ffffff00ffffff00e3e1e300
cccccc000000000033343400ffffff00ffffff00737373000101010012121200ffffff00ffffff00ffffff00ffffff00e3e1e300
cccccc000000000033343400ffffff00ffffff00dddddd00000000000000000088888800ffffff00ffffff00ffffff00e1e1df00
cccccc000000000033343400ffffff00ffffff00ffffff00444445000000000033343400ffffff00ffffff00ffffff00e1e1df00
cccccc000000000033343400ffffff00ffffff00ffffff009a999a000000000000000000bbbbbb00ffffff00ffffff00e1e1e100
cccccc000000000033343400ffffff00ffffff00ffffff00ffffff00121212000000000056565600ffffff00ffffff00e1e1e100
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e1dfe100
ffffff00ffffff00ffffff00ffffff00ffffff00ededed00b2b2b200fdfdfd00ffffff00ffffff00ffffff00ffffff00e1dfe100
ffffff00ffffff00ffffff00ffffff00e3e1e3001b1b1c000000000065656500fbfbfb00ffffff00ffffff00ffffff00e1dfe100
ffffff00ffffff00ffffff00e3e1e3000303030000000000000000000000000044444500ffffff00ffffff00ffffff00dfdfdf00
ffffff00ffffff00f7f9f7001b1b1c0000000000000000000000000000000000010101007d7d7d00ffffff00ffffff00dfdfdd00
ffffff00ffffff007373730000000000000000000000000000000000000000000000000000000000d7d7d700ffffff00dddddd00
ffffff00e9e7e900000000000000000000000000000000000000000000000000000000000000000044444500ffffff00dfdddf00
ffffff00a2a2a200000000000000000000000000000000000000000000000000000000000000000000000000ffffff00dfdddf00
ffffff0065656500000000000000000000000000565656009292920025252600000000000000000000000000ffffff00dddddd00
ffffff00d4d4d400010203000000000003030300c3c3c400bbbbbb0092929200000000000000000033343400ffffff00dbdbdd00
ffffff00ffffff00dfdfdf0088888800e1e1e100e5e5e50033343400fbfbfb00b2b2b200a2a2a200f5f5f500ffffff00dddddd00
ffffff00ffffff00ffffff00ffffff00ffffff00aaaaab001b1b1c00e9e9e900ffffff00ffffff00ffffff00ffffff00dddbdd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dbdbdb00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dbdbdb00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dbdbdb00
i$Ts               13  31 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00dddddd000000000088888800ffffff00ffffff00888888000000000033343400efedef00ffffff00ffffff00e5e3e500
ffffff009a999a000000000088888800ffffff00efedef0012121200000000000000000073737300ffffff00ffffff00e5e3e500
ffffff00333434000000000088888800ffffff009a999a0000000000aaaaab004444450025252600ffffff00ffffff00e5e3e500
65656500000000000000000088888800ffffff006565650012121200ffffff008888880000000000dddddd00ffffff00e5e3e500
00000000121212000000000088888800ffffff005656560012121200ffffff009a999a0000000000bbbbbb00ffffff00e5e3e500
00000000737373000000000088888800ffffff003334340033343400ffffff00aaaaab0000000000aaaaab00ffffff00e3e3e300
88888800aaaaab000000000088888800ffffff003334340033343400ffffff00aaaaab0000000000aaaaab00ffffff00e3e3e300
ffffff00aaaaab000000000088888800ffffff003334340033343400ffffff00aaaaab0000000000aaaaab00ffffff00e3e3e300
ffffff00aaaaab000000000088888800ffffff003334340033343400ffffff00aaaaab0000000000aaaaab00ffffff00e3e3e300
ffffff00aaaaab000000000088888800ffffff003334340033343400ffffff00aaaaab0000000000aaaaab00ffffff00e3e3e300
ffffff00aaaaab000000000088888800ffffff005656560025252600ffffff009a999a0000000000bbbbbb00ffffff00e3e3e300
ffffff00aaaaab000000000088888800ffffff006565650012121200ffffff008888880000000000dddddd00ffffff00e3e3e300
ffffff00aaaaab000000000088888800ffffff009a999a0000000000aaaaab005656560025252600ffffff00ffffff00e3e1e300
ffffff00aaaaab000000000088888800ffffff00efedef0012121200000000000000000073737300ffffff00ffffff00e3e1e300
ffffff00aaaaab000000000088888800ffffff00ffffff00888888000000000033343400efedef00ffffff00ffffff00e3e1e300
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e1e1e100
ffffff00ffffff00ffffff00ffffff00fdfdfd00b2b2b200efedef00ffffff00ffffff00ffffff00ffffff00ffffff00e1e1e100
ffffff00ffffff00ffffff00fbfbfb0065656500000000001b1b1c00e3e3e300ffffff00ffffff00ffffff00ffffff00e1e1e100
ffffff00ffffff00fdfdfd004444450000000000000000000000000006060600e5e5e500ffffff00ffffff00ffffff00e1e1e100
ffffff00ffffff007373730000000000000000000000000000000000000000001b1b1c00f9f9f900ffffff00ffffff00e1dfe100
ffffff00d4d4d400000000000000000000000000000000000000000000000000000000007d7d7d00ffffff00ffffff00dfdfdd00
ffffff00444445000000000000000000000000000000000000000000000000000000000000000000ebebeb00ffffff00e1dfe100
ffffff00000000000000000000000000000000000000000000000000000000000000000000000000a2a2a200ffffff00e1dfe100
ffffff0000000000000000000000000025252600929292005656560000000000000000000101010065656500ffffff00dfdfdf00
ffffff003334340000000000000000009a999a00b2b2b200c3c3c400010203000000000003030300d7d7d900ffffff00dfdfdf00
ffffff00f5f5f5009a999a00b2b2b200fdfdfd0033343400e5e5e500e1dfe10088888800e1e1e100ffffff00ffffff00dfdfdf00
ffffff00ffffff00ffffff00ffffff00e5e7e70025252600aaaaab00ffffff00ffffff00ffffff00ffffff00ffffff00dfdddf00
ffffff00ffffff00ffffff00ffffff00ffffff00fdfdfd00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dfdddf00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dfdddf00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dfdddf00
i$5s               13  31 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00444445000000000000000000000000000000000088888800ffffff00ffffff00ffffff00e5e3e500
ffffff00ffffff00ffffff00252526000000000000000000000000000000000088888800ffffff00ffffff00ffffff00e5e3e500
ffffff00ffffff00ffffff001212120012121200ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00ffffff00cccccc000000000033343400ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00ffffff00aaaaab000000000056565600ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff008888880000000000121212000000000012121200aaaaab00ffffff00ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff00656565000000000000000000000000000000000012121200dddddd00ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff00737373000000000088888800ffffff00444445000000000073737300ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00aaaaab000000000033343400ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00cccccc000000000012121200ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00cccccc000000000025252600ffffff00ffffff00ffffff00e3e1e300
ffffff00ffffff002525260000000000cccccc00ffffff009a999a000000000044444500ffffff00ffffff00ffffff00dfdfdf00
ffffff00ffffff00444445000000000065656500ffffff00444445000000000088888800ffffff00ffffff00ffffff00e3e1e300
ffffff00ffffff00bbbbbb000000000000000000000000000000000025252600ffffff00ffffff00ffffff00ffffff00e1e1e100
ffffff00ffffff00ffffff009a999a00121212000000000044444500dddddb00ffffff00ffffff00ffffff00ffffff00e1e1e100
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e1e1e100
ffffff00ffffff00ffffff00ffffff00ffffff00fbf9f900b2b2b200f5f5f500ffffff00ffffff00ffffff00ffffff00e1dfe100
ffffff00ffffff00ffffff00ffffff00f7f7f700444445000000000033343400edebed00ffffff00ffffff00ffffff00e1dfe100
ffffff00ffffff00ffffff00f9f7f9003334340000000000000000000000000012121200f1eff100ffffff00ffffff00e1dfe100
ffffff00ffffff00ffffff0056565600000000000000000000000000000000000000000033343400fdfdfd00ffffff00dfdfdf00
ffffff00ffffff00bbbbbb00000000000000000000000000000000000000000000000000000000009a999a00ffffff00dfdfdf00
ffffff00f9f7f90025252600000000000000000000000000000000000000000000000000000000000a0a0a00f3f3f300dfdfdf00
ffffff00cccccc00000000000000000000000000000000000000000000000000000000000000000000000000bbbbbb00dfdddf00
ffffff009a999a0000000000000000000000000033343400929292004444450000000000000000000000000088888800dfdddf00
ffffff00ededed001b1b1c000000000000000000aaaaab00b2b2b200bbbbbb0000000000000000000a0a0a00e3e1e300dfdddf00
ffffff00ffffff00f1f1f20092929200c3c3c400f9f9f90025252600f1f1f200d4d4d40092929200e9e7e900fdfdfd00dddddd00
ffffff00ffffff00ffffff00ffffff00ffffff00dbdbdb0025252600c3c3c400fdfdfd00fdfdfd00ffffff00fdfdfd00dddddd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fffdfd00ffffff00ffffff00ffffff00ffffff00fdfdfd00dbdddd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fdfdfd00dddbdd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fdfdfd00dddbdd00
i$8c               13  31 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00efedef0044444500000000000000000073737300ffffff00ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00ffffff00444445000000000000000000000000000000000088888800ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00dddddd000000000012121200dddddd00888888000000000025252600ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00aaaaab000000000033343400ffffff00efedef000000000012121200ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00bbbbbb000000000033343400ffffff00dddddd000000000012121200ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00ffffff001212120012121200dddddd00888888000000000056565600ffffff00ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff00aaaaab0000000000000000000000000033343400efedef00ffffff00ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff008888880000000000000000000000000012121200cccccc00ffffff00ffffff00ffffff00ffffff00e3e3e300
ffffff00dddddd000000000012121200efedef009a999a000000000033343400ffffff00ffffff00ffffff00ffffff00e3e3e300
ffffff009a999a000000000065656500ffffff00ffffff001212120000000000dddddd00ffffff00ffffff00ffffff00e3e3e300
ffffff00888888000000000073737300ffffff00ffffff001212120000000000cccccc00ffffff00ffffff00ffffff00e3e1e300
ffffff00888888000000000065656500ffffff00ffffff001212120000000000dddddd00ffffff00ffffff00ffffff00e3e1e300
ffffff00bbbbbb000000000012121200dddddd009a999a000000000025252600ffffff00ffffff00ffffff00ffffff00e3e1e300
ffffff00ffffff00444445000000000000000000000000000000000088888800ffffff00ffffff00ffffff00ffffff00e1e1e100
ffffff00ffffff00efedef0044444500000000000000000088888800ffffff00ffffff00ffffff00ffffff00ffffff00e1e1e100
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e1e1e100
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e1e1df00
ffffff00ffffff00ffffff00ffffff00fbf9f9007d7d7d0056565600e1dfe100ffffff00ffffff00ffffff00ffffff00e1dfe100
ffffff00ffffff00ffffff00ffffff0073737300000000000000000001020300f1f1f200ffffff00ffffff00ffffff00e1dfe100
ffffff00ffffff00ffffff00fdfdfd001b1b1c00000000000000000000000000bbbbbb00ffffff00ffffff00ffffff00dfdfdf00
ffffff00ffffff00ffffff00fdfdfd0056565600000000000000000000000000e1e1e100ffffff00ffffff00ffffff00dfdddf00
ffffff00fdfdfd00a2a2a20073737300888888001b1b1c0003030300737373008888880073737300f1eff100ffffff00dfdfdf00
ffffff009292920000000000000000000a0a0a0006060600000000001b1b1c00000000000000000025252600ffffff00dfdddf00
ffffff001b1b1c0001010100000000000000000025252600565656000000000000000000000000000e0e0f00ffffff00dfdddf00
ffffff00444445000000000000000000000000009a999a00aaaaab002525260000000000000000000a0a0a00ffffff00dfdddf00
ffffff00e7e7e700333434000e0e0f0088888800dddddd007d7d7d00d0d0d0001b1b1c0012121200aaaaab00ffffff00dddddd00
ffffff00ffffff00fbf9f900e9e9e900ffffff00b2b2b20025252600fdfdfd00f5f3f500ededed00ffffff00ffffff00dddddd00
ffffff00ffffff00ffffff00ffffff00ffffff00fffdfd00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dddddd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dddbdd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dddbdd00
i$Ac               13  31 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00121212000000000012121200ffffff00ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00ffffff00ffffff00bbbbbb00000000000000000000000000bbbbbb00ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00ffffff00ffffff008888880000000000000000000000000088888800ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00ffffff00ffffff004444450000000000333434000000000044444500ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00ffffff00ffffff001212120000000000888888000000000012121200ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00ffffff00bbbbbb000000000012121200efedef001212120000000000bbbbbb00ffffff00ffffff00ffffff00e5e3e500
ffffff00ffffff00888888000000000044444500ffffff00444445000000000088888800ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff00444445000000000088888800ffffff00888888000000000044444500ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff001212120000000000bbbbbb00ffffff00bbbbbb000000000012121200ffffff00ffffff00ffffff00e3e3e300
ffffff00bbbbbb0000000000000000000000000000000000000000000000000000000000bbbbbb00ffffff00ffffff00e3e3e300
ffffff00888888000000000000000000000000000000000000000000000000000000000088888800ffffff00ffffff00e3e3e300
ffffff00444445000000000000000000000000000000000000000000000000000000000044444500ffffff00ffffff00e3e3e300
ffffff0012121200000000009a999a00ffffff00ffffff00ffffff009a999a000000000012121200ffffff00ffffff00e3e3e300
bbbbbb000000000000000000dddddd00ffffff00ffffff00ffffff00bbbbbb000000000000000000dddddd00ffffff00e3e1e300
888888000000000025252600ffffff00ffffff00ffffff00ffffff00ffffff0012121200000000009a999a00ffffff00e3e1e300
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e3e1e300
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e1e1e100
ffffff00ffffff00ffffff00ffffff00fbf9f9007d7d7d0056565600e3e3e300ffffff00ffffff00ffffff00ffffff00e1e1e100
ffffff00ffffff00ffffff00ffffff0065656500000000000000000012121200f5f5f500ffffff00ffffff00ffffff00e1e1e100
ffffff00ffffff00ffffff00f5f5f50000000000000000000000000000000000bbbbbb00ffffff00ffffff00ffffff00e1e1e100
ffffff00ffffff00ffffff00fdfdfd003334340000000000000000000a0a0a00e5e5e500ffffff00ffffff00ffffff00dfdfdd00
ffffff00ffffff009a999a0073737300929292001b1b1c0003030300737373008888880073737300f5f3f500ffffff00dfe1e100
ffffff0092929200000000000000000006060600060606000000000012121200000000000000000033343400fdfdfd00e1dfe100
ffffff001b1b1c0001010100000000000000000025252600565656000101010000000000000000000a0a0a00ffffff00e1dfe100
ffffff00444445000000000000000000000000007d7d7d00a2a2a2002525260000000000000000000a0a0a00ffffff00e1dfe100
ffffff00e9e9e900252526000e0e0f0092929200cccccc0073737300d0d0d0001b1b1c0012121200b2b2b200ffffff00dfdfdf00
ffffff00ffffff00fbfbfb00ebebeb00fffdfd00aaaaab001b1b1c00ffffff00f3f5f300f1eff100ffffff00ffffff00dfdfdf00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dfdfdf00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dfdfdf00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dfdfdf00
i$dealer           4   4  
ffffff00fefdfc00fdfaf500fdfaf600
ffffff00f4e0c400da963800db993e00
ffffff00f0d7b500ce770400ce760400
ffffff00f0d6b500cb720400cb700400
i$9h               13  31 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff005c5cff000000ff001414ff009b9bfe00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff007474fe000000ff000000ff000000ff000000ff00cbcbff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00eeeefd001414ff001414ff00ddddff009b9bfe000000ff005c5cff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00acacfe000000ff003d3dff00ffffff00ffffff001414ff000a0aff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff008888fe000000ff005c5cff00ffffff00ffffff003d3dff000000ff00ddddff00ffffff00ffffff00ffffff00c9c9ff00
ffffff008888fe000000ff005c5cff00ffffff00ffffff002525ff000000ff00b9b9fe00ffffff00ffffff00ffffff00c9c9ff00
ffffff00acacfe000101ff003d3dff00fdfdff00ffffff002525ff000000ff00acacfe00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ddddff000000ff000101ff00b9b9fe00b9b9fe000000ff000000ff00acacfe00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff005c5cff000404ff000000ff000000ff000000ff000000ff00acacfe00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff003d3dff000101ff003d3dff005c5cff000000ff00b9b9fe00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff003d3dff000000ff00ddddff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00eeeefd003d3dff002525ff00ffffff00ffffff002525ff001414ff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff001414ff000000ff00b9b9fe009b9bfe000000ff005c5cff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00fbfbff005c5cff000000ff000000ff000000ff001414ff00e2e2fe00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00eeeefd003d3dff000000ff002525ff00acacfe00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00e2e2fe007474fe007474fe00e2e2fe00ffffff00c9c9ff005c5cff008888fe00f9f9ff00ffffff00ffffff00c7c7fd00
f9f9ff003d3dff000000ff000000ff003d3dff00c2c2fa001414ff000101ff000404ff007474fe00ffffff00ffffff00c7c7fd00
b9b9fe000404ff000101ff000000ff000000ff001414ff000000ff000000ff000000ff001414ff00ffffff00ffffff00c7c7fd00
b9b9fe000000ff000000ff000000ff000000ff000101ff000000ff000000ff000000ff001414ff00ffffff00ffffff00c7c7fd00
eaeaff002525ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff005c5cff00ffffff00ffffff00c7c7fd00
fdfdff009b9bfe000000ff000000ff000000ff000000ff000000ff000000ff000404ff00c5c5fc00fdfdff00ffffff00c7c7fd00
ffffff00fdfdff005c5cff000404ff000000ff000000ff000000ff000000ff009b9bfe00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00fbfbff005c5cff000000ff000000ff000000ff008888fe00ffffff00ffffff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00fdfdff00eaeaff003d3dff000101ff005c5cff00fdfdff00ffffff00ffffff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00ffffff00ffffff00cdcdff003d3dff00f7f7ff00ffffff00ffffff00ffffff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00ffffff00ffffff00ffffff00d4d4fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c5c5fc00
i$Qs               13  31 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff009a999a0025252600000000000000000088888800ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00ffffff009a999a00000000000000000000000000000000000000000065656500ffffff00ffffff00ffffff00e5e3e500
ffffff00efedef0012121200000000000000000000000000000000000000000000000000cccccc00ffffff00ffffff00e5e3e500
ffffff00888888000000000012121200aaaaab00ffffff00cccccc00252526000000000065656500ffffff00ffffff00e5e3e500
ffffff00565656000000000088888800ffffff00ffffff00ffffff00aaaaab000000000025252600ffffff00ffffff00e3e3e300
ffffff002525260000000000dddddd00ffffff00ffffff00ffffff00ffffff001212120012121200ffffff00ffffff00e3e3e300
ffffff001212120012121200ffffff00ffffff00ffffff00ffffff00ffffff002525260000000000efedef00ffffff00e3e3e300
ffffff001212120012121200ffffff00ffffff00ffffff00ffffff00ffffff003334340000000000efedef00ffffff00e3e3e300
ffffff001212120012121200ffffff00ffffff00ffffff00ffffff00ffffff002525260000000000efedef00ffffff00e3e3e300
ffffff002525260000000000dddddd00ffffff00888888009a999a00dddddd000000000012121200ffffff00ffffff00e3e1e300
ffffff00565656000000000073737300ffffff004444450000000000333434000000000025252600ffffff00ffffff007d7d7d00
ffffff00888888000000000012121200aaaaab00cccccc0025252600000000000000000073737300ffffff00ffffff0033343400
ffffff00efedef0012121200000000000000000000000000000000000000000000000000cccccc00ffffff00ffffff0033343400
ffffff00ffffff009a999a0000000000000000000000000000000000000000000000000044444500ffffff00ffffff0044444500
ffffff00ffffff00ffffff009a999a00252526000000000000000000737373000000000000000000aaaaab00ffffff00a2a2a200
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00cccccc0025252600ffffff00ffffff00e1e1df00
ffffff00ffffff00ffffff00ffffff00ffffff00d7d7d700c3c3c400ffffff00ffffff00ffffff00ffffff00ffffff00e1e3e100
ffffff00ffffff00ffffff00ffffff00c3c3c40001010100000000009a999a00ffffff00ffffff00ffffff00ffffff00e1dfe100
ffffff00ffffff00ffffff00bbbbbb000000000000000000000000000000000088888800ffffff00ffffff00ffffff00dfdfdf00
ffffff00ffffff00e1e1e100000000000000000000000000000000000000000000000000bbbbbb00ffffff00ffffff00d9d9d900
ffffff00fdfdfd003334340000000000000000000000000000000000000000000000000006060600f5f3f500ffffff009a999a00
ffffff00c3c3c400000000000000000000000000000000000000000000000000000000000000000088888800ffffff0065656500
ffffff0056565600010203000000000000000000000000000000000000000000000000000000000025252600ffffff00dbdbdb00
ffffff0025252600000000000000000000000000737373008888880003030300000000000000000003030300ffffff00dfdddf00
ffffff00aaaaab00000000000000000025252600cccccc00bbbbbb0065656500000000000000000073737300ffffff00dddbdd00
ffffff00ffffff00cccccc0088888800f5f5f500b2b2b20073737300fbf9f9009a999a00b2b2b200fbfbfb00ffffff0073737300
ffffff00ffffff00ffffff00ffffff00ffffff007373730033343400f9f9f900ffffff00ffffff00ffffff00ffffff0033343400
ffffff00ffffff00ffffff00ffffff00ffffff00fdfdfd00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff0033343400
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff0044444500
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff0044444500
i$8s               13  31 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00efedef0044444500000000000000000073737300ffffff00ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00ffffff00444445000000000000000000000000000000000088888800ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00dddddd000000000012121200dddddd00888888000000000025252600ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00aaaaab000000000033343400ffffff00efedef000000000012121200ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00bbbbbb000000000033343400ffffff00dddddd000000000012121200ffffff00ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff001212120012121200dddddd00888888000000000056565600ffffff00ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff00aaaaab0000000000000000000000000033343400efedef00ffffff00ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff008888880000000000000000000000000012121200cccccc00ffffff00ffffff00ffffff00ffffff00e3e3e300
ffffff00dddddd000000000012121200efedef009a999a000000000033343400ffffff00ffffff00ffffff00ffffff00e3e3e300
ffffff009a999a000000000065656500ffffff00ffffff001212120000000000dddddd00ffffff00ffffff00ffffff00e3e3e300
ffffff00888888000000000073737300ffffff00ffffff001212120000000000cccccc00ffffff00ffffff00ffffff00e3e1e300
ffffff00888888000000000065656500ffffff00ffffff001212120000000000dddddd00ffffff00ffffff00ffffff00e1dfdf00
ffffff00bbbbbb000000000012121200dddddd009a999a000000000025252600ffffff00ffffff00ffffff00ffffff00e3e1e300
ffffff00ffffff00444445000000000000000000000000000000000088888800ffffff00ffffff00ffffff00ffffff00e1e1e100
ffffff00ffffff00efedef0044444500000000000000000088888800ffffff00ffffff00ffffff00ffffff00ffffff00e1e1e100
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e1e1e100
ffffff00ffffff00ffffff00ffffff00ffffff00e5e3e500bbbbbb00ffffff00ffffff00ffffff00ffffff00ffffff00e1dfe100
ffffff00ffffff00ffffff00ffffff00d4d4d4000e0e0f00000000007d7d7d00fdfdfd00ffffff00ffffff00ffffff00e1dfe100
ffffff00ffffff00ffffff00d4d4d4000000000000000000000000000000000065656500ffffff00ffffff00ffffff00e1dfe100
ffffff00ffffff00f1eff1000a0a0a0000000000000000000000000000000000000000009a999a00ffffff00ffffff00dfdfdf00
ffffff00ffffff005656560000000000000000000000000000000000000000000000000000000000e7e7e700ffffff00dfdfdf00
ffffff00dbdbdb00000000000000000000000000000000000000000000000000000000000000000065656500ffffff00dfdfdf00
ffffff007d7d7d0001010100000000000000000000000000000000000000000000000000000000000a0a0a00ffffff00dfdddf00
ffffff0044444500000000000000000000000000656565008888880012121200000000000000000000000000ffffff00dfdddf00
ffffff00c3c3c40000000000000000000e0e0f00cccccc00bbbbbb007d7d7d00000000000101010044444500ffffff00dfdddf00
ffffff00ffffff00d7d7d70088888800ebebeb00d4d4d40044444500fffdfd00aaaaab00aaaaab00fbf9f900ffffff00dddddd00
ffffff00ffffff00ffffff00ffffff00ffffff009292920025252600f1f1f200fffdfd00ffffff00ffffff00ffffff00dddddd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dddddd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dddbdd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dddbdd00
i$prefold          43  13 
62626200626262006262620062626200626262006262620062626200626262006262620062626200626262006262620062626200626262006262620062626200626262006262620062626200626262006262620062626200626262006262620062626200626262006262620062626200626262006262620062626200626262006262620062626200626262006262620062626200626262006262620062626200626262006262620062626200
58585800585858005e5b56008e784300c4982e00ca9d2c00c99f2d00c9a12e00c9a43000caa73100cbab3200caaf3400cab23600c2af3a007d774e0059595800585858005858580058585800606057009b9b5000afaf520063635700595958009d9f6900c5c77e00c4c68800c2c39000c1c29200c1c29200c1c29200b6b78c007d7d6c005b5b590083837000b6b68c00bfbf9100bfbf9100bfbf9100bfbf9100b8b88d008e8f760061615d00
4e4e4e004e4e4d0076664100ebae1b00ca9a2500bd922a00bd932b00bd952b00bd972c00bd9b2f00bd9e3000bda13100bfa63200eacd2a00ac9d390052514d004e4e4e004e4e4e0053534d009f9e4500f2f03f00fbfa430068684c007d7e5200eff36c00b7ba6e00b3b57700b2b27f00b0b18500b0b18700b1b28700dcdda000afb0860070706100c8c99500c7c89300b5b68900b5b68900b5b68900b5b68900c8c89400e4e5a40089897000
42424200454441009b7a2d00c59522004a4740004242420042424200424242004242420042424200424242004242420059543e00ccb12c008075380044444200424242004747420088843e00cdc73900beba3d00d0cf400053534200bdbe4c00b8ba5400424242004242420042424200424242004242420047474500cfd0980085856a007f806700b7b888005e5e5300424242004242420042424200424242004c4c48009b9c780082836900
363636003d3b3500c4931f009c792400363636003636360036363600363636003636360036363600363636003636360078692f00cdad26005952330036363600393836006f693500c8bc3100847f3400aba73800a5a338004a493700efef45007a7b3f00363636003636360036363600363636003636360064645300dbdc9e0055564a00939371009b9c76003f403c003636360036363600363636003636360044443f00999a7500797a6100
19191900382f1a00dda21600664f17002e281b002e281b002e281b002e281b002f281b002d281a001e1c19001e1d19008e741d00a98a1b002f2b1900191919004b452000c1ad280080761c00413e1e00bbb32d00646120005c5a2500f6f43e00444422002f2f20002f2f21002f2f22002f2f24002f2f260098996c00c2c28b0048483a00aeae7f007a7b5b002e2e28002e2e28002e2e28002e2e28003b3b31007d7e5f00adae7e004d4d3b00
000000003a2a0400f1b01400bc890f00ae7f0e00ae7f0e00ae7f0e00ae7f0e00af7f0e00a87b0d005a43070019130200a4801100745b0d00090701001b160300bda11a00907e1600141203004c460d00b8ad230033300a008d891f00faf53b00b2b12c00afb03000aeb03900adb04400abae5000a9ac5c00dadc8800d8d99400b1b27e00dbdc9c00bebf8800a4a57600a5a57600a5a57600a5a57600aaab7900a1a2730053533b000b0b0800
000000000e0a0100553e070060460700604607006046070060460700604607005e450700c48f1000946d0c003a2c0500b489120049380700100d0100b4931500ad9016001613030003030000736914009d901c00211e0600cfc62b00989422005f5e17006060190060601d005f6022005d5f290082844200eff288006a6a4500a2a37100bcbd8600616246005a5a40005a5a40005a5a4000595a40004a4a3500232319000707050000000000
000000000000000002010000030200000302000003020000030200000302000003020000ce961000614707005a430800aa7f0f00291f030099781100cfa61800110e020000000000161303009c8a19007165120038340a00e6d92c001d1c060003030000030300000303010003030100030301006e713000cfd3680012120b009a9c640081825900040403000303020003030200030302000303020002020100000000000000000000000000
000000000000000000000000000000000000000000000000000000000000000037270400e3a512002a1f03007f5c0a008e680c007c5e0b00cd9d1500221a04000000000000000000372f0700b69e1b0050460c00786e1400c2b52300000000000000000000000000000000000000000002020000b4b6420085873a0017180d00c4c7720050503200000000000000000000000000000000000000000000000000000000000000000000000000
1d1502002a1e03002a1e03002a1e03002a1e03002a1e03002a1e03002a1e0300936b0c00bf8b100023190200ac7d0e00c5901000c49111004735060002010000000000000201000057480b00bfa11a0054480c00b9a51d007e721500000000000000000000000000000000000000000025250900dadb440055571e0055572500c1c562001c1c1000000000000000000000000000000000000000000000000000000000000000000000000000
825f0a00b4830f00b4830f00b4830f00b4830f00b4830f00b4830f00b3830f00c791100058400700241b0300ab7d0e00c59010005b4307000907000000000000000000000403000054450a00c8a51a00ba9e1a00baa21c00302b0700000000000000000000000000000000000000000030300c00bdbd3400babc3b00bcbf48007477350003030100000000000000000000000000000000000000000000000000000000000000000000000000
402f0500523c0600523c0600523c0600523c0600523c0600523c0600523c06003c2c050005040000080600003023040031240400090700000000000000000000000000000000000015110200443708004f420a0031290600040300000000000000000000000000000000000000000000080802003b3b0f0051511700454517001010060000000000000000000000000000000000000000000000000000000000000000000000000000000000
i$Kh               13  31 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
cbcbff000000ff003d3dff00fbfbff00ffffff00ffffff008888fe000000ff000000ff009b9bfe00ffffff00ffffff00c9c9ff00
cbcbff000000ff002525ff00ffffff00ffffff00eeeefd001414ff000000ff005c5cff00ffffff00ffffff00ffffff00c9c9ff00
cbcbff000000ff003d3dff00ffffff00fdfdff005c5cff000000ff002525ff00eaeaff00ffffff00ffffff00ffffff00c9c9ff00
cbcbff000000ff002525ff00ffffff009b9bfe000000ff000000ff009b9bfe00ffffff00ffffff00ffffff00ffffff00c9c9ff00
cbcbff000101ff003d3dff00eaeaff002525ff000000ff005c5cff00fbfbff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
cbcbff000000ff003d3dff005c5cff000000ff000000ff00dadaff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
cbcbff000000ff000a0aff000000ff000101ff000000ff00acacfe00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
cbcbff000000ff000000ff000000ff000000ff000000ff003d3dff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
cbcbff000000ff000000ff002525ff009b9bfe000000ff000101ff00cbcbff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
cbcbff000000ff000000ff009b9bfe00ffffff002525ff000000ff007474fe00ffffff00ffffff00ffffff00ffffff00c9c9ff00
cbcbff000000ff003d3dff00ffffff00ffffff007474fe000000ff001414ff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
cbcbff000000ff002525ff00ffffff00ffffff00ddddff000000ff000000ff008888fe00ffffff00ffffff00ffffff00c9c9ff00
cbcbff000101ff003d3dff00fdfdff00ffffff00ffffff003d3dff000101ff003d3dff00fbfbff00ffffff00ffffff00c9c9ff00
cbcbff000101ff002525ff00ffffff00ffffff00ffffff009b9bfe000000ff000000ff00b9b9fe00ffffff00ffffff00c7c7fd00
cbcbff000000ff003d3dff00ffffff00ffffff00ffffff00ffffff001414ff000000ff005c5cff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00fdfdff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fdfdff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ddddff005c5cff007474fe00eeeefd00ffffff00b9b9fe005c5cff009b9bfe00ffffff00ffffff00c7c7fd00
ffffff00f3f3ff002525ff000101ff000101ff005c5cff00b9b9fe000404ff000000ff000000ff009b9bfe00ffffff00c7c7fd00
ffffff00acacfe000404ff000000ff000000ff000101ff001414ff000101ff000000ff000000ff002525ff00ffffff00c7c7fd00
ffffff00acacfe000000ff000000ff000000ff000000ff000101ff000000ff000000ff000101ff002525ff00ffffff00c7c7fd00
ffffff00e2e2fe000a0aff000000ff000000ff000000ff000000ff000000ff000000ff000101ff007474fe00ffffff00c5c5fc00
ffffff00ffffff007474fe000000ff000000ff000000ff000000ff000000ff000000ff001414ff00dadaff00ffffff00c5c5fc00
ffffff00ffffff00fbfbff005c5cff000000ff000000ff000000ff000000ff000404ff00b9b9fe00ffffff00ffffff00c5c5fc00
ffffff00ffffff00fbfbff00eeeefd003d3dff000000ff000000ff000000ff009b9bfe00fdfdff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00ffffff00fdfdff00e2e2fe002525ff000000ff008888fe00ffffff00ffffff00ffffff00ffffff00c2c2fa00
ffffff00ffffff00ffffff00ffffff00ffffff00b9b9fe003d3dff00fdfdff00ffffff00ffffff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00ffffff00ffffff00ffffff00f5f5ff00ddddff00ffffff00ffffff00ffffff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c5c5fc00
i$Tc               13  31 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00dddddd000000000088888800ffffff00ffffff00888888000000000033343400efedef00ffffff00ffffff00e5e3e500
ffffff009a999a000000000088888800ffffff00efedef0012121200000000000000000073737300ffffff00ffffff00e5e3e500
ffffff00333434000000000088888800ffffff009a999a0000000000aaaaab004444450025252600ffffff00ffffff00e5e3e500
65656500000000000000000088888800ffffff006565650012121200ffffff008888880000000000dddddd00ffffff00e5e3e500
00000000121212000000000088888800ffffff005656560012121200ffffff009a999a0000000000bbbbbb00ffffff00e5e3e500
00000000737373000000000088888800ffffff003334340033343400ffffff00aaaaab0000000000aaaaab00ffffff00e3e3e300
88888800aaaaab000000000088888800ffffff003334340033343400ffffff00aaaaab0000000000aaaaab00ffffff00e3e3e300
ffffff00aaaaab000000000088888800ffffff003334340033343400ffffff00aaaaab0000000000aaaaab00ffffff00e3e3e300
ffffff00aaaaab000000000088888800ffffff003334340033343400ffffff00aaaaab0000000000aaaaab00ffffff00e3e3e300
ffffff00aaaaab000000000088888800ffffff003334340033343400ffffff00aaaaab0000000000aaaaab00ffffff00e3e3e300
ffffff00aaaaab000000000088888800ffffff005656560025252600fffdfd009a999a0000000000bbbbbb00ffffff00e3e3e300
ffffff00aaaaab000000000088888800ffffff006565650012121200ffffff008888880000000000dddddd00ffffff00e3e1e300
ffffff00aaaaab000000000088888800ffffff009a999a0000000000aaaaab005656560025252600ffffff00ffffff00e3e1e300
ffffff00aaaaab000000000088888800ffffff00efedef0012121200000000000000000073737300ffffff00ffffff00e3e1e300
ffffff00aaaaab000000000088888800ffffff00ffffff00888888000000000033343400efedef00ffffff00ffffff00e3e1e300
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e1e1e100
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e1e1e100
ffffff00ffffff00ffffff00ffffff00fdfdfd008888880056565600d4d4d400ffffff00ffffff00ffffff00ffffff00e1e1e100
ffffff00ffffff00ffffff00ffffff0092929200000000000000000000000000e9e7e900ffffff00ffffff00ffffff00e1e1e100
ffffff00ffffff00ffffff00fdfdfd001b1b1c00000000000000000000000000b2b2b200ffffff00ffffff00ffffff00e1dfe100
ffffff00ffffff00ffffff00ffffff0065656500000000000000000000000000d7d7d700ffffff00ffffff00ffffff00dfdddf00
ffffff00ffffff00b2b2b200656565009a999a002525260001010100656565008888880065656500e9e7e900ffffff00dfdfdd00
ffffff00aaaaab000000000000000000030303000a0a0a00000000001b1b1c0000000000000000001b1b1c00fdfdfd00e1dfe100
ffffff004444450000000000000000000000000033343400565656000103010000000000000000000e0e0f00ffffff00dfdfdf00
ffffff0065656500000000000000000000000000a2a2a200b2b2b2003334340000000000000000000a0a0a00ffffff00dfdfdf00
ffffff00f1f1f200444445000e0e0f0073737300d7d7d70073737300e1dfe10025252600121212009a999a00ffffff00dfdfdf00
ffffff00ffffff00fbf9f900e7e7e700ffffff00bbbbbb0006060600ffffff00f7f7f700ededed00fdfdfd00ffffff00dfdddf00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dfdddf00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dfdddf00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dfdddf00
i$Js               13  31 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00565656000000000088888800ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00ffffff00ffffff00ffffff00ffffff00565656000000000088888800ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00ffffff00ffffff00ffffff00ffffff00565656000000000088888800ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00ffffff00ffffff00ffffff00ffffff00565656000000000088888800ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00ffffff00ffffff00ffffff00ffffff00565656000000000088888800ffffff00ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff00ffffff00ffffff00ffffff00565656000000000088888800ffffff00ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff00ffffff00ffffff00ffffff00565656000000000088888800ffffff00ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff00ffffff00ffffff00ffffff00565656000000000088888800ffffff00ffffff00ffffff00ffffff00e3e1e300
ffffff00ffffff00ffffff00ffffff00ffffff00565656000000000088888800ffffff00ffffff00ffffff00fdfdfd00e3e1e300
efedef000000000012121200ffffff00ffffff00565656000000000088888800ffffff00ffffff00ffffff00fdfdfd00e1e1e100
efedef000000000000000000dddddd00ffffff00565656000000000088888800ffffff00ffffff00ffffff00fdfdfd00e1e1e100
ffffff00121212000000000088888800ededed002525260000000000aaaaab00ffffff00ffffff00ffffff00fdfdfd00e1dfe100
ffffff00565656000000000000000000000000000000000000000000dddddd00ffffff00ffffff00ffffff00fdfdfd00dfdfdd00
ffffff00aaaaab000000000000000000000000000000000056565600ffffff00ffffff00ffffff00ffffff00fbfbfb00dfdfdf00
ffffff00ffffff0088888800000000000000000044444500ffffff00ffffff00ffffff00ffffff00ffffff00fbfbfb00dfdddf00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fbf9f900dddddb00
ffffff00ffffff00ffffff00ffffff00ffffff00c3c3c400dbdbdb00ffffff00ffffff00ffffff00ffffff00fbf9f900dbdddd00
ffffff00ffffff00ffffff00ffffff00929292000000000001020300c3c3c400ffffff00ffffff00ffffff00f9f9f900dddbdd00
ffffff00ffffff00ffffff008888880000000000000000000000000000000000bbbbbb00ffffff00ffffff00f9f9f900dbdbdb00
ffffff00ffffff00b2b2b200000000000000000000000000000000000000000000000000e5e3e500ffffff00f9f7f900d9d9d900
ffffff00f1f1f2000606060000000000000000000000000000000000000000000000000033343400fdfdfd00f9f7f900d9d9d900
ffffff00888888000000000000000000000000000000000000000000000000000000000000000000c3c3c400f7f7f700d9d9d900
ffffff001b1b1c00000000000000000000000000000000000000000000000000000000000000000065656500f7f7f700d9d9d900
ffffff0001020300000000000000000006060600888888007373730000000000000000000000000033343400f5f5f700d7d7d900
ffffff0065656500000000000000000065656500bbbbbb00c3c3c400252526000000000000000000aaaaab00f5f3f500d8d5d500
ffffff00fdfdfd00b2b2b2009a999a00fdfdfd0073737300bbbbbb00f1f1f20092929200cccccc00ffffff00f5f5f500d7d7d700
ffffff00ffffff00ffffff00ffffff00f9f7f9003334340073737300fdfdfd00fdfdfd00ffffff00ffffff00f5f3f500d7d7d900
ffffff00ffffff00ffffff00ffffff00ffffff00fdfdfd00fdfdfd00ffffff00ffffff00ffffff00ffffff00f5f3f500d4d4d400
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00f3f3f300d4d4d400
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00f3f3f300d4d4d400
i$Ad               13  31 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff001414ff000000ff001414ff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00b9b9fe000000ff000000ff000000ff00b9b9fe00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff008888fe000000ff000000ff000000ff008888fe00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff003d3dff000101ff003d3dff000000ff003d3dff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff001414ff000000ff008888fe000000ff001414ff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00b9b9fe000000ff001414ff00eeeefd001414ff000000ff00b9b9fe00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff008888fe000000ff003d3dff00ffffff003d3dff000000ff008888fe00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff003d3dff000404ff008888fe00ffffff008888fe000000ff003d3dff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff001414ff000000ff00b9b9fe00ffffff00b9b9fe000000ff001414ff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00b9b9fe000000ff000000ff000000ff000000ff000000ff000000ff000000ff00b9b9fe00ffffff00ffffff00c9c9ff00
ffffff008888fe000000ff000000ff000000ff000000ff000000ff000000ff000000ff008888fe00ffffff00ffffff00c9c9ff00
ffffff003d3dff000101ff000000ff000000ff000000ff000000ff000000ff000000ff003d3dff00ffffff00ffffff00c9c9ff00
ffffff001414ff000000ff009b9bfe00ffffff00ffffff00ffffff009b9bfe000000ff001414ff00ffffff00ffffff00c9c9ff00
b9b9fe000000ff000000ff00ddddff00ffffff00ffffff00ffffff00b9b9fe000000ff000000ff00ddddff00ffffff00c9c9ff00
8888fe000000ff002525ff00ffffff00ffffff00ffffff00ffffff00ffffff001414ff000000ff009b9bfe00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00e6e6ff007474fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00fbfbff003d3dff000404ff00b9b9fe00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff009b9bfe000404ff000000ff000101ff00dadaff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00cdcdff000000ff000000ff000000ff000101ff002525ff00f5f5ff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00e6e6ff000404ff000000ff000000ff000000ff000000ff000404ff005c5cff00ffffff00ffffff00ffffff00c7c7fd00
ffffff005c5cff000000ff000000ff000000ff000000ff000000ff000000ff000404ff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00e6e6ff000a0aff000000ff000000ff000000ff000000ff000000ff007474fe00fdfdff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00dadaff000404ff000000ff000000ff000000ff003d3dff00f7f7ff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00acacfe000000ff000000ff000a0aff00e2e2fe00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00ffffff005c5cff000000ff00c5c5fc00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00ffffff00eaeaff008888fe00fbfbff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00ffffff00ffffff00fdfdff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
i$7c               13  31 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff0088888800000000000000000000000000000000000000000000000000cccccc00ffffff00ffffff00ffffff00e5e3e500
ffffff0088888800000000000000000000000000000000000000000000000000cccccc00ffffff00ffffff00ffffff00e5e3e500
ffffff00ffffff00ffffff00ffffff00ffffff00aaaaab000000000025252600ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00ffffff00ffffff00ffffff00ffffff00333434000000000088888800ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00ffffff00ffffff00ffffff00cccccc000000000025252600ffffff00ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00ffffff00ffffff00ffffff00737373000000000073737300ffffff00ffffff00ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff00ffffff00ffffff002525260000000000bbbbbb00ffffff00ffffff00ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff00ffffff00dddddd000000000025252600ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff00ffffff009a999a000000000065656500ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff00ffffff00656565000000000088888800ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e1e3e100
ffffff00ffffff00ffffff004444450000000000bbbbbb00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e3e1e300
ffffff00ffffff00ffffff001212120000000000dddddd00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e3e1e300
ffffff00ffffff00efedef000000000012121200ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e3e1e300
ffffff00ffffff00dddddd000000000012121200ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e1e1e100
ffffff00ffffff00cccccc000000000033343400ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e1e1e100
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e1e1e100
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e1dfe100
ffffff00ffffff00ffffff00ffffff00c3c3c40044444500a2a2a200ffffff00ffffff00ffffff00ffffff00ffffff00e1dfe100
ffffff00ffffff00ffffff00d7d7d900000000000101010000000000a2a2a200ffffff00ffffff00ffffff00ffffff00e1dfe100
ffffff00ffffff00ffffff009292920000000000000000000000000033343400ffffff00ffffff00ffffff00ffffff00dfdfdf00
ffffff00ffffff00ffffff00c3c3c4000000000000000000000000007d7d7d00ffffff00ffffff00ffffff00ffffff00dfdfdf00
ffffff00dddfdd00656565009a999a005656560000000000333434009a999a0065656500bbbbbb00ffffff00ffffff00dfdfdf00
f1eff1000000000000000000000000001b1b1c000000000012121200000000000000000000000000cccccc00ffffff00dfdddf00
929292000000000000000000000000000e0e0f00444445001212120000000000000000000000000056565600ffffff00dfdddf00
cccccc0000000000000000000101010044444500a2a2a2006565650000000000000000000000000088888800ffffff00dddddd00
ffffff007d7d7d000e0e0f0033343400e7e7e70044444500e9ebeb00565656000e0e0f0044444500f9f9f900ffffff00dddddd00
ffffff00ffffff00ebebeb00f9f9f900fbfbfb0001010100e9ebeb00ffffff00e7e7e700fffdfd00ffffff00ffffff00dddddd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dddbdd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dddbdd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dddbdd00
i$7s               13  31 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff0088888800000000000000000000000000000000000000000000000000cccccc00ffffff00ffffff00ffffff00e5e3e500
ffffff0088888800000000000000000000000000000000000000000000000000cccccc00ffffff00ffffff00ffffff00e5e3e500
ffffff00ffffff00ffffff00ffffff00ffffff00aaaaab000000000025252600ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00ffffff00ffffff00ffffff00ffffff00333434000000000088888800ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00ffffff00ffffff00ffffff00cccccc000000000025252600ffffff00ffffff00ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff00ffffff00ffffff00737373000000000073737300ffffff00ffffff00ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff00ffffff00ffffff002525260000000000bbbbbb00ffffff00ffffff00ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff00ffffff00dddddd000000000025252600ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff00ffffff009a999a000000000065656500ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff00ffffff00656565000000000088888800ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e3e1e300
ffffff00ffffff00ffffff004444450000000000bbbbbb00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e3e1e300
ffffff00ffffff00ffffff001212120000000000dddddd00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff00efedef000000000012121200ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e3e1e300
ffffff00ffffff00dddddd000000000012121200ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e1e1e100
ffffff00ffffff00cccccc000000000033343400ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e1e1e100
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e1e1e100
ffffff00ffffff00ffffff00ffffff00ffffff00cccccc00cccccc00ffffff00ffffff00ffffff00ffffff00ffffff00e1dfe100
ffffff00ffffff00ffffff00ffffff00aaaaab000000000000000000aaaaab00ffffff00ffffff00ffffff00ffffff00e1dfe100
ffffff00ffffff00ffffff00a2a2a20000000000000000000000000000000000a2a2a200ffffff00ffffff00ffffff00e1dfe100
ffffff00ffffff00d0d0d000000000000000000000000000000000000000000000000000d0d0d000ffffff00ffffff00dfdfdf00
ffffff00fbf9f9001b1b1c000000000000000000000000000000000000000000000000001b1b1c00fbf9f900ffffff00dfdfdf00
ffffff00aaaaab000000000000000000000000000000000000000000000000000000000000000000aaaaab00ffffff00dfdfdf00
ffffff0044444500000000000000000000000000000000000000000000000000000000000000000044444500fffdfd00dfdddf00
ffffff00121212000000000000000000000000007d7d7d007d7d7d0000000000000000000000000012121200ffffff00dfdddf00
ffffff0088888800000000000000000044444500c3c3c400c3c3c40044444500000000000000000088888800ffffff00dddddd00
ffffff00ffffff00bbbbbb0092929200f9f9f9009a999a0092929200f9f9f90092929200bbbbbb00ffffff00ffffff00dddddd00
ffffff00ffffff00ffffff00ffffff00fdfdfd005656560044444500ffffff00ffffff00ffffff00ffffff00ffffff00dddbdd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dddbdd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dddbdd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dbdbdb00
i$7h               13  31 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff008888fe000000ff000000ff000000ff000000ff000000ff000000ff00cbcbff00ffffff00ffffff00ffffff00c9c9ff00
ffffff008888fe000000ff000000ff000000ff000000ff000000ff000000ff00cbcbff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00ffffff00acacfe000000ff002525ff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00ffffff003d3dff000000ff008888fe00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00c9c9ff000000ff002525ff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff007474fe000000ff007474fe00ffffff00ffffff00ffffff00ffffff00ffffff00cbcbff00
ffffff00ffffff00ffffff00ffffff002525ff000000ff00b9b9fe00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ddddff000000ff002525ff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff009b9bfe000000ff005c5cff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff005c5cff000101ff008888fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff003d3dff000101ff00b9b9fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff001414ff000101ff00ddddff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00eeeefd000000ff001414ff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ddddff000000ff001414ff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00cbcbff000000ff003d3dff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00ffffff00fbfbff00fdfdff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00acacfe005c5cff009b9bfe00ffffff00ffffff008888fe005c5cff00c2c2fa00ffffff00ffffff00c7c7fd00
ffffff00c5c5fc000101ff000101ff000000ff009b9bfe008888fe000101ff000404ff000a0aff00dadaff00ffffff00c7c7fd00
ffffff005c5cff000000ff000000ff000000ff001414ff000a0aff000000ff000000ff000000ff007474fe00ffffff00c7c7fd00
ffffff005c5cff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000101ff007474fe00ffffff00c7c7fd00
ffffff009b9bfe000000ff000000ff000000ff000000ff000000ff000000ff000000ff000101ff00b9b9fe00ffffff00c5c5fc00
ffffff00f3f3ff003d3dff000000ff000000ff000000ff000000ff000000ff000404ff003d3dff00fbfbff00ffffff00c5c5fc00
ffffff00ffffff00d4d4fe001414ff000000ff000000ff000000ff000000ff002525ff00eaeaff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00ffffff00c5c5fc000a0aff000000ff000000ff001414ff00dadaff00ffffff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00ffffff00ffffff00acacfe000000ff000a0aff00c5c5fc00ffffff00ffffff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00ffffff00ffffff00fdfdff007474fe008888fe00ffffff00ffffff00ffffff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00ffffff00ffffff00ffffff00e2e2fe00e6e6ff00ffffff00ffffff00ffffff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c5c5fc00
i$7d               13  31 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff008888fe000000ff000000ff000000ff000000ff000000ff000000ff00cbcbff00ffffff00ffffff00ffffff00c9c9ff00
ffffff008888fe000000ff000000ff000000ff000000ff000000ff000000ff00cbcbff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00ffffff00acacfe000000ff002525ff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00ffffff003d3dff000000ff008888fe00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00c9c9ff000000ff002525ff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff007474fe000000ff007474fe00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff002525ff000000ff00b9b9fe00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ddddff000000ff002525ff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff009b9bfe000101ff005c5cff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff005c5cff000000ff008888fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff003d3dff000404ff00b9b9fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff001414ff000000ff00ddddff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00eeeefd000000ff001414ff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ddddff000000ff001414ff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00cbcbff000000ff003d3dff00fbfbff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00fdfdff00fbfbff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00f5f5ff007474fe00f3f3ff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff009b9bfe000000ff007474fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00c5c5fc000000ff000000ff000101ff00c5c5fc00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00eaeaff000a0aff000000ff000000ff000000ff001414ff00e2e2fe00ffffff00ffffff00ffffff00ffffff00c7c7fd00
f5f5ff003d3dff000000ff000000ff000000ff000000ff000000ff002525ff00f1f1ff00ffffff00ffffff00ffffff00c5c5fc00
9b9bfe000000ff000000ff000000ff000000ff000000ff000000ff000000ff008888fe00ffffff00ffffff00ffffff00c5c5fc00
f7f7ff003d3dff000000ff000000ff000000ff000000ff000000ff002525ff00f5f5ff00ffffff00ffffff00ffffff00c5c5fc00
ffffff00f1f1ff001414ff000000ff000000ff000101ff001414ff00eaeaff00ffffff00ffffff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00d4d4fe000404ff000000ff000000ff00cdcdff00ffffff00ffffff00ffffff00ffffff00ffffff00c2c2fa00
ffffff00ffffff00ffffff009b9bfe000101ff008888fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00ffffff00fbfbff007474fe00f3f3ff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c5c5fc00
i$3s               13  31 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00dddddd00444445000000000012121200aaaaab00ffffff00ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00ffffff003334340000000000000000000000000000000000cccccc00ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00bbbbbb000000000012121200efedef00656565000000000065656500ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00cccccc004444450044444500ffffff00aaaaab000000000033343400ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00ffffff00ffffff00ffffff00ffffff009a999a000000000044444500ffffff00ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff00ffffff00ffffff00ffffff00444445000000000088888800ffffff00ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff00ffffff00ffffff00333434000000000044444500ffffff00ffffff00ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff00ffffff00fffdfd00252526000000000025252600cccccc00ffffff00ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff00ffffff00ffffff00ffffff009a999a000000000025252600ffffff00ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff001212120000000000cccccc00ffffff00ffffff00ffffff00e3e1e300
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff001212120000000000cccccc00ffffff00ffffff00ffffff00e3e1e300
ffffff00888888000000000088888800ffffff00ffffff001212120000000000dddddd00ffffff00ffffff00ffffff00e1e1e100
ffffff009a999a000000000025252600efedef009a999a000000000025252600ffffff00ffffff00ffffff00ffffff00e1e1e100
ffffff00ffffff0025252600000000000000000000000000000000009a999a00ffffff00ffffff00ffffff00ffffff00e1e1e100
ffffff00ffffff00dddddb004444450000000000121212009a999a00ffffff00ffffff00ffffff00ffffff00ffffff00e1dfe100
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e1dfe100
ffffff00ffffff00ffffff00ffffff00ffffff00d9d9d900c3c3c400ffffff00ffffff00ffffff00ffffff00ffffff00e1dfe100
ffffff00ffffff00ffffff00ffffff00c3c3c400010203000000000092929200ffffff00ffffff00ffffff00ffffff00dfdfdf00
ffffff00ffffff00ffffff00bbbbbb00010101000000000000000000000000007d7d7d00ffffff00ffffff00ffffff00dfdfdf00
ffffff00ffffff00e5e5e500000000000000000000000000000000000000000000000000b2b2b200ffffff00ffffff00dfdfdf00
ffffff00fdfdfd003334340000000000000000000000000000000000000000000000000003030300f1f1f200ffffff00dfdddf00
ffffff00c3c3c400010101000000000000000000000000000000000000000000000000000000000088888800fdfdfd00dfdddf00
ffffff006565650000000000000000000000000000000000000000000000000000000000000000001b1b1c00ffffff00dddddd00
fffdfd0033343400000000000000000000000000737373008888880006060600000000000000000001020300ffffff00dddddd00
ffffff00aaaaab00000000000000000025252600cccccc00bbbbbb0065656500000000000000000065656500fdfdfd00dddddd00
ffffff00ffffff00cccccc0088888800f3f3f300bbbbbb0065656500fdfdfd009a999a00b2b2b200fdfdfd00fdfdfd00dddbdd00
ffffff00ffffff00ffffff00ffffff00ffffff007373730033343400f7f9f700ffffff00ffffff00ffffff00fdfdfd00dbdbdd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fffdff00ffffff00ffffff00ffffff00ffffff00fdfdfd00dbdbdb00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fdfdfd00dbdbdb00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fdfdfd00dbdbdb00
i$sitout           1   1  
ffffff00
i$Jh               13  31 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00ffffff005c5cff000000ff008888fe00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00fbfbff005c5cff000000ff008888fe00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00fdfdff005c5cff000000ff008888fe00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00ffffff005c5cff000000ff008888fe00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00fdfdff005c5cff000000ff008888fe00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00fbfbff005c5cff000000ff008888fe00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00fdfdff005c5cff000000ff008888fe00ffffff00ffffff00ffffff00fdfdff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00ffffff005c5cff000000ff008888fe00ffffff00ffffff00ffffff00fdfdff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00fdfdff005c5cff000000ff008888fe00ffffff00ffffff00ffffff00fbfbff00c7c7fd00
eeeefd000000ff001414ff00ffffff00fbfbff005c5cff000000ff008888fe00ffffff00ffffff00ffffff00fbfbff00c7c7fd00
eeeefd000000ff000000ff00ddddff00fdfdff005c5cff000000ff008888fe00ffffff00ffffff00ffffff00fbfbff00c7c7fd00
ffffff001414ff000000ff008888fe00eeeefd002525ff000000ff00acacfe00ffffff00ffffff00ffffff00fbfbff00c7c7fd00
ffffff005c5cff000000ff000000ff000000ff000000ff000000ff00ddddff00ffffff00ffffff00ffffff00fbfbff00c7c7fd00
fbfbff00acacfe000000ff000000ff000000ff000000ff005c5cff00ffffff00ffffff00ffffff00ffffff00f9f9ff00c7c7fd00
ffffff00ffffff008888fe000000ff000101ff003d3dff00fdfdff00ffffff00ffffff00ffffff00ffffff00f9f9ff00c5c5fc00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00f9f9ff00c5c5fc00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00f7f7ff00c5c5fc00
ffffff00ffffff009b9bfe005c5cff00acacfe00ffffff00f3f3ff008888fe005c5cff00d0d0fb00ffffff00f7f7ff00c5c5fc00
ffffff00acacfe000000ff000101ff000101ff00acacfe005c5cff000000ff000000ff002525ff00eaeaff00f7f7fd00c5c5fc00
ffffff003d3dff000000ff000000ff000000ff001414ff000a0aff000101ff000000ff000000ff009b9bfe00f7f7fd00c5c5fc00
ffffff003d3dff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff009b9bfe00f5f5fd00c2c2fa00
ffffff008888fe000000ff000000ff000000ff000000ff000000ff000000ff000000ff000404ff00d4d4fe00f5f5fd00c2c2fa00
ffffff00e2e2fe002525ff000000ff000000ff000000ff000000ff000000ff000404ff005c5cff00ffffff00f5f5fd00c2c2fa00
ffffff00ffffff00c2c2fa000a0aff000000ff000000ff000000ff000000ff003d3dff00f7f7ff00ffffff00f3f3fd00c2c2fa00
ffffff00ffffff00ffffff00acacfe000404ff000000ff000000ff002525ff00eaeaff00ffffff00ffffff00f3f3fd00c2c2fa00
ffffff00ffffff00ffffff00fbfbff009b9bfe000000ff001414ff00dadaff00ffffff00ffffff00ffffff00f3f3fd00c2c2fa00
ffffff00ffffff00ffffff00ffffff00fbfbff005c5cff009b9bfe00ffffff00ffffff00ffffff00ffffff00f1f1fd00c2c2fa00
ffffff00ffffff00ffffff00ffffff00ffffff00dadaff00f3f3ff00ffffff00ffffff00ffffff00ffffff00f1f1fd00c2c2fa00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00f1f1fd00c2c2fa00
i$3c               13  31 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00dddddd00444445000000000012121200aaaaab00ffffff00ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00ffffff003334340000000000000000000000000000000000cccccc00ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00bbbbbb000000000012121200efedef00656565000000000065656500ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00cccccc004444450044444500ffffff00aaaaab000000000033343400ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00ffffff00ffffff00ffffff00ffffff009a999a000000000044444500ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00ffffff00ffffff00ffffff00ffffff00444445000000000088888800ffffff00ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff00ffffff00ffffff00333434000000000044444500ffffff00ffffff00ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff00ffffff00ffffff00252526000000000025252600cccccc00ffffff00ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff00ffffff00ffffff00ffffff009a999a000000000025252600ffffff00ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff001212120000000000cccccc00ffffff00ffffff00ffffff00e3e1e300
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff001212120000000000cccccc00ffffff00ffffff00ffffff00e3e1e300
ffffff00888888000000000088888800ffffff00ffffff001212120000000000dddddd00ffffff00ffffff00ffffff00e3e1e300
ffffff009a999a000000000025252600efedef009a999a000000000025252600ffffff00ffffff00ffffff00ffffff00e1e1e100
ffffff00ffffff0025252600000000000000000000000000000000009a999a00ffffff00ffffff00ffffff00ffffff00e1e1e100
ffffff00ffffff00dddddb004444450000000000121212009a999a00ffffff00ffffff00ffffff00ffffff00ffffff00e1dfe100
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e1dfe100
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e1dfe100
ffffff00ffffff00ffffff00ffffff00d4d4d4005656560088888800fdfdfd00ffffff00ffffff00ffffff00ffffff00dfdfdf00
ffffff00ffffff00ffffff00e9ebeb000000000000000000000000007d7d7d00ffffff00ffffff00ffffff00ffffff00dfdfdf00
ffffff00ffffff00ffffff00bbbbbb000000000000000000000000000e0e0f00fdfdfd00ffffff00ffffff00ffffff00dfdfdf00
ffffff00ffffff00ffffff00d9d9d90000000000000000000000000056565600ffffff00ffffff00ffffff00ffffff00dfdddf00
ffffff00ebebeb0065656500929292006565650000000000252526009a999a0065656500aaaaab00ffffff00ffffff00dfdddf00
f7f7f7000e0e0f0001010100000000001b1b1c00000000000a0a0a00060606000000000000000000aaaaab00ffffff00dddddd00
aaaaab0000000000000000000000000003030300444445001b1b1c0000000000000000000000000033343400ffffff00dddddd00
d9d9d90000000000000000000000000033343400a2a2a2007d7d7d0000000000000000000000000065656500fdfdfd00dddddd00
ffffff009a999a000e0e0f0025252600d9d9d90065656500e1dfe100737373000e0e0f0033343400f1f1f200fdfdfd00dddbdd00
ffffff00ffffff00ededed00f5f5f500ffffff0006060600d0d0d000ffffff00e7e7e700fbfbfb00ffffff00fdfdfd00dbdbdd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fdfdfd00dbdbdb00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fdfdfd00dbdbdb00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fdfdfd00dbdbdb00
i$6d               13  31 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff008888fe000000ff000000ff008888fe00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff008888fe000000ff000000ff000000ff000000ff00acacfe00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff002525ff001414ff00ddddff008888fe000000ff003d3dff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00b9b9fe000000ff005c5cff00ffffff00cbcbff000000ff005c5cff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff008888fe000101ff008888fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff007474fe000000ff007474fe001414ff000000ff008888fe00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff007474fe000000ff000000ff000000ff000000ff000000ff00acacfe00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff007474fe000000ff002525ff00eeeefd008888fe000000ff003d3dff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff007474fe000101ff005c5cff00ffffff00ddddff000000ff001414ff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff007474fe000000ff008888fe00ffffff00ffffff001414ff000000ff00ddddff00ffffff00ffffff00ffffff00c9c9ff00
ffffff008888fe000000ff008888fe00ffffff00ffffff001414ff000000ff00ddddff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00b9b9fe000000ff005c5cff00ffffff00eeeefd000000ff001414ff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff001414ff001414ff00ddddff008888fe000101ff003d3dff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff008888fe000000ff000000ff000000ff000000ff00acacfe00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff007474fe000000ff000000ff009b9bfe00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00ffffff00e2e2fe007474fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00fbfbff005c5cff000000ff00c2c2fa00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff008888fe000000ff000000ff000404ff00e2e2fe00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00c9c9ff000000ff000000ff000000ff000000ff003d3dff00f7f7ff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00e6e6ff000000ff000000ff000000ff000000ff000000ff000000ff005c5cff00fbfbff00ffffff00ffffff00c7c7fd00
ffffff005c5cff000000ff000000ff000000ff000000ff000000ff000000ff000000ff00c7c7fd00ffffff00ffffff00c5c5fc00
ffffff00e6e6ff000a0aff000000ff000000ff000000ff000000ff000000ff005c5cff00ffffff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00d8d8fd000000ff000000ff000000ff000404ff003d3dff00fdfdff00ffffff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00ffffff009b9bfe000000ff000000ff001414ff00eaeaff00ffffff00ffffff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00ffffff00ffffff005c5cff000101ff00c9c9ff00ffffff00ffffff00ffffff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00ffffff00ffffff00eaeaff007474fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c5c5fc00
i$sitin            1   1  
00000000
i$As               13  31 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00121212000000000012121200ffffff00ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00ffffff00ffffff00bbbbbb00000000000000000000000000bbbbbb00ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00ffffff00ffffff008888880000000000000000000000000088888800ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00ffffff00ffffff004444450000000000333434000000000044444500ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00ffffff00ffffff001212120000000000888888000000000012121200ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00ffffff00bbbbbb000000000012121200efedef001212120000000000bbbbbb00ffffff00ffffff00ffffff00e5e3e500
ffffff00ffffff00888888000000000044444500ffffff00444445000000000088888800ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff00444445000000000088888800ffffff00888888000000000044444500ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff001212120000000000bbbbbb00ffffff00bbbbbb000000000012121200ffffff00ffffff00ffffff00e3e3e300
ffffff00bbbbbb0000000000000000000000000000000000000000000000000000000000bbbbbb00ffffff00ffffff00e3e3e300
ffffff00888888000000000000000000000000000000000000000000000000000000000088888800ffffff00ffffff00e3e3e300
ffffff00444445000000000000000000000000000000000000000000000000000000000044444500ffffff00ffffff00e3e3e300
ffffff0012121200000000009a999a00ffffff00ffffff00ffffff009a999a000000000012121200ffffff00ffffff00e3e3e300
bbbbbb000000000000000000dddddd00ffffff00ffffff00ffffff00bbbbbb000000000000000000dddddd00ffffff00e3e1e300
888888000000000025252600ffffff00ffffff00ffffff00ffffff00ffffff0012121200000000009a999a00ffffff00e3e1e300
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e3e1e300
ffffff00ffffff00ffffff00ffffff00fbf9f900b2b2b200f5f5f700ffffff00ffffff00ffffff00ffffff00ffffff00e1e1e100
ffffff00ffffff00ffffff00f5f5f500444445000000000033343400efefef00ffffff00ffffff00ffffff00ffffff00e1e1e100
ffffff00ffffff00f7f7f700252526000101010000000000000000001b1b1c00f1f1f200ffffff00ffffff00ffffff00e1e1e100
ffffff00fdfdfd0056565600010101000000000000000000000000000101010033343400fdfdfd00ffffff00ffffff00e1e1e100
ffffff00bbbbbb0000000000000000000000000000000000000000000000000001010100a2a2a200ffffff00ffffff00e1e1e100
f9f7f9001b1b1c00000000000000000000000000000000000000000000000000000000000e0e0f00f5f5f500ffffff00e1e1e100
cccccc00000000000000000000000000000000000000000000000000000000000000000000000000c3c3c400ffffff00e1dfe100
9a999a0000000000000000000000000033343400929292004444450000000000000000000000000088888800ffffff00e1dfe100
ebebeb001b1b1c000000000000000000b2b2b200b2b2b200bbbbbb0000000000000000000a0a0a00e9e7e900ffffff00e1dfe100
ffffff00efefef0092929200c3c3c400f9f9f90025252600f1f1f200d0d0d00092929200e9ebeb00ffffff00ffffff00dfdfdf00
ffffff00ffffff00ffffff00ffffff00d8d5d5001b1b1c00c3c3c400ffffff00ffffff00ffffff00ffffff00ffffff00dfdfdf00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dfdfdf00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dfdfdf00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dfdfdf00
i$3h               13  31 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ddddff003d3dff000101ff001414ff00acacfe00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00fbfbff003d3dff000101ff000000ff000000ff000000ff00cbcbff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00b9b9fe000000ff000a0aff00f1f1ff005c5cff000404ff005c5cff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00cbcbff003d3dff003d3dff00ffffff00acacfe000101ff003d3dff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00ffffff009b9bfe000000ff003d3dff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00ffffff003d3dff000000ff008888fe00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00fbfbff003d3dff000404ff003d3dff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff002525ff000000ff002525ff00cbcbff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00ffffff009b9bfe000000ff002525ff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff001414ff000000ff00cbcbff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff001414ff000000ff00cbcbff00ffffff00ffffff00ffffff00c9c9ff00
ffffff008888fe000000ff008888fe00ffffff00ffffff000a0aff000000ff00ddddff00ffffff00ffffff00ffffff00c9c9ff00
ffffff009b9bfe000000ff002525ff00eeeefd009b9bfe000000ff002525ff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff002525ff000000ff000000ff000000ff000000ff009b9bfe00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ddddff003d3dff000000ff001414ff009b9bfe00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00c2c2fa005c5cff008888fe00ffffff00ffffff009b9bfe005c5cff00acacfe00ffffff00ffffff00c7c7fd00
ffffff00dadaff000a0aff000101ff000000ff008888fe009b9bfe000000ff000101ff000101ff00c5c5fc00ffffff00c7c7fd00
ffffff007474fe000404ff000000ff000000ff000a0aff001414ff000000ff000000ff000000ff005c5cff00ffffff00c5c5fc00
ffffff007474fe000101ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff005c5cff00ffffff00c5c5fc00
ffffff00c2c2fa000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff009b9bfe00fdfdff00c5c5fc00
ffffff00fdfdff003d3dff000000ff000000ff000000ff000000ff000000ff000000ff003d3dff00f3f3ff00fdfdff00c5c5fc00
ffffff00ffffff00eaeaff002525ff000101ff000000ff000000ff000000ff001414ff00d8d8fd00fdfdff00fdfdff00c5c5fc00
ffffff00ffffff00ffffff00dadaff001414ff000101ff000000ff000a0aff00c5c5fc00ffffff00ffffff00fdfdff00c5c5fc00
ffffff00ffffff00ffffff00ffffff00c5c5fc000a0aff000000ff00acacfe00ffffff00ffffff00ffffff00fdfdff00c5c5fc00
ffffff00ffffff00ffffff00ffffff00ffffff008888fe007474fe00ffffff00ffffff00ffffff00ffffff00fdfdff00c5c5fc00
ffffff00ffffff00ffffff00ffffff00ffffff00eaeaff00ddddff00ffffff00ffffff00ffffff00ffffff00fdfdff00c5c5fc00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fdfdff00c5c5fc00
i$Ah               13  31 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff001414ff000000ff001414ff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00b9b9fe000000ff000000ff000000ff00b9b9fe00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff008888fe000000ff000000ff000000ff008888fe00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff003d3dff000101ff003d3dff000000ff003d3dff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff001414ff000000ff008888fe000000ff001414ff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00b9b9fe000000ff001414ff00eeeefd001414ff000000ff00b9b9fe00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff008888fe000101ff003d3dff00ffffff003d3dff000000ff008888fe00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff003d3dff000101ff008888fe00ffffff008888fe000000ff003d3dff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff001414ff000000ff00b9b9fe00ffffff00b9b9fe000101ff001414ff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00b9b9fe000000ff000000ff000000ff000000ff000000ff000000ff000000ff00b9b9fe00ffffff00ffffff00c9c9ff00
ffffff008888fe000000ff000000ff000000ff000000ff000000ff000000ff000000ff008888fe00ffffff00ffffff00c9c9ff00
ffffff003d3dff000101ff000000ff000000ff000000ff000000ff000000ff000000ff003d3dff00ffffff00ffffff00c9c9ff00
ffffff001414ff000000ff009b9bfe00ffffff00ffffff00ffffff009b9bfe000000ff001414ff00ffffff00ffffff00c9c9ff00
b9b9fe000000ff000000ff00ddddff00ffffff00ffffff00ffffff00b9b9fe000000ff000000ff00ddddff00ffffff00c9c9ff00
8888fe000000ff002525ff00ffffff00ffffff00ffffff00ffffff00ffffff001414ff000000ff009b9bfe00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00eeeefd007474fe005c5cff00d8d8fd00ffffff00cdcdff007474fe007474fe00f1f1ff00ffffff00c9c9ff00
ffffff00f7f7ff005c5cff000101ff000404ff002525ff00c5c5fc002525ff000000ff000404ff005c5cff00ffffff00c7c7fd00
ffffff00b9b9fe000404ff000000ff000000ff000000ff002525ff000000ff000000ff000000ff000a0aff00ffffff00c7c7fd00
ffffff00b9b9fe000404ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000a0aff00ffffff00c7c7fd00
ffffff00eaeaff003d3dff000000ff000000ff000000ff000000ff000000ff000000ff000000ff003d3dff00ffffff00c7c7fd00
ffffff00ffffff00acacfe000000ff000000ff000000ff000000ff000000ff000000ff000000ff00b9b9fe00ffffff00c7c7fd00
ffffff00ffffff00fdfdff007474fe000101ff000000ff000000ff000000ff000000ff008888fe00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00ffffff005c5cff000101ff000000ff000000ff007474fe00fbfbff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00ffffff00ffffff003d3dff000000ff005c5cff00fbfbff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00ffffff00ffffff00e6e6ff003d3dff00e6e6ff00fbfbff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00d4d4fe00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
i$6s               13  31 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff0088888800000000000000000088888800ffffff00ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00ffffff008888880000000000000000000000000000000000aaaaab00ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00ffffff002525260012121200dddddd00888888000000000044444500ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00bbbbbb000000000065656500ffffff00cccccc000000000065656500ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00888888000000000088888800ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00737373000000000073737300121212000000000088888800ffffff00ffffff00ffffff00ffffff00ffffff00e3e3e300
ffffff00737373000101010000000000000000000000000000000000aaaaab00ffffff00ffffff00ffffff00ffffff00e3e3e300
ffffff00737373000000000025252600efedef00888888000000000044444500ffffff00ffffff00ffffff00ffffff00e3e3e300
ffffff00737373000000000065656500ffffff00dddddd000000000012121200ffffff00ffffff00ffffff00ffffff00e3e3e300
ffffff00737373000101010088888800ffffff00ffffff001212120000000000dddddd00ffffff00ffffff00ffffff00e3e3e300
ffffff00888888000000000088888800ffffff00ffffff001212120000000000dddddd00ffffff00ffffff00ffffff00e3e1e300
ffffff00bbbbbb000000000065656500ffffff00efedef000000000012121200ffffff00ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff001212120012121200dddddd00888888000000000044444500ffffff00ffffff00ffffff00ffffff00e3e1e300
ffffff00ffffff008888880000000000000000000000000000000000aaaaab00ffffff00ffffff00ffffff00ffffff00e1e1e100
ffffff00ffffff00ffffff007373730000000000000000009a999a00ffffff00ffffff00ffffff00ffffff00ffffff00e1e1e100
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e1e1e100
ffffff00ffffff00ffffff00ffffff00ffffff00bbbbbb00e5e3e500ffffff00ffffff00ffffff00ffffff00ffffff00e1dfe100
ffffff00ffffff00ffffff00ffffff007d7d7d00000000000a0a0a00d4d4d400ffffff00ffffff00ffffff00ffffff00e1dfe100
ffffff00ffffff00ffffff006565650000000000000000000000000000000000d4d4d400ffffff00ffffff00ffffff00e1dfe100
ffffff00ffffff009a999a0000000000000000000000000000000000000000000a0a0a00f1eff100ffffff00ffffff00dfdfdf00
ffffff00e7e7e7000000000000000000000000000000000000000000000000000000000056565600ffffff00ffffff00dfdfdf00
ffffff00656565000000000000000000000000000000000000000000000000000000000000000000dbdbdb00ffffff00dfdfdf00
ffffff000a0a0a0000000000000000000000000000000000000000000000000000000000000000007d7d7d00ffffff00dfdddf00
ffffff0000000000000000000000000012121200888888006565650000000000000000000101010044444500ffffff00dfdddf00
ffffff004444450000000000000000007d7d7d00bbbbbb00c3c3c400121212000000000000000000c3c3c400ffffff00dfdddf00
ffffff00fdfdfd00aaaaab00a2a2a200fdfdfd0044444500d4d4d400edebed0088888800d4d4d400ffffff00ffffff00dddddd00
ffffff00ffffff00ffffff00ffffff00f3f5f3002525260088888800ffffff00ffffff00ffffff00ffffff00ffffff00dddddd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dddddd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dddbdd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dddbdd00
i$autopost         2   1  
ffffff00ffffff00
i$4s               13  31 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00888888000000000073737300ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00ffffff00ffffff00ffffff00ffffff00252526000000000073737300ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00ffffff00ffffff00ffffff00aaaaab00000000000000000073737300ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00ffffff00ffffff00ffffff0044444500000000000101010073737300ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00ffffff00ffffff00cccccc0000000000000000000000000073737300ffffff00ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff00ffffff007373730000000000333434000000000073737300ffffff00ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff00efedef001212120025252600737373000101010073737300ffffff00ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff00888888000000000088888800737373000101010073737300ffffff00ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff002525260012121200ffffff00737373000101010073737300ffffff00ffffff00ffffff00ffffff00e3e3e300
ffffff00aaaaab000000000073737300ffffff00737373000101010073737300ffffff00ffffff00ffffff00ffffff00e3e1e300
ffffff007373730001010100000000000000000000000000000000000000000033343400ffffff00ffffff00ffffff00e3e1e300
ffffff007373730000000000000000000000000000000000000000000000000033343400ffffff00ffffff00ffffff00e3e1e300
ffffff00ffffff00ffffff00ffffff00ffffff00737373000000000073737300ffffff00ffffff00ffffff00ffffff00e3e1e300
ffffff00ffffff00ffffff00ffffff00ffffff00737373000101010073737300ffffff00ffffff00ffffff00ffffff00e1e1e100
ffffff00ffffff00ffffff00ffffff00ffffff00737373000101010073737300ffffff00ffffff00ffffff00ffffff00e1e1e100
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e1e1e100
ffffff00ffffff00ffffff00ffffff00ffffff00efefef00b2b2b200fdfdfd00ffffff00ffffff00ffffff00ffffff00e1dfe100
ffffff00ffffff00ffffff00ffffff00e5e5e5001b1b1c000101010065656500f9f9f900ffffff00ffffff00ffffff00e1dfe100
ffffff00ffffff00ffffff00e5e7e7000a0a0a0000000000000000000000000044444500fdfdfd00ffffff00ffffff00e1dfe100
ffffff00ffffff00f9f9f9001b1b1c00000000000000000000000000000000000000000073737300ffffff00fdfdfd00dfdfdf00
ffffff00ffffff007d7d7d0000000000000000000000000000000000000000000000000000000000d4d4d400fdfdfd00dfdfdf00
ffffff00ebebeb00000000000000000000000000000000000000000000000000000000000000000044444500f5f5f500dfdfdf00
ffffff00a2a2a200000000000000000000000000000000000000000000000000000000000000000000000000cccccc00dfdddf00
ffffff00656565000000000000000000000000005656560092929200252526000000000000000000000000009a999a00dfdddf00
ffffff00d9d9d900030303000000000001030100c3c3c400bbbbbb009a999a00000000000000000033343400e9ebeb00dddddd00
ffffff00ffffff00e3e1e30088888800dfdfdf00e9e7e90025252600fbfbfb00b2b2b2009a999a00f5f5f500fdfdfd00dddddd00
ffffff00ffffff00ffffff00ffffff00ffffff00aaaaab0025252600e5e5e500ffffff00ffffff00ffffff00fdfdfd00dddbdd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fdfdfd00dddbdd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fdfdfd00dbdddb00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fdfdfd00dbdbdb00
i$9d               13  31 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00fdfdff005c5cff000000ff001414ff009b9bfe00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff007474fe000000ff000000ff000000ff000000ff00cbcbff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00eeeefd001414ff001414ff00ddddff009b9bfe000000ff005c5cff00fdfdff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00acacfe000000ff003d3dff00ffffff00ffffff001414ff000a0aff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff008888fe000000ff005c5cff00ffffff00fbfbff003d3dff000000ff00ddddff00ffffff00ffffff00ffffff00c9c9ff00
ffffff008888fe000000ff005c5cff00ffffff00ffffff002525ff000000ff00b9b9fe00ffffff00ffffff00ffffff00c9c9ff00
ffffff00acacfe000000ff003d3dff00ffffff00ffffff002525ff000101ff00acacfe00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ddddff000000ff000101ff00b9b9fe00b9b9fe000000ff000000ff00acacfe00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff005c5cff000000ff000000ff000000ff000000ff000000ff00acacfe00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff003d3dff000101ff003d3dff005c5cff000000ff00b9b9fe00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff003d3dff000000ff00ddddff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00eeeefd003d3dff002525ff00ffffff00ffffff002525ff001414ff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff001414ff000000ff00b9b9fe009b9bfe000101ff005c5cff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00fdfdff005c5cff000000ff000000ff000000ff001414ff00e2e2fe00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00eaeaff003d3dff000101ff002525ff00acacfe00fbfbff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00ffffff00f1f1ff007474fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00fdfdff007474fe000000ff00acacfe00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff009b9bfe000000ff000000ff000000ff00dadaff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00dadaff000000ff000000ff000000ff000101ff002525ff00f5f5ff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00eaeaff001414ff000000ff000000ff000000ff000000ff000000ff005c5cff00ffffff00ffffff00ffffff00c7c7fd00
ffffff005c5cff000000ff000000ff000000ff000000ff000000ff000000ff000101ff00fdfdff00ffffff00ffffff00c7c7fd00
ffffff00eaeaff002525ff000000ff000000ff000000ff000000ff000000ff005c5cff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00e2e2fe000404ff000000ff000000ff000000ff003d3dff00f7f7ff00ffffff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00ffffff00acacfe000000ff000000ff000404ff00ddddff00ffffff00ffffff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00ffffff00ffffff007474fe000000ff00acacfe00ffffff00ffffff00ffffff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00ffffff00ffffff00f1f1ff008888fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c5c5fc00
i$9s               13  31 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff005656560000000000121212009a999a00ffffff00ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00ffffff007373730000000000000000000000000000000000cccccc00ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00efedef001212120012121200dddddd009a999a000000000056565600ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00aaaaab000000000044444500ffffff00ffffff001212120012121200ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00888888000000000056565600ffffff00ffffff003334340000000000dddddd00ffffff00ffffff00ffffff00e5e3e500
ffffff00888888000000000056565600ffffff00ffffff003334340000000000bbbbbb00ffffff00ffffff00ffffff00e3e3e300
ffffff00aaaaab000000000044444500ffffff00ffffff002525260000000000aaaaab00ffffff00ffffff00ffffff00e3e3e300
ffffff00dddddd000000000000000000bbbbbb00bbbbbb000000000000000000aaaaab00ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff00656565000000000000000000000000000000000000000000aaaaab00ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff00ffffff004444450000000000444445005656560000000000bbbbbb00ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff004444450000000000dddddd00ffffff00ffffff00ffffff00e3e3e300
ffffff00efedef004444450025252600ffffff00ffffff002525260012121200ffffff00ffffff00ffffff00ffffff00e3e1e300
ffffff00ffffff001212120000000000bbbbbb009a999a000000000065656500ffffff00ffffff00ffffff00ffffff00e3e1e300
ffffff00ffffff005656560000000000000000000000000012121200dddddd00ffffff00ffffff00ffffff00ffffff00e3e1e300
ffffff00ffffff00efedef00444445000000000025252600aaaaab00ffffff00ffffff00ffffff00ffffff00ffffff00e1e1e100
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e1e1e100
ffffff00ffffff00ffffff00ffffff00ffffff00f3f5f300b2b2b200fbf9f900ffffff00ffffff00ffffff00ffffff00e1e1e100
ffffff00ffffff00ffffff00ffffff00efedef00333434000000000044444500f7f7f700ffffff00ffffff00ffffff00e1e1df00
ffffff00ffffff00ffffff00f1eff1001b1b1c0000000000000000000000000033343400f7f7f700ffffff00ffffff00e1dfe100
ffffff00ffffff00fdfdfd0033343400000000000000000000000000000000000000000056565600ffffff00ffffff00e1dfe100
ffffff00ffffff009a999a0000000000000000000000000000000000000000000000000000000000bbbbbb00ffffff00dfdddf00
ffffff00f5f3f5000a0a0a000000000000000000000000000000000000000000000000000000000025252600f9f9f900dfdfdf00
ffffff00bbbbbb00000000000000000000000000000000000000000000000000000000000000000000000000cccccc00dfdfdd00
ffffff00888888000000000000000000000000004444450092929200333434000000000000000000000000009a999a00dfdfdf00
ffffff00e5e7e7000a0a0a000000000000000000bbbbbb00b2b2b200aaaaab0000000000000000001b1b1c00edebed00dfdddf00
ffffff00ffffff00e9ebeb0092929200d0d0d000f3f3f30025252600f9f9f900c3c3c40092929200f1f1f200ffffff00dfdddf00
ffffff00ffffff00ffffff00ffffff00ffffff00c3c3c40025252600d7d7d700ffffff00ffffff00ffffff00ffffff00dfdddf00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dddddd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dddddd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dddddd00
i$9c               13  31 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff005656560000000000121212009a999a00ffffff00ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00ffffff007373730000000000000000000000000000000000cccccc00ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00efedef001212120012121200dddddd009a999a000000000056565600ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00aaaaab000000000044444500ffffff00ffffff001212120012121200ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00888888000000000056565600ffffff00ffffff003334340000000000dddddd00ffffff00ffffff00ffffff00e5e3e500
ffffff00888888000000000056565600ffffff00ffffff003334340000000000bbbbbb00ffffff00ffffff00ffffff00e3e3e300
ffffff00aaaaab000000000044444500ffffff00ffffff002525260000000000aaaaab00ffffff00ffffff00ffffff00e3e3e300
ffffff00dddddd000000000000000000bbbbbb00bbbbbb000000000000000000aaaaab00ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff00656565000000000000000000000000000000000000000000aaaaab00ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff00ffffff004444450000000000444445005656560000000000bbbbbb00ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff004444450000000000dddddd00ffffff00ffffff00ffffff00e3e1e300
ffffff00efedef004444450025252600ffffff00ffffff002525260012121200ffffff00ffffff00ffffff00ffffff00e3e1e300
ffffff00ffffff001212120000000000bbbbbb009a999a000000000065656500ffffff00ffffff00ffffff00ffffff00e3e1e300
ffffff00ffffff005656560000000000000000000000000012121200dddddd00ffffff00ffffff00ffffff00ffffff00e3e1e300
ffffff00ffffff00efedef00444445000000000025252600aaaaab00ffffff00ffffff00ffffff00ffffff00ffffff00e1e1e100
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e1e1e100
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e1e1e100
ffffff00ffffff00ffffff00ffffff00fdfdfd008888880056565600dbdbdb00ffffff00ffffff00ffffff00ffffff00e1e1e100
ffffff00ffffff00ffffff00ffffff0073737300000000000000000000000000f1f1f200ffffff00ffffff00ffffff00e1dfe100
ffffff00ffffff00ffffff00fdfdfd0012121200000000000000000000000000bbbbbb00ffffff00ffffff00ffffff00e1dfe100
ffffff00ffffff00ffffff00ffffff0056565600000000000000000000000000e1e1e100ffffff00ffffff00ffffff00dfdfdf00
ffffff00ffffff00a2a2a200656565009a999a002525260001020300656565008888880073737300efefef00ffffff00dfdfdf00
ffffff00aaaaab000000000001010100060606000e0e0f00000000001b1b1c0000000000000000001b1b1c00ffffff00dfdfdf00
ffffff004444450000000000000000000000000025252600565656000606060000000000000000000e0e0f00ffffff00dfdfdf00
ffffff006565650000000000000000000000000088888800c3c3c4004444450000000000000000000a0a0a00ffffff00dfdfdf00
ffffff00f1eff100333434000e0e0f007d7d7d00d4d4d4007d7d7d00d7d7d7001b1b1c0012121200a2a2a200ffffff00dfdddf00
ffffff00ffffff00fbf9f900e9e9e900ffffff00bbbbbb0006060600ffffff00f5f5f700ededed00ffffff00ffffff00dfdddf00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dddddd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dddddd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dddddd00
i$8d               13  31 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00f1f1ff003d3dff000000ff000000ff007474fe00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff003d3dff000101ff000000ff000000ff000000ff008888fe00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ddddff000101ff001414ff00ddddff008888fe000000ff002525ff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00acacfe000000ff003d3dff00fbfbff00eeeefd000000ff001414ff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00b9b9fe000000ff002525ff00ffffff00ddddff000000ff001414ff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff001414ff001414ff00ddddff008888fe000000ff005c5cff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00acacfe000000ff000000ff000000ff003d3dff00eaeaff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff008888fe000000ff000000ff000000ff000a0aff00cbcbff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ddddff000000ff001414ff00eeeefd009b9bfe000000ff003d3dff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff009b9bfe000404ff005c5cff00ffffff00ffffff001414ff000000ff00dadaff00ffffff00ffffff00ffffff00c9c9ff00
ffffff008888fe000000ff007474fe00ffffff00ffffff001414ff000000ff00cbcbff00ffffff00ffffff00ffffff00c9c9ff00
ffffff008888fe000000ff005c5cff00ffffff00ffffff001414ff000000ff00ddddff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00b9b9fe000101ff001414ff00ddddff009b9bfe000000ff002525ff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff003d3dff000000ff000000ff000000ff000000ff008888fe00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00f1f1ff003d3dff000101ff000000ff008888fe00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00ffffff007474fe00ddddff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00c5c5fc000000ff003d3dff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00e6e6ff000404ff000404ff000404ff008888fe00ffffff00ffffff00ffffff00ffffff00ffffff00c5c5fc00
ffffff00f9f9ff003d3dff000000ff000000ff000000ff000000ff00c7c7fd00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff005c5cff000101ff000000ff000000ff000000ff000000ff000101ff00ddddff00ffffff00ffffff00ffffff00c7c7fd00
d4d4fe000000ff000000ff000000ff000000ff000000ff000000ff000000ff003d3dff00ffffff00ffffff00ffffff00c5c5fc00
fdfdff007474fe000000ff000000ff000000ff000000ff000000ff000a0aff00e2e2fe00ffffff00ffffff00ffffff00c5c5fc00
ffffff00f5f5ff005c5cff000000ff000000ff000000ff000000ff00d4d4fe00ffffff00ffffff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00eaeaff001414ff000000ff000101ff008888fe00ffffff00ffffff00ffffff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00ffffff00d4d4fe000000ff005c5cff00fdfdff00ffffff00ffffff00ffffff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00ffffff00ffffff007474fe00ddddff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c5c5fc00
i$6c               13  31 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff0088888800000000000000000088888800ffffff00ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00ffffff008888880000000000000000000000000000000000aaaaab00ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00ffffff002525260012121200dddddd00888888000000000044444500ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00bbbbbb000000000065656500ffffff00cccccc000000000065656500ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00888888000000000088888800ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00737373000000000073737300121212000000000088888800ffffff00ffffff00ffffff00ffffff00ffffff00e3e3e300
ffffff00737373000101010000000000000000000000000000000000aaaaab00ffffff00ffffff00ffffff00ffffff00e3e3e300
ffffff00737373000000000025252600efeded00888888000000000044444500ffffff00ffffff00ffffff00ffffff00e3e3e300
ffffff00737373000000000065656500ffffff00dddddd000000000012121200ffffff00ffffff00ffffff00ffffff00e3e3e300
ffffff00737373000000000088888800ffffff00ffffff001212120000000000dddddd00ffffff00ffffff00ffffff00e3e3e300
ffffff00888888000000000088888800ffffff00ffffff001212120000000000dddddd00ffffff00ffffff00ffffff00e3e1e300
ffffff00bbbbbb000000000065656500ffffff00efedef000000000012121200ffffff00ffffff00ffffff00ffffff00e3e1e300
ffffff00ffffff001212120012121200dddddd00888888000000000044444500ffffff00ffffff00ffffff00ffffff00e3e1e300
ffffff00ffffff008888880000000000000000000000000000000000aaaaab00ffffff00ffffff00ffffff00ffffff00e1e1e100
ffffff00ffffff00ffffff007373730000000000000000009a999a00ffffff00ffffff00ffffff00ffffff00ffffff00e1e1e100
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e1e1e100
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e1dfe100
ffffff00ffffff00ffffff00ffffff00ffffff00a2a2a20044444500c3c3c400ffffff00ffffff00ffffff00ffffff00e1dfe100
ffffff00ffffff00ffffff00ffffff00a2a2a200000000000102030000000000d9d9d900ffffff00ffffff00ffffff00e1dfe100
ffffff00ffffff00ffffff00ffffff00444445000000000000000000000000007d7d7d00ffffff00ffffff00ffffff00dfdfdf00
ffffff00ffffff00ffffff00ffffff007d7d7d00000000000000000001010100bbbbbb00ffffff00ffffff00ffffff00dfdfdf00
ffffff00ffffff00c3c3c400656565009a999a003334340000000000565656009a999a0065656500dddbdd00ffffff00dfdfdf00
ffffff00d0d0d000000000000000000000000000121212000000000012121200000000000000000000000000e3e3e300dfdddf00
ffffff00565656000000000000000000000000001b1b1c0044444500030303000000000000000000000000007d7d7d00dfdddf00
fdfdfd0092929200000000000000000001010100737373009a999a0044444500000000000000000000000000b2b2b200dfdddf00
ffffff00f7f7f700565656000e0e0f0056565600e1e1df0056565600e9e9e900333434000e0e0f0073737300fdfdfd00dddddd00
ffffff00ffffff00fbfbfb00e7e7e700ffffff00e9e7e90000000000fdfdfd00fbf9f900e9e9e900ffffff00ffffff00dddddd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dddddd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dbdbdb00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dddbdd00
i$4c               13  31 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00888888000000000073737300ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00ffffff00ffffff00ffffff00ffffff00252526000000000073737300ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00ffffff00ffffff00ffffff00aaaaab00000000000000000073737300ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00ffffff00ffffff00ffffff0044444500000000000000000073737300ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00ffffff00ffffff00cccccc0000000000000000000101010073737300ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00ffffff00ffffff007373730000000000333434000000000073737300ffffff00ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff00efedef001212120025252600737373000000000073737300ffffff00ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff00888888000000000088888800737373000101010073737300ffffff00ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff002525260012121200ffffff00737373000101010073737300ffffff00ffffff00ffffff00ffffff00e3e3e300
ffffff00aaaaab000000000073737300ffffff00737373000101010073737300ffffff00ffffff00ffffff00ffffff00e1e3e100
ffffff007373730000000000000000000000000000000000000000000000000033343400ffffff00ffffff00ffffff00e3e1e300
ffffff007373730000000000000000000000000000000000000000000000000033343400ffffff00ffffff00ffffff00e3e1e300
ffffff00ffffff00ffffff00ffffff00ffffff00737373000000000073737300ffffff00ffffff00ffffff00ffffff00e3e1e300
ffffff00ffffff00ffffff00ffffff00ffffff00737373000101010073737300ffffff00ffffff00ffffff00ffffff00e1e1e100
ffffff00ffffff00ffffff00ffffff00ffffff00737373000101010073737300ffffff00ffffff00ffffff00ffffff00e1e1e100
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e1e1e100
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e1dfe100
ffffff00ffffff00ffffff00ffffff00fdfdfd009292920056565600cccccc00ffffff00ffffff00ffffff00ffffff00e1dfe100
ffffff00ffffff00ffffff00ffffff0092929200000000000000000000000000e9e7e900ffffff00ffffff00ffffff00e1dfe100
ffffff00ffffff00ffffff00fdfdfd001b1b1c00000000000000000000000000b2b2b200ffffff00ffffff00fdfdfd00dfdfdf00
ffffff00ffffff00ffffff00ffffff0065656500000000000000000000000000d7d7d700ffffff00ffffff00fdfdfd00dfdfdf00
ffffff00ffffff00b2b2b200656565009a999a002525260000000000565656008888880065656500e9e7e900fdfdfd00dddfdf00
ffffff00c3c3c4000000000000000000010203000e0e0f00010101001212120001010100010101001b1b1c00fdfdfd00dfdddf00
ffffff004444450000000000000000000000000025252600444445000303030000000000000000000e0e0f00fdfdfd00dfdddf00
fdfdfd007d7d7d000000000000000000010101007d7d7d00bbbbbb004444450000000000000000000a0a0a00fdfdfd00dddddd00
ffffff00f3f5f300444445000e0e0f0073737300d4d4d4007d7d7d00e5e5e50025252600121212009a999a00fdfdfd00dddddd00
ffffff00ffffff00fbf9f900e7e7e700fdfdfd00d4d4d40006060600fdfdfd00f7f7f700ebebeb00fdfdfd00fdfdfd00dddbdd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fdfdfd00dddbdd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fdfdfd00dbdddb00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fdfdfd00dbdbdb00
i$4h               13  31 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00ffffff008888fe000000ff007474fe00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00ffffff002525ff000000ff007474fe00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00acacfe000000ff000000ff007474fe00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff003d3dff000101ff000000ff007474fe00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00cbcbff000101ff000000ff000000ff007474fe00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff007474fe000000ff003d3dff000000ff007474fe00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00eeeefd001414ff002525ff007474fe000000ff007474fe00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff008888fe000000ff008888fe007474fe000000ff007474fe00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff002525ff001414ff00ffffff007474fe000000ff007474fe00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00acacfe000000ff007474fe00ffffff007474fe000000ff007474fe00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff007474fe000000ff000000ff000000ff000000ff000000ff000000ff003d3dff00ffffff00ffffff00ffffff00c9c9ff00
ffffff007474fe000000ff000000ff000000ff000000ff000000ff000000ff002525ff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00ffffff007474fe000000ff007474fe00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00ffffff007474fe000101ff007474fe00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00ffffff00ffffff007474fe000000ff007474fe00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fdfdff00c7c7fd00
ffffff00ffffff00ddddff005c5cff007474fe00eaeaff00ffffff00b9b9fe005c5cff008888fe00ffffff00fdfdff00c7c7fd00
ffffff00f5f5ff002525ff000404ff000000ff005c5cff00b9b9fe000404ff000101ff000101ff009b9bfe00fdfdff00c7c7fd00
ffffff00b9b9fe000101ff000101ff000000ff000000ff001414ff000404ff000000ff000000ff002525ff00fdfdff00c7c7fd00
ffffff00acacfe000101ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff002525ff00fdfdff00c7c7fd00
ffffff00eaeaff000a0aff000000ff000000ff000000ff000000ff000000ff000000ff000000ff007474fe00fdfdff00c5c5fc00
ffffff00ffffff007474fe000000ff000000ff000000ff000000ff000000ff000000ff001414ff00d4d4fe00f9f9ff00c5c5fc00
ffffff00ffffff00ffffff005c5cff000000ff000000ff000000ff000000ff000404ff00acacfe00ffffff00fbfbff00c5c5fc00
ffffff00ffffff00fdfdff00f1f1ff003d3dff000000ff000000ff000000ff009b9bfe00ffffff00ffffff00fbfbff00c5c5fc00
ffffff00ffffff00ffffff00ffffff00e6e6ff002525ff000101ff007474fe00ffffff00ffffff00ffffff00fbfbff00c5c5fc00
ffffff00ffffff00ffffff00ffffff00ffffff00b9b9fe003d3dff00ffffff00ffffff00ffffff00ffffff00fbfbff00c5c5fc00
ffffff00ffffff00ffffff00ffffff00ffffff00f9f9ff00dadaff00ffffff00ffffff00ffffff00ffffff00fbfbff00c5c5fc00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fbfbff00c5c5fc00
i$4d               13  31 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00ffffff008888fe000000ff007474fe00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00ffffff002525ff000000ff007474fe00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00acacfe000000ff000000ff007474fe00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff003d3dff000101ff000000ff007474fe00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00cbcbff000101ff000000ff000000ff007474fe00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff007474fe000000ff003d3dff000000ff007474fe00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00eeeefd001414ff002525ff007474fe000000ff007474fe00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff008888fe000000ff008888fe007474fe000000ff007474fe00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff002525ff001414ff00ffffff007474fe000000ff007474fe00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00acacfe000000ff007474fe00ffffff007474fe000000ff007474fe00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff007474fe000000ff000000ff000000ff000000ff000000ff000000ff003d3dff00fbfbff00ffffff00ffffff00c9c9ff00
ffffff007474fe000000ff000000ff000000ff000000ff000000ff000000ff002525ff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00ffffff007474fe000000ff007474fe00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00ffffff007474fe000000ff007474fe00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00ffffff00ffffff007474fe000000ff007474fe00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fdfdff00c7c7fd00
ffffff00ffffff00ffffff00ffffff008888fe00c9c9ff00ffffff00ffffff00ffffff00ffffff00ffffff00fdfdff00c7c7fd00
ffffff00ffffff00ffffff00dadaff000000ff002525ff00f7f7ff00ffffff00ffffff00ffffff00ffffff00fdfdff00c7c7fd00
ffffff00ffffff00f3f3ff001414ff000101ff000101ff005c5cff00ffffff00ffffff00ffffff00ffffff00fdfdff00c7c7fd00
ffffff00fdfdff005c5cff000000ff000000ff000000ff000000ff00acacfe00ffffff00ffffff00ffffff00fdfdff00c7c7fd00
ffffff008888fe000101ff000000ff000000ff000000ff000000ff000000ff00c9c9ff00ffffff00ffffff00fdfdff00c5c5fc00
dadaff000000ff000000ff000000ff000000ff000000ff000000ff000000ff002525ff00ffffff00ffffff00fbfbff00c5c5fc00
ffffff008888fe000101ff000000ff000000ff000000ff000000ff000000ff00cdcdff00ffffff00ffffff00fbfbff00c5c5fc00
ffffff00ffffff007474fe000000ff000000ff000000ff000000ff00b9b9fe00ffffff00ffffff00ffffff00fbfbff00c5c5fc00
ffffff00ffffff00f9f9ff002525ff000000ff000000ff007474fe00ffffff00ffffff00ffffff00ffffff00fbfbff00c5c5fc00
ffffff00ffffff00ffffff00e6e6ff000000ff002525ff00fbfbff00ffffff00ffffff00ffffff00ffffff00fbfbff00c5c5fc00
ffffff00ffffff00ffffff00ffffff009b9bfe00cbcbff00ffffff00ffffff00ffffff00ffffff00ffffff00fbfbff00c5c5fc00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fbfbff00c5c5fc00
i$Kd               13  31 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
cbcbff000000ff003d3dff00ffffff00ffffff00ffffff008888fe000000ff000000ff009b9bfe00ffffff00ffffff00c9c9ff00
cbcbff000404ff002525ff00fdfdff00ffffff00eeeefd001414ff000000ff005c5cff00ffffff00ffffff00ffffff00c9c9ff00
cbcbff000000ff003d3dff00ffffff00ffffff005c5cff000000ff002525ff00eaeaff00ffffff00ffffff00ffffff00c9c9ff00
cbcbff000000ff002525ff00ffffff009b9bfe000000ff000000ff009b9bfe00ffffff00ffffff00ffffff00ffffff00c9c9ff00
cbcbff000000ff003d3dff00eeeefd002525ff000000ff005c5cff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
cbcbff000000ff003d3dff005c5cff000000ff000000ff00dadaff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
cbcbff000000ff000a0aff000000ff000101ff000000ff00acacfe00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
cbcbff000000ff000000ff000000ff000000ff000000ff003d3dff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
cbcbff000000ff000000ff002525ff009b9bfe000000ff000101ff00cbcbff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
cbcbff000000ff000000ff009b9bfe00fdfdff002525ff000000ff007474fe00ffffff00ffffff00ffffff00ffffff00c9c9ff00
cbcbff000000ff003d3dff00fbfbff00ffffff007474fe000000ff001414ff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
cbcbff000000ff002525ff00ffffff00ffffff00ddddff000000ff000000ff008888fe00ffffff00ffffff00ffffff00c9c9ff00
cbcbff000000ff003d3dff00ffffff00ffffff00ffffff003d3dff000000ff003d3dff00ffffff00ffffff00ffffff00c9c9ff00
cbcbff000404ff002525ff00ffffff00ffffff00ffffff009b9bfe000000ff000000ff00b9b9fe00ffffff00ffffff00c7c7fd00
cbcbff000000ff003d3dff00ffffff00ffffff00ffffff00ffffff001414ff000000ff005c5cff00fdfdff00ffffff00c7c7fd00
ffffff00ffffff00fdfdff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fdfdff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00ffffff008888fe00cbcbff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00d4d4fe000000ff002525ff00fbfbff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00f1f1ff001414ff000101ff000404ff005c5cff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00fbfbff005c5cff000000ff000000ff000101ff000404ff00acacfe00ffffff00ffffff00ffffff00ffffff00c5c5fc00
ffffff007474fe000000ff000000ff000000ff000000ff000000ff000000ff00cdcdff00ffffff00ffffff00ffffff00c5c5fc00
dadaff000404ff000101ff000000ff000000ff000000ff000000ff000000ff002525ff00ffffff00ffffff00ffffff00c5c5fc00
ffffff008888fe000000ff000000ff000000ff000000ff000000ff000000ff00d4d4fe00ffffff00ffffff00ffffff00c5c5fc00
ffffff00fbfbff007474fe000000ff000000ff000000ff000000ff00c2c2fa00ffffff00ffffff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00f3f3ff002525ff000000ff000000ff007474fe00ffffff00ffffff00ffffff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00ffffff00e2e2fe000000ff003d3dff00f7f7ff00ffffff00ffffff00ffffff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00ffffff00ffffff008888fe00d0d0fb00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c2c2fa00
i$2d               13  31 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00acacfe003d3dff000000ff001414ff00acacfe00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00cbcbff000000ff000000ff000000ff000000ff000000ff00cbcbff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff007474fe000000ff003d3dff00ffffff005c5cff000404ff005c5cff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff003d3dff000404ff009b9bfe00ffffff00b9b9fe000101ff003d3dff00fdfdff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00ffffff00b9b9fe000000ff000a0aff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00ffffff008888fe000000ff003d3dff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff00ffffff002525ff000000ff005c5cff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00ffffff005c5cff000101ff000101ff00acacfe00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff00b9b9fe000404ff000000ff003d3dff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff00ffffff002525ff000000ff000000ff00b9b9fe00fdfdff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00ffffff007474fe000000ff000000ff005c5cff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff00eeeefd001414ff000000ff002525ff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c9c9ff00
ffffff008888fe000000ff000000ff009b9bfe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff003d3dff000000ff000000ff000000ff000000ff000000ff001414ff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff001414ff000000ff000000ff000000ff000000ff000000ff001414ff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00ffffff00f1f1ff005c5cff00f9f9ff00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00ffffff007474fe000000ff009b9bfe00ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd00
ffffff00ffffff00ffffff00b9b9fe000000ff000000ff000000ff00d8d8fd00ffffff00ffffff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00dadaff000404ff000000ff000000ff000000ff001414ff00eeeefd00ffffff00ffffff00ffffff00c5c5fc00
ffffff00f1f1ff001414ff000000ff000000ff000000ff000000ff000101ff003d3dff00f7f7ff00ffffff00ffffff00c5c5fc00
ffffff008888fe000000ff000000ff000000ff000000ff000000ff000000ff000000ff009b9bfe00ffffff00ffffff00c5c5fc00
ffffff00f1f1ff002525ff000000ff000000ff000000ff000000ff000000ff003d3dff00ffffff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00e2e2fe000a0aff000000ff000000ff000000ff002525ff00f5f5ff00ffffff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00ffffff00c7c7fd000000ff000000ff000404ff00ddddff00ffffff00ffffff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00ffffff00ffffff008888fe000000ff00acacfe00ffffff00ffffff00ffffff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00ffffff00ffffff00f1f1ff007474fe00f7f7ff00ffffff00ffffff00ffffff00ffffff00ffffff00c5c5fc00
ffffff00ffffff00ffffff00ffffff00ffffff00fdfdff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c5c5fc00
i$2c               13  31 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00aaaaab00333434000000000012121200aaaaab00ffffff00ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00cccccc000000000000000000000000000000000000000000cccccc00ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00737373000000000044444500ffffff00656565000000000065656500ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff0044444500000000009a999a00ffffff00bbbbbb000000000033343400ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00ffffff00ffffff00ffffff00ffffff00bbbbbb000000000012121200ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00ffffff00ffffff00ffffff00ffffff00888888000000000033343400ffffff00ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff00ffffff00ffffff00ffffff00252526000000000065656500ffffff00ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff00ffffff00ffffff00656565000000000000000000aaaaab00ffffff00ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff00ffffff00bbbbbb00000000000000000033343400ffffff00ffffff00ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff00ffffff00252526000000000000000000bbbbbb00ffffff00ffffff00ffffff00ffffff00ffffff00e3e1e300
ffffff00ffffff0073737300000000000000000065656500ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e3e1e300
ffffff00efedef00121212000000000025252600ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e1e1e100
ffffff008888880000000000000000009a999a00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e1e1e100
ffffff0044444500000000000000000000000000000000000000000012121200ffffff00ffffff00ffffff00ffffff00e1e1e100
ffffff0012121200000000000000000000000000000000000000000012121200ffffff00ffffff00ffffff00ffffff00e1dfe100
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e1dfe100
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dfdfdf00
ffffff00ffffff00ffffff00ffffff00bbbbbb0044444500aaaaab00ffffff00ffffff00ffffff00ffffff00ffffff00dfdfdf00
ffffff00ffffff00ffffff00c3c3c400000000000000000001010100bbbbbb00ffffff00ffffff00ffffff00ffffff00dfdfdf00
ffffff00ffffff00ffffff007373730000000000000000000000000056565600ffffff00ffffff00ffffff00ffffff00dfdddf00
ffffff00ffffff00ffffff00a2a2a2000000000000000000000000009a999a00ffffff00ffffff00ffffff00ffffff00dfdddf00
ffffff00d0d0d000656565009a999a004444450000000000444445009a999a0065656500cccccc00ffffff00ffffff00dddddd00
e5e3e5000000000000000000000000001b1b1c000000000012121200000000000000000000000000dddddd00ffffff00dddddd00
7373730000000000000000000000000012121200444445000a0a0a0000000000000000000000000073737300ffffff00dddddd00
b2b2b200000000000000000000000000656565009a999a0056565600000000000000000000000000a2a2a200ffffff00dddbdd00
ffffff00656565001212120033343400e5e5e50044444500ebeded00444445000e0e0f0056565600fdfdfd00ffffff00dddbdd00
ffffff00ffffff00e9ebeb00fdfdfd00f9f7f90000000000f5f5f700fdfdfd00e7e7e700ffffff00ffffff00ffffff00dbdbdb00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dbdbdb00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dbdbdb00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dbdbdb00
i$Kc               13  31 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
cccccc000000000033343400ffffff00ffffff00ffffff008888880000000000000000009a999a00ffffff00ffffff00e5e3e500
cccccc000000000033343400ffffff00ffffff00efedef00121212000000000056565600ffffff00ffffff00ffffff00e5e3e500
cccccc000000000033343400ffffff00ffffff00565656000000000025252600efeded00ffffff00ffffff00ffffff00e5e3e500
cccccc000000000033343400ffffff009a999a0000000000000000009a999a00ffffff00ffffff00ffffff00ffffff00e5e3e500
cccccc000000000033343400ededed00252526000000000056565600ffffff00ffffff00ffffff00ffffff00ffffff00e5e3e500
cccccc000000000033343400656565000000000000000000dddddd00ffffff00ffffff00ffffff00ffffff00ffffff00e3e3e300
cccccc000000000012121200000000000000000000000000aaaaab00ffffff00ffffff00ffffff00ffffff00ffffff00e3e3e300
cccccc00000000000000000000000000000000000000000044444500ffffff00ffffff00ffffff00ffffff00ffffff00e3e3e300
cccccc000000000000000000252526009a999a000000000000000000cccccc00ffffff00ffffff00ffffff00ffffff00e3e3e300
cccccc0000000000000000009a999a00fffdfd00252526000000000073737300ffffff00ffffff00ffffff00ffffff00e3e1e300
cccccc000000000033343400ffffff00ffffff00737373000000000012121200ffffff00ffffff00ffffff00ffffff00e3e1e300
cccccc000000000033343400ffffff00ffffff00dddddd00000000000000000088888800ffffff00ffffff00ffffff00e3e1e300
cccccc000000000033343400ffffff00ffffff00ffffff00444445000000000033343400ffffff00ffffff00ffffff00e1e1e100
cccccc000000000033343400ffffff00ffffff00ffffff009a999a000000000000000000bbbbbb00ffffff00ffffff00e1e1e100
cccccc000000000033343400ffffff00ffffff00ffffff00ffffff00121212000000000056565600ffffff00ffffff00e1e1e100
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e1dfe100
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e1dfe100
ffffff00ffffff00ffffff00ffffff00fdfdfd009292920056565600d4d4d400ffffff00ffffff00ffffff00ffffff00e1dfe100
ffffff00ffffff00ffffff00ffffff0092929200000000000000000000000000e9e7e900ffffff00ffffff00ffffff00dfdfdf00
ffffff00ffffff00ffffff00fdfdfd001b1b1c00000000000000000000000000b2b2b200ffffff00ffffff00ffffff00dfdfdf00
ffffff00ffffff00ffffff00ffffff0065656500000000000000000000000000d7d7d700ffffff00ffffff00ffffff00dfdfdf00
ffffff00ffffff00b2b2b200656565009a999a002525260000000000656565008888880065656500e9e7e900ffffff00dfdddf00
ffffff00aaaaab0000000000000000000606060012121200000000001b1b1c0000000000000000001b1b1c00ffffff00dfdddf00
ffffff004444450000000000000000000000000025252600444445000606060000000000000000000e0e0f00ffffff00dddddd00
ffffff006565650000000000000000000000000088888800b2b2b2003334340000000000000000000a0a0a00ffffff00dddddd00
ffffff00f1f1f200444445000e0e0f0073737300d7d7d90073737300e1dfe10025252600121212009a999a00ffffff00dddddd00
ffffff00ffffff00fbf9f900e7e7e700ffffff00d4d4d4000a0a0a00ffffff00f7f7f700ebebeb00fdfdfd00ffffff00dddbdd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dddbdd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dbdbdb00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dbdbdb00
i$2s               13  31 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00aaaaab00333434000000000012121200aaaaab00ffffff00ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00cccccc000000000000000000000000000000000000000000cccccc00ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00737373000000000044444500ffffff00656565000000000065656500ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff0044444500000000009a999a00ffffff00bbbbbb000000000033343400ffffff00ffffff00ffffff00ffffff00e5e3e500
ffffff00ffffff00ffffff00ffffff00ffffff00bbbbbb000000000012121200ffffff00ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff00ffffff00ffffff00ffffff00888888000000000033343400ffffff00ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff00ffffff00ffffff00ffffff00252526000000000065656500ffffff00ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff00ffffff00ffffff00656565000000000000000000aaaaab00ffffff00ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff00ffffff00bbbbbb00000000000000000033343400ffffff00ffffff00ffffff00ffffff00ffffff00e3e3e300
ffffff00ffffff00ffffff00252526000000000000000000bbbbbb00ffffff00ffffff00ffffff00ffffff00ffffff00e3e1e300
ffffff00ffffff0073737300000000000000000065656500ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e3e1e300
ffffff00efedef00121212000000000025252600ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e1e1e100
ffffff008888880000000000000000009a999a00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e1e1e100
ffffff0044444500000000000000000000000000000000000000000012121200ffffff00ffffff00ffffff00ffffff00e1e1e100
ffffff0012121200000000000000000000000000000000000000000012121200ffffff00ffffff00ffffff00ffffff00e1dfe100
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e1dfe100
ffffff00ffffff00ffffff00ffffff00ffffff00c3c3c400d7d7d900ffffff00ffffff00ffffff00ffffff00ffffff00dfdfdf00
ffffff00ffffff00ffffff00ffffff009a999a000000000001010100c3c3c400ffffff00ffffff00ffffff00ffffff00dfdfdf00
ffffff00ffffff00ffffff008888880000000000000000000000000000000000bbbbbb00ffffff00ffffff00ffffff00dfdfdf00
ffffff00ffffff00bbbbbb00000000000000000000000000000000000000000000000000e1e1e100ffffff00ffffff00dfdddf00
ffffff00f5f3f5000a0a0a0000000000000000000000000000000000000000000000000033343400fdfdfd00ffffff00dfdddf00
ffffff00888888000000000000000000000000000000000000000000000000000000000000000000c3c3c400ffffff00dddddd00
ffffff0025252600000000000000000000000000000000000000000000000000000000000000000056565600ffffff00dddddd00
ffffff0003030300000000000000000001020300888888007373730000000000000000000102030025252600ffffff00dddddd00
ffffff0073737300000000000000000065656500c3c3c400c3c3c400252526000000000000000000a2a2a200ffffff00dddbdd00
ffffff00fbfbfb00b2b2b2009a999a00fbf9f90073737300b2b2b200f7f7f70092929200cccccc00ffffff00ffffff00dbdbdb00
ffffff00ffffff00ffffff00ffffff00fbf9f9003334340065656500ffffff00fdfdfd00ffffff00ffffff00ffffff00dbdbdb00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dbdbdb00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dbdbdb00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dbdbdb00
i$cardback         12  1  
1e110000a45f0200d67f0300c7750300905402006b3f010097570200cd790400d68003009a5b02002a1800005c340100

