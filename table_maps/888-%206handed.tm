.osdb2

// OpenScrape 2.0.0

// 2012-07-14 08:05:40
// 32 bits per pixel

//
// sizes
//

z$clientsize       794  546

//
// strings
//

s$activemethod              2
s$buttonclickmethod         Single
s$handresetmethod           1
s$nchairs                   6
s$network                   pacific
s$potmethod                 2
s$sitename                  888-6ring
s$swagconfirmationmethod    Enter
s$swagdeletionmethod        Backspace
s$swagselectionmethod       Dbl Click
s$swagtextmethod            1
s$t3type                    0.25
s$titletext                 /
s$ttlimits0                 ^* ^LH ^v/^V

//
// regions
//

r$c0cardface0        264 153 279 187        0    0 H0
r$c0cardface1        318 153 333 187        0    0 H0
r$c0cardface2        372 153 387 187        0    0 H0
r$c0cardface3        426 153 441 187        0    0 H0
r$c0cardface4        480 153 495 187        0    0 H0
r$c0handnumber       122 486 182 496   6912a6    0 T1
r$c0pot0             334 139 468 152   ffffff  180 T3
r$chatbox             12 519  39 532        0    0 N
r$i0button           329 439 340 450   ff0000    0 N
r$i0label            386 445 415 456   d68205    0 T0
r$i0state            329 461 329 461   1f1301    0 C
r$i1button           485 439 496 450   ff0000    0 N
r$i1label            487 445 614 457   2ebd66    0 T0
r$i1state            485 461 485 461     200d    0 C
r$i2button           641 439 652 450   ff0000    0 N
r$i2label            641 445 770 456       ff  128 T0
r$i2state            641 439 641 439   737373    0 C
r$i3button           641 439 652 450        0    0 N
r$i3edit             717 501 779 513        0    0 N
r$i3label            641 445 770 457     7efe  128 T0
r$i3state            641 461 641 461    11325    0 C
r$i4button           485 439 496 450        0    0 N
r$i4label            487 445 614 457       ff    0 T0
r$i4state            485 461 485 461       25    0 C
r$i5button           329 527 333 531        0    0 N
r$i5label            329 530 329 530   ffffff    0 H0
r$i5state            335 528 335 528   ffffff    0 C
r$i6button           485 527 489 532        0    0 N
r$i6label            499 527 504 535        0    0 H0
r$i6state            485 530 485 530   404040    0 C
r$i7button           641 439 652 450        0    0 N
r$i7label            641 445 770 457       ff    0 T0
r$i7state            641 461 641 461       25    0 C
r$i8button           329 527 333 531        0    0 N
r$i8label            329 530 329 530        0    0 H0
r$i8state            335 528 335 528        0    0 C
r$p0active           489  70 493  76   f89432    0 H1
r$p0balance          474  66 573  78    1dcfc    0 T0
r$p0bet              478 123 556 137   ffffff  180 T3
r$p0cardback         508  29 514  35     ffff    0 H0
r$p0cardface0        458   1 473  35        0    0 H0
r$p0cardface1        508   1 523  35        0    0 H0
r$p0dealer           572 103 573 104   ca6f04    4 C
r$p0name             474  48 573  63   ffffff    0 T0
r$p0seated           504  23 519  24   a87443  -13 C
r$p1active           693 216 697 222   f89432    0 H1
r$p1balance          678 212 777 224    1dcfc    0 T0
r$p1bet              548 224 626 238   ffffff  180 T3
r$p1cardback         712 175 718 181     ffff    0 H0
r$p1cardface0        662 147 677 181        0    0 H0
r$p1cardface1        712 147 727 181        0    0 H0
r$p1dealer           613 169 614 170   c66803    4 C
r$p1name             678 195 777 207   ffffff    0 T0
r$p1seated           760 202 761 217   906437  -12 C
r$p2active           489 392 493 398   f89432    0 H1
r$p2balance          474 388 573 400    1dcfc    0 T0
r$p2bet              474 308 553 322   bbffff  180 T3
r$p2cardback         508 351 514 357     ffff    0 H0
r$p2cardface0        458 323 473 357        0    0 H0
r$p2cardface1        508 323 523 357        0    0 H0
r$p2dealer           572 305 573 306   c66803    4 C
r$p2name             474 370 573 384   ffffff    0 T0
r$p2seated           505 413 520 414   765d37  -12 C
r$p3active           249 392 253 398   f89432    0 H1
r$p3balance          234 388 333 400    1dcfc    0 T0
r$p3bet              233 308 316 322   ffffff  180 T3
r$p3cardback         268 351 274 357     ffff    0 H0
r$p3cardface0        218 323 233 357        0    0 H0
r$p3cardface1        268 323 283 357        0    0 H0
r$p3dealer           331 305 332 306   c66903    4 C
r$p3name             234 369 333 384   ffffff    0 T0
r$p3seated           205 411 220 412   7a6039  -13 C
r$p4active            63 216  67 222   f89432    0 H1
r$p4balance           48 212 147 224    1dcfc    0 T0
r$p4bet              164 224 250 238   ffffff  180 T3
r$p4cardback          82 175  88 181     ffff    0 H0
r$p4cardface0         32 147  47 181        0    0 H0
r$p4cardface1         82 147  97 181        0    0 H0
r$p4dealer           179 169 180 170   c66803    4 C
r$p4name              48 194 147 209   ffffff    0 T0
r$p4seated            34 202  35 217   8f6538  -10 C
r$p5active           249  70 253  76   f89432    0 H1
r$p5balance          234  66 333  78    1dcfc    0 T0
r$p5bet              230 122 316 136   ffffff  180 T3
r$p5cardback         268  29 274  35     ffff    0 H0
r$p5cardface0        268   1 283  35        0    0 H0
r$p5cardface1        218   1 233  35        0    0 H0
r$p5dealer           331 103 332 104   ca6f04    4 C
r$p5name             234  49 333  61   ffffff    0 T0
r$p5seated           265  23 280  24   9d7045   -7 C

//
// fonts
//

t0$T 100 100 100 1ff 100 100 100
t0$T 100 100 100 1ff 1ff 100 100 100
t0$7 100 100 107 11f 178 1e0 180
t0$7 100 101 106 118 160 180
t0$7 100 101 107 11e 178 1e0 180
t0$Y 100 c0 20 1f 20 c0 100
t0$I 101 1ff 101
t0$j 101 5fe
t0$X 101 c6 38 38 c6 101
t0$Z 103 105 119 121 141 181
t0$J 1 101 101 1fe
t0$j 1 101 5fe
t0$j 1 101 9fe
t0$- 1 1 1 1
t0$_ 1 1 1 1 1 1 1
t0$_ 1 1 1 1 1 1 1 1
t0$i 17f
t0$i 17f 17f
t0$V 180 70 c 3 3 c 70 180
t0$V 180 70 e 1 e 70 180
t0$y 180 71 e 70 180
t0$W 180 78 7 c 70 180 70 c 7 78 180
t0$4 18 28 48 88 1ff 1ff 8
t0$4 18 28 48 88 1ff 8
t0$4 18 28 48 88 1ff 8 8
t0$X 183 44 38 38 44 183
t0$4 18 38 68 c8 1ff 1ff 8
t0$$ 184 244 fff 224 224 218
t0$, 1 e
t0$W 1e0 1c 3 c 70 180 70 c 3 1c 1e0
t0$5 1e2 121 121 121 121 11e
t0$5 1e2 1e3 121 121 121 13f 11e
t0$Q 1f0 208 404 404 404 406 209 1f1
t0$U 1fc 2 1 1 1 2 1fc
t0$H 1ff 10 10 10 10 10 1ff
t0$D 1ff 101 101 101 101 82 7c
t0$K 1ff 10 28 44 82 101
t0$P 1ff 108 108 108 108 f0
t0$F 1ff 110 110 110 110
t0$F 1ff 110 110 110 110 100
t0$R 1ff 110 110 118 114 e2 1
t0$R 1ff 110 110 118 e6 1
t0$L 1ff 1 1 1 1
t0$L 1ff 1 1 1 1 1
t0$E 1ff 111 111 111 111 111
t0$B 1ff 111 111 111 111 ee
t0$B 1ff 111 111 111 f1 e
t0$N 1ff 180 60 10 c 3 1ff
t0$M 1ff 180 70 c c 70 180 1ff
t0$F 1ff 1ff 110 110 110 110
t0$R 1ff 1ff 110 118 1fe e7 1
t0$B 1ff 1ff 111 111 111 1ff ee
t0$p 1ff 84 104 104 104 f8
t0$5 2 1e1 1e1 121 121 13f 11e
t0$i 27f
t0$. 3
t0$s 31 49 49 46
t0$s 31 49 49 49 46
t0$s 31 79 49 4f 46
t0$A 3 1c 64 184 184 64 1c 3
t0$A 3 1c e4 104 e4 1c 3
t0$. 3 3
t0$c 3e 41 41 41 22
t0$c 3e 41 41 41 41
t0$o 3e 41 41 41 41 3e
t0$d 3e 41 41 41 42 3ff
t0$e 3e 49 49 49 49 3a
t0$c 3e 7f 41 41 41 41
t0$o 3e 7f 41 41 41 7f 3e
t0$d 3e 7f 41 41 43 3ff 3ff
t0$e 3e 7f 49 49 49 7b 3a
t0$l 3ff
t0$h 3ff 20 40 40 40 3f
t0$b 3ff 21 41 41 41 3e
t0$l 3ff 3ff
t0$k 3ff 3ff 1c 36 63 41
t0$h 3ff 3ff 60 40 40 7f 3f
t0$k 3ff 8 14 22 41
t0$t 40 1fe 1ff 41 41
t0$t 40 1fe 41 41
t0$t 40 1fe 41 41 41
t0$f 40 1ff 240 240
t0$x 41 36 8 36 41
t0$z 43 45 49 51 61
t0$z 43 4d 51 61
t0$v 60 1c 3 1c 60
t0$w 60 1c 3 1c 60 1c 3 1c 60
t0$x 63 14 8 14 63
t0$a 6 49 49 49 3f
t0$a 6 49 49 49 49 3f
t0$a 6 4f 49 49 7f 3f
t0$C 7c 82 101 101 101 101
t0$C 7c 82 101 101 101 101 82
t0$O 7c 82 101 101 101 101 82 7c
t0$G 7c 82 101 101 111 111 9e
t0$G 7c 82 101 101 111 111 9f
t0$C 7c fe 183 101 101 101 101
t0$� 7c fe 3ff 3ff 82 82
t0$u 7e 1 1 1 2 7f
t0$6 7e a1 121 121 121 1e
t0$6 7e ff 1a1 121 121 13f 1e
t0$r 7f 20 40
t0$r 7f 20 40 40
t0$n 7f 20 40 40 40 3f
t0$m 7f 40 40 40 3f 40 40 40 3f
t0$n 7f 7f 60 40 40 7f 3f
t0$2 81 103 107 10d 119 1f1 e1
t0$1 81 81 1ff 1 1
t0$1 81 81 1ff 1ff 1 1
t0$3 82 101 111 111 111 1ff ee
t0$3 82 101 111 111 111 ee
t0$3 82 183 111 111 111 1ff ee
t0$2 83 105 109 109 111 e1
t0$2 83 187 10d 109 119 1f1 e1
t0$2 c3 105 109 109 111 e1
t0$$ c4 1e6 7ff 7ff 19e 8c
t0$S e2 111 111 111 111 8e
t0$8 ee 111 111 111 111 ee
t0$8 ee 1ff 111 111 111 1ff ee
t0$9 f0 109 109 109 10a fc
t0$9 f0 1f9 109 109 10b 1fe fc
t0$q f8 104 104 104 108 1ff
t0$g f8 105 105 105 109 1fe
t0$, f e
t0$0 fe 101 101 101 101 fe
t0$0 fe 1ff 101 101 101 1ff fe
t1$1 10 20 7f
t1$5 1a 71 51 51 4e
t1$2 21 43 45 49 31
t1$3 22 41 49 49 36
t1$8 36 49 49 49 36
t1$9 3a 45 45 45 3e
t1$0 3e 41 41 41 3e
t1$6 3e 51 51 51 2e
t1$c 3e 7f 41 41 41 41
t1$e 3e 7f 49 49 49 7b 3a
t1$l 3ff 3ff
t1$k 3ff 3ff 1c 36 63 41
t1$h 3ff 3ff 60 40 40 7f 3f
t1$7 40 43 4c 70 40
t1$a 6 4f 49 49 7f 3f
t1$4 c 14 24 44 7f 4
t3$7 100 101 107 11c 170 1c0
t3$3 101 101 111 131 ce
t3$7 101 106 118 160 180
t3$4 18 28 48 88 1ff 8
t3$2 183 105 109 119 e1
t3$5 1e3 121 121 121 11e
t3$3 2 101 111 111 1b1 ee
t3$. 3 3
t3$, 7 4
t3$� 7c 82 3ff 82 c6
t3$6 7e a3 121 121 13e c
t3$1 81 81 1ff 1 1
t3$8 ee 131 111 111 ee
t3$9 f1 109 109 10a fc
t3$� f8 104 7ff 104 18c
t3$0 fe 101 101 101 fe
t3$6 fe a3 121 121 13e c

//
// points
//

p1$   0    1
p1$   0    2
p1$   0    5
p1$   1    0
p1$   1    1
p1$   1    2
p1$   1    3
p1$   1    6
p1$   2    0
p1$   2    3
p1$   2    6
p1$   3    0
p1$   3    3
p1$   3    4
p1$   3    5
p1$   3    6
p1$   4    1
p1$   4    4
p1$   4    5

//
// hash
//

h0$2d                 02ff0a41
h0$unseated3          0914e58b
h0$unseated4          0c9f5c42
h0$Ad                 12c848a3
h0$7c                 1351a87d
h0$5h                 14970852
h0$Qs                 14b3f07f
h0$8c                 15944db1
h0$sitout             18116a0f
h0$8h                 1e9f91b4
h0$6h                 1fb0f108
h0$4d                 22ed2b82
h0$8s                 26c9a3c5
h0$Qh                 26e54f37
h0$Qc                 2deabbcf
h0$unseated6          36137968
h0$7s                 37e8f522
h0$9c                 42371f0e
h0$Js                 44bf765e
h0$Ac                 48bc78b7
h0$3c                 4cb31318
h0$7h                 4dd827bb
h0$2h                 4fb003b3
h0$5c                 52b859b3
h0$Th                 530def25
h0$unseated5          54422b1f
h0$unseated0          574bfb5b
h0$Ks                 5cf33f00
h0$2c                 5f2f31fa
h0$8d                 60c69cf2
h0$3s                 6184d58f
h0$As                 7aa11eb7
h0$Jd                 7b3cc471
h0$5s                 7cd59282
h0$9d                 863be1ae
h0$unseated7          89d6ed59
h0$call               8ca887cb
h0$4s                 9099e7a4
h0$4c                 95ccbe9f
h0$9s                 a218bec4
h0$Ah                 a2ceb4e8
h0$7d                 a925e46a
h0$3h                 a9a5b0f8
h0$Jc                 aa114f40
h0$Qd                 c618d2ca
h0$9h                 c7b5b2d7
h0$3d                 d553e116
h0$Ts                 d6164d06
h0$Jh                 de237b14
h0$Td                 e2365e01
h0$5d                 e328e911
h0$6c                 e52c508a
h0$sitin              e8f59493
h0$unseated1          f05d5d2a
h0$6s                 f061c276
h0$autopost           f29da8bb
h0$Tc                 f2a097b5
h0$cardback           f458abf6
h0$unseated8          f50e5ba3
h0$Kh                 f82e3a9b
h0$Kd                 f8f612f0
h0$4h                 fb34f25f
h0$2s                 fbe28566
h0$6d                 fbe8b38e
h0$Kc                 ff995896
h1$out                f621097c

//
// images
//

i$Th               16  35 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00bcbcff000101ff008282fe00ffffff00ffffff00ffffff003e3eff000101ff003e3eff00ededff00ffffff00ffffff00ffffff00
ffffff00ffffff00fdfdff005d5dfe000101ff008282fe00ffffff00ffffff008282fe000101ff000000ff000000ff008282fe00ffffff00ffffff00ffffff00
ffffff00ffffff00bcbcff000000ff000000ff008282fe00ffffff00ffffff002727ff000000ff000000ff000000ff000a0aff00ffffff00ffffff00ffffff00
ffffff00ceceff001212ff000000ff000101ff008282fe00ffffff00ddddff000000ff002727ff00ffffff003e3eff000000ff00bcbcff00ffffff00ffffff00
ffffff002727ff000000ff001212ff000404ff008282fe00ffffff00bcbcff000000ff005d5dfe00ffffff008282fe000000ff009797fe00ffffff00ffffff00
fdfdff003e3eff002727ff005d5dfe000101ff008282fe00ffffff009797fe000101ff008282fe00ffffff009797fe000101ff008282fe00ffffff00ffffff00
ffffff005d5dfe00ddddff008282fe000000ff008282fe00ffffff008282fe000101ff008282fe00ffffff00a8a8ff000000ff008282fe00fdfdff00ffffff00
ffffff00ffffff00fdfdff008282fe000000ff008282fe00ffffff008282fe000404ff008282fe00ffffff00a8a8ff000000ff008282fe00fbfbff00ffffff00
ffffff00ffffff00f9f9ff008282fe000404ff008282fe00ffffff008282fe000404ff008282fe00ffffff00a8a8ff000000ff008282fe00ffffff00ffffff00
ffffff00ffffff00fbfbff008282fe000000ff008282fe00ffffff008282fe000404ff008282fe00ffffff00a8a8ff000000ff008282fe00fbfbff00ffffff00
ffffff00ffffff00ffffff008282fe000000ff008282fe00ffffff008282fe000404ff008282fe00ffffff00a8a8ff000000ff008282fe00f9f9ff00ffffff00
ffffff00ffffff00fbfbff008282fe000000ff008282fe00ffffff008282fe000404ff008282fe00ffffff009797fe000000ff008282fe00ffffff00ffffff00
ffffff00ffffff00f9f9ff008282fe000404ff008282fe00ffffff00a8a8ff000404ff005d5dfe00ffffff008282fe000000ff009797fe00ffffff00ffffff00
ffffff00ffffff00fbfbff008282fe000000ff008282fe00ffffff00ddddff000000ff002727ff00ffffff003e3eff000000ff00bcbcff00ffffff00ffffff00
ffffff00ffffff00ffffff008282fe000000ff008282fe00ffffff00ffffff002727ff000000ff000000ff000000ff001212ff00ffffff00ffffff00ffffff00
ffffff00ffffff00fbfbff008282fe000000ff008282fe00ffffff00ffffff008282fe000101ff000000ff000000ff008282fe00fbfbff00ffffff00ffffff00
ffffff00ffffff00f9f9ff008282fe000404ff008282fe00ffffff00ffffff00ffffff005d5dfe000000ff003e3eff00eaeafd00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00fbfbff00fbfbff00ffffff00ffffff00ffffff00ffffff00fdfdff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00f5f5ff009797fe009797fe00e6e6fe00ffffff00ffffff00d7d7ff009797fe00a8a8ff00fdfdff00ffffff00ffffff00ffffff00
ffffff00ffffff00f7f7ff005d5dfe000000ff000000ff002727ff00e3e3ff00c7c7fd001212ff000000ff000000ff008282fe00ffffff00ffffff00ffffff00
ffffff00ffffff00b0b0fe000000ff000000ff000000ff000000ff005d5dfe003e3eff000000ff000000ff000000ff000101ff00ceceff00ffffff00ffffff00
ffffff00ffffff008282fe000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff009797fe00ffffff00ffffff00
ffffff00fbfbff008282fe000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff00b0b0fe00ffffff00ffffff00
ffffff00ffffff00ceceff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff001212ff00eaeafd00ffffff00ffffff00
ffffff00ffffff00fdfdff005d5dfe000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff008282fe00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ededff002727ff000101ff000000ff000000ff000000ff000000ff000000ff005d5dfe00f3f3ff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ddddff002727ff000000ff000000ff000000ff000000ff003e3eff00eaeafd00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd000a0aff000000ff000000ff002727ff00ddddff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00b0b0fe000404ff000a0aff00ceceff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff005d5dfe008282fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e3e3ff00ededff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
i$cardback         7   7  
e88e04008c4f02000704000000000000000000000000000047280200
a05b0300130c01000000000000000000000000000000000000000000
140c010000000000160d010053300200643a02002d1b010000000000
00000000190f0100a9600300e1870400e58b0400cd78040045270200
0000000057330200e2880400de850400db840400e38a0400a9600300
000000006f400200e58c0400da820400da820400dd850400c2710400
00000000341f0100d37d0400e2870400dd840400e48a040079440200
i$As               16  35 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff0037373800000000000000000052525500ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00efeded0000000000000000000000000011111200efeded00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00a9a9a90000000000000000000000000000000000a9a9a900ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00767676000000000037373800111112000000000076767600ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00373738000000000067676700525255000000000037373800ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00efeded0000000000000000008b8b8b00848484000000000000000000efeded00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00a9a9a9000000000000000000cfcfcf00dddddd000000000000000000a9a9a900ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00767676000000000011111200ffffff00fdfdfd00373738000000000076767600ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00373738000000000052525500ffffff00ffffff00767676000000000026262700ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00efeded0000000000000000008b8b8b00ffffff00ffffff00a9a9a9000000000000000000cfcfcf00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00a9a9a90000000000000000000000000000000000000000000000000000000000000000008b8b8b00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff0076767600000000000000000000000000000000000000000000000000000000000000000052525500ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff0037373800000000000000000000000000000000000000000000000000000000000000000011111200ffffff00ffffff00ffffff00ffffff00
ffffff00efeded0000000000000000008b8b8b00ffffff00ffffff00ffffff00ffffff008b8b8b000000000000000000cfcfcf00ffffff00ffffff00ffffff00
ffffff00a9a9a9000000000000000000bbbbbb00ffffff00ffffff00ffffff00ffffff00cfcfcf0000000000000000008b8b8b00ffffff00ffffff00ffffff00
ffffff00767676000000000000000000efeded00ffffff00ffffff00ffffff00ffffff00ffffff00111112000000000047474700ffffff00ffffff00ffffff00
ffffff00373738000000000037373800ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00525255000000000000000000efeded00ffffff00ffffff00
ffffff00ffffff00ffffff00fdfdfd00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00cfcfcf000c0c0c0011111200cfcfcf00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00b6b6b60000000000000000000000000000000000b6b6b600ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00c8c8c800000000000000000000000000000000000000000000000000c8c8c800ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00efeded000303030000000000000000000000000000000000000000000000000007070700f1eff100ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff005252550000000000000000000000000000000000000000000000000000000000000000005d5d5d00ffffff00ffffff00ffffff00
ffffff00ffffff00d9d9d90000000000000000000000000000000000000000000000000000000000000000000000000000000000dddbdb00ffffff00ffffff00
ffffff00ffffff0076767600000000000000000000000000000000000000000000000000000000000000000000000000000000007c7c7c00ffffff00ffffff00
ffffff00ffffff00373738000000000000000000000000000303030026262700373738000203010000000000000000000000000037373800ffffff00ffffff00
ffffff00ffffff00373738000000000000000000000000001b1b1c00bdbebd00b6b6b6001b1b1c0000000000000000000101010037373800ffffff00ffffff00
ffffff00ffffff00dbd9db00111112000000000001010100b3b3b300bbbbbb00bbbbbb00aeaeae00010101000000000011111200dfdfdd00ffffff00ffffff00
ffffff00ffffff00ffffff00efefef00a9a9a900cfcfcf00ffffff006767670067676700ffffff00cfcfcf00a9a9a900f1f1f100ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00f6f6f6001b1b1c001b1b1c00f6f6f600ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fffffd00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
i$9c               16  35 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00a9a9a900111112000000000037373800bbbbbb00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00a9a9a9000000000000000000000000000000000000000000cfcfcf00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00efeded0011111200000000000000000000000000000000000000000047474700ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00a9a9a90000000000000000008b8b8b00ffffff00474747000000000000000000dddddd00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff008b8b8b000000000000000000ffffff00ffffff00cfcfcf000000000000000000a9a9a900ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff008b8b8b000000000026262700ffffff00ffffff00ffffff0000000000000000008b8b8b00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff008b8b8b000000000000000000ffffff00ffffff00efeded00000000000000000067676700ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00a9a9a90000000000000000008b8b8b00ffffff0076767600000000000000000067676700ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00efeded001111120000000000000000000000000000000000000000000000000067676700ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff008b8b8b0000000000000000000000000000000000111112000000000067676700ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff008b8b8b000000000000000000a9a9a900262627000000000067676700ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff0000000000000000008b8b8b00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00dddddd004747470000000000cfcfcf00ffffff00cfcfcf000000000000000000a9a9a900ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00efeded00000000000000000067676700ffffff00525255000000000000000000efeded00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff0037373800000000000000000000000000000000000000000052525500ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00979797000000000000000000000000000000000011111200dddddd00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff0097979700000000000000000047474700cfcfcf00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c8c8c80076767600bbbbbb00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00c0c0c000000000000000000000000000a9a9a900ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00f6f6f6001111120000000000000000000000000000000000f1eff100ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00f3f3f3000000000000000000000000000000000000000000e7e5e700ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff005252550000000000000000000000000037373800fbfdfd00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00f3f3f3006e6e6e0076767600a9a9a900373738000000000026262700a9a9a9007676760067676700ededed00ffffff00ffffff00ffffff00
ffffff00fafafa001b1b1c0000000000000000000707070026262700000000001b1b1c000c0c0c0000000000000000000c0c0c00f3f3f300ffffff00ffffff00
ffffff00a9a9a900000000000000000000000000000000000101010002030100010101000000000000000000000000000000000097979700ffffff00ffffff00
ffffff0084848400000000000000000000000000000000005d5d5d00a0a0a000767676000000000000000000000000000000000084848400ffffff00ffffff00
ffffff00ededed0000000000000000000000000000000000c8c8c8007c7c7c00cfcfcf0000000000000000000000000000000000dfdfdf00ffffff00ffffff00
ffffff00ffffff00c8c8c800262627001b1b1c00a0a0a000fafafa000c0c0c00f3f3f300b1b1b1001b1b1c001b1b1c00bdbebd00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00f9f7f700f6f6f600ffffff00e5e3e50000000000cfcfcf00ffffff00f6f6f600f6f6f600ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dddbdb0097979700cfcfcf00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
i$unseated4        2   6  
817d7c00817d7c00
7f7b7a007f7b7a00
7f7c78007f7c7800
7d7978007d797800
7d7a76007d7a7600
7b7776007b777600
i$8d               16  35 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00a8a8ff002727ff000000ff000000ff008282fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ceceff000000ff000000ff000000ff000000ff000101ff008282fe00fbfbff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff003e3eff000000ff000000ff000000ff000000ff000000ff000000ff00ededff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff000101ff000101ff003e3eff00ffffff008282fe000000ff000000ff00a8a8ff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ddddff000000ff000000ff00a8a8ff00ffffff00ffffff000000ff000000ff009797fe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff000000ff000000ff00a8a8ff00ffffff00ffffff000000ff000000ff00a8a8ff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff003e3eff000000ff005d5dfe00ffffff008282fe000000ff000000ff00ededff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ceceff000000ff000000ff000000ff000101ff000000ff008282fe00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff005d5dfe000101ff000000ff000000ff002727ff00eaeafd00fdfdff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff008282fe000404ff000000ff000000ff000000ff000000ff003e3eff00fbfbff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ededff001212ff000000ff008282fe00ffffff00bcbcff000000ff000000ff009797fe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00bcbcff000000ff000000ff00eaeafd00fdfdff00ffffff003e3eff000101ff005d5dfe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff009797fe000000ff000000ff00ddddff00ffffff00ffffff003e3eff000404ff005d5dfe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ceceff000000ff000000ff005d5dfe00ffffff00bcbcff000101ff000101ff008282fe00fdfdff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff001212ff000101ff000101ff000000ff000000ff000000ff000000ff00b9b9fc00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00a8a8ff000000ff000000ff000000ff000000ff000000ff005d5dfe00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00a8a8ff002727ff000000ff000000ff008282fe00f9f9ff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fdfdff00fbfbff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00a8a8ff00fbfbff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c0c0fd000000ff009797fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00eaeafd001212ff000000ff000101ff00d7d7ff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00fbfbff003e3eff000000ff000000ff000000ff002727ff00f3f3ff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00fbfbff008282fe000000ff000000ff000000ff000000ff000000ff005d5dfe00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00a8a8ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff008282fe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00d7d7ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff00b3b3ff00ffffff00ffffff00ffffff00
ffffff00ffffff00f5f5ff003e3eff000000ff000000ff000000ff000000ff000000ff000000ff000000ff002727ff00e6e6fe00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00efefff002727ff000000ff000000ff000000ff000000ff000000ff000404ff00e6e6fe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ddddff000404ff000000ff000000ff000000ff000000ff00c7c7fd00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00b9b9fc000000ff000000ff000000ff008282fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff008282fe000000ff003e3eff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00fbfbff00efefff003e3eff00e3e3ff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fdfdff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
i$Qs               16  35 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00efeded006767670000000000000000000000000067676700efeded00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00efeded0026262700000000000000000000000000000000000000000026262700dddbdb00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00474747000000000000000000000000000000000000000000000000000000000037373800ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00bbbbbb00000000000000000037373800cfcfcf00ffffff00c8c8c800373738000000000000000000a9a9a900ffffff00ffffff00ffffff00
ffffff00ffffff00676767000000000011111200dddbdb00fffffd00ffffff00ffffff00dddbdb00111112000000000067676700ffffff00ffffff00ffffff00
ffffff00ffffff00373738000000000067676700ffffff00ffffff00ffffff00ffffff00ffffff00676767000000000026262700ffffff00ffffff00ffffff00
ffffff00fffffd0011111200000000008b8b8b00ffffff00ffffff00ffffff00ffffff00ffffff008b8b8b000000000011111200ffffff00ffffff00ffffff00
ffffff00efeded000000000000000000a9a9a900ffffff00ffffff00ffffff00ffffff00ffffff00a9a9a9000000000000000000efeded00ffffff00ffffff00
ffffff00efeded000000000000000000a9a9a900ffffff00ffffff00ffffff00ffffff00ffffff00a9a9a9000000000000000000efeded00ffffff00ffffff00
ffffff00efeded000000000000000000a9a9a900ffffff00ffffff00ffffff00ffffff00ffffff00a9a9a9000000000011111200ffffff00ffffff00ffffff00
ffffff00ffffff0011111200000000008b8b8b00ffffff00ffffff00ffffff00ffffff00ffffff008b8b8b000000000026262700ffffff00ffffff00ffffff00
ffffff00ffffff00373738000000000052525500ffffff00ffffff006767670076767600ffffff00474747000000000067676700ffffff00ffffff00fffffd00
ffffff00ffffff00676767000000000011111200dddddd00ffffff002626270000000000474747000000000000000000a9a9a900ffffff00ffffff00ffffff00
ffffff00ffffff00bbbbbb00000000000000000037373800dbdbd900a9a9a90037373800000000000000000037373800ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff0047474700000000000000000000000000000000000000000000000000000000000000000052525500ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ededed0026262700000000000000000000000000000000000000000000000000000000000101010097979700ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ededed007676760000000000000000000000000067676700474747000000000000000000dddddd00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff008b8b8b0047474700ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00cfcfcf000c0c0c0011111200cfcfcf00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00b6b6b60000000000000000000000000000000000b6b6b600ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00c8c8c800000000000000000000000000000000000000000000000000c8c8c800ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00efeded000303030000000000000000000000000000000000000000000000000007070700f1eff100ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff005252550000000000000000000000000000000000000000000000000000000000000000005d5d5d00ffffff00ffffff00ffffff00
ffffff00ffffff00d9d9d90000000000000000000000000000000000000000000000000000000000000000000000000000000000dddbdb00ffffff00ffffff00
ffffff00ffffff0076767600000000000000000000000000000000000000000000000000000000000000000000000000000000007c7c7c00ffffff00ffffff00
ffffff00ffffff00373738000000000000000000000000000303030026262700373738000203010000000000000000000000000037373800ffffff00ffffff00
ffffff00ffffff00373738000000000000000000000000001b1b1c00bdbebd00b6b6b6001b1b1c0000000000000000000101010037373800ffffff00ffffff00
ffffff00ffffff00dbd9db00111112000000000001010100b3b3b300bbbbbb00bbbbbb00aeaeae00010101000000000011111200dfdfdd00ffffff00ffffff00
ffffff00ffffff00ffffff00efefef00a9a9a900cfcfcf00ffffff006767670067676700ffffff00cfcfcf00a9a9a900f1f1f100ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00f6f6f6001b1b1c001b1b1c00f6f6f600ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fffffd00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
i$sitout           1   1  
00000000
i$2c               16  35 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00cfcfcf0047474700000000000000000067676700ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00dddddd00111112000000000000000000000000000000000052525500ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff0067676700000000000000000000000000000000000000000000000000bbbbbb00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00111112000000000037373800ffffff00cfcfcf00000000000000000076767600ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00dddddd000000000000000000a9a9a900ffffff00ffffff00474747000000000067676700ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00dddddd004747470000000000dddddd00ffffff00ffffff00525255000000000067676700ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff0026262700000000008b8b8b00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00979797000000000000000000dddbdb00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dddddd00111112000000000067676700ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00373738000000000026262700efeded00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00767676000000000011111200c8c8c800ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00bbbbbb000000000000000000bbbbbb00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ededed00262627000000000097979700ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff008b8b8b000000000067676700ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff001111120000000000000000000000000000000000000000000000000067676700ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00cfcfcf000000000000000000000000000000000000000000000000000000000067676700ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff008b8b8b000000000000000000000000000000000000000000000000000000000067676700ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c8c8c80076767600bbbbbb00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00c0c0c000000000000000000000000000a9a9a900ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00f6f6f6001111120000000000000000000000000000000000f1eff100ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00f3f3f3000000000000000000000000000000000000000000e7e5e700ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff005252550000000000000000000000000037373800fbfdfd00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00f3f3f3006e6e6e0076767600a9a9a900373738000000000026262700a9a9a9007676760067676700ededed00ffffff00ffffff00ffffff00
ffffff00fafafa001b1b1c0000000000000000000707070026262700000000001b1b1c000c0c0c0000000000000000000c0c0c00f3f3f300ffffff00ffffff00
ffffff00a9a9a900000000000000000000000000000000000101010002030100010101000000000000000000000000000000000097979700ffffff00ffffff00
ffffff0084848400000000000000000000000000000000005d5d5d00a0a0a000767676000000000000000000000000000000000084848400ffffff00ffffff00
ffffff00ededed0000000000000000000000000000000000c8c8c8007c7c7c00cfcfcf0000000000000000000000000000000000dfdfdf00ffffff00ffffff00
ffffff00ffffff00c8c8c800262627001b1b1c00a0a0a000fafafa000c0c0c00f3f3f300b1b1b1001b1b1c001b1b1c00bdbebd00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00f9f7f700f6f6f600ffffff00e5e3e50000000000cfcfcf00ffffff00f6f6f600f6f6f600ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dddbdb0097979700cfcfcf00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
i$Jc               16  35 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00979797000000000000000000ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00979797000000000000000000ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00979797000000000000000000ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00979797000000000000000000ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00979797000000000000000000ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00979797000000000000000000ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00979797000000000000000000ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00979797000000000000000000ffffff00ffffff00ffffff00ffffff00fdfdfd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00979797000000000000000000ffffff00ffffff00ffffff00ffffff00c8c8c800
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00979797000000000000000000ffffff00ffffff00ffffff00ffffff0097979700
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00979797000000000000000000ffffff00ffffff00ffffff00ffffff0097979700
ffffff00ffffff00ffffff00474747000000000067676700ffffff00ffffff00979797000000000000000000ffffff00ffffff00ffffff00ffffff0097979700
ffffff00ffffff00ffffff00525255000000000037373800ffffff00ffffff008b8b8b000000000000000000ffffff00ffffff00ffffff00ffffff0097979700
ffffff00ffffff00ffffff00767676000000000000000000cfcfcf00ffffff00373738000000000037373800ffffff00ffffff00ffffff00ffffff0097979700
ffffff00ffffff00ffffff00bbbbbb0000000000000000000000000000000000000000000000000076767600ffffff00ffffff00ffffff00ffffff0097979700
ffffff00ffffff00ffffff00ffffff00474747000000000000000000000000000000000011111200efeded00ffffff00ffffff00ffffff00ffffff008b8b8b00
ffffff00ffffff00ffffff00ffffff00efeded0052525500000000000000000047474700cfcfcf00ffffff00ffffff00ffffff00ffffff00ffffff008b8b8b00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff008b8b8b00
ffffff00ffffff00ffffff00ffffff00ffffff00c8c8c80076767600bbbbbb00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff008b8b8b00
ffffff00ffffff00ffffff00ffffff00c0c0c000000000000000000000000000a9a9a900ffffff00ffffff00ffffff00ffffff00ffffff00ffffff008b8b8b00
ffffff00ffffff00ffffff00f6f6f6001111120000000000000000000000000000000000f1eff100ffffff00ffffff00ffffff00ffffff00ffffff008b8b8b00
ffffff00ffffff00ffffff00f3f3f3000000000000000000000000000000000000000000e7e5e700ffffff00ffffff00ffffff00ffffff00ffffff008b8b8b00
ffffff00ffffff00ffffff00ffffff005252550000000000000000000000000037373800fbfdfd00ffffff00ffffff00ffffff00ffffff00ffffff008b8b8b00
ffffff00f3f3f3006e6e6e0076767600a9a9a900373738000000000026262700a9a9a9007676760067676700ededed00ffffff00ffffff00ffffff008b8b8b00
fafafa001b1b1c0000000000000000000707070026262700000000001b1b1c000c0c0c0000000000000000000c0c0c00f3f3f300ffffff00ffffff008b8b8b00
a9a9a900000000000000000000000000000000000101010002030100010101000000000000000000000000000000000097979700ffffff00ffffff0084848400
84848400000000000000000000000000000000005d5d5d00a0a0a000767676000000000000000000000000000000000084848400ffffff00ffffff008b8b8b00
ededed0000000000000000000000000000000000c8c8c8007c7c7c00cfcfcf0000000000000000000000000000000000dfdfdf00ffffff00ffffff0084848400
ffffff00c8c8c800262627001b1b1c00a0a0a000fafafa000c0c0c00f3f3f300b1b1b1001b1b1c001b1b1c00bdbebd00ffffff00ffffff00ffffff0084848400
ffffff00ffffff00f9f7f700f6f6f600ffffff00e5e3e50000000000cfcfcf00ffffff00f6f6f600f6f6f600ffffff00ffffff00ffffff00ffffff007c7c7c00
ffffff00ffffff00ffffff00ffffff00ffffff00dddbdb0097979700cfcfcf00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff007c7c7c00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff007c7c7c00
i$unseated7        2   6  
2d2b29002d2b2b00
2d2b29002d2b2b00
2d2b29002d2b2b00
2d2c2a002e2c2b00
2d2b29002d2b2b00
2d2c2a002e2c2b00
i$2s               16  35 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00cfcfcf0047474700000000000000000067676700ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00dddddd00111112000000000000000000000000000000000052525500ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff0067676700000000000000000000000000000000000000000000000000bbbbbb00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00111112000000000037373800ffffff00cfcfcf00000000000000000076767600ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00dddddd000000000000000000a9a9a900ffffff00ffffff00474747000000000067676700ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00dddddd004747470000000000dddddd00ffffff00ffffff00525255000000000067676700ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff0026262700000000008b8b8b00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00979797000000000000000000dddddd00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dddddd00111112000000000067676700ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00373738000000000026262700ededed00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00767676000000000011111200c8c8c800ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00bbbbbb000000000000000000bbbbbb00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00efeded00262627000000000097979700ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff008b8b8b000000000067676700ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff001111120000000000000000000000000000000000000000000000000067676700ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00cfcfcf000000000000000000000000000000000000000000000000000000000067676700ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff008b8b8b000000000000000000000000000000000000000000000000000000000067676700ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00cfcfcf000c0c0c0011111200cfcfcf00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00b6b6b60000000000000000000000000000000000b6b6b600ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00c8c8c800000000000000000000000000000000000000000000000000c8c8c800ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00efeded000303030000000000000000000000000000000000000000000000000007070700f1eff100ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff005252550000000000000000000000000000000000000000000000000000000000000000005d5d5d00ffffff00ffffff00ffffff00
ffffff00ffffff00d9d9d90000000000000000000000000000000000000000000000000000000000000000000000000000000000dddbdb00ffffff00ffffff00
ffffff00ffffff0076767600000000000000000000000000000000000000000000000000000000000000000000000000000000007c7c7c00ffffff00ffffff00
ffffff00ffffff00373738000000000000000000000000000303030026262700373738000203010000000000000000000000000037373800ffffff00ffffff00
ffffff00ffffff00373738000000000000000000000000001b1b1c00bdbebd00b6b6b6001b1b1c0000000000000000000101010037373800ffffff00ffffff00
ffffff00ffffff00dbd9db00111112000000000001010100b3b3b300bbbbbb00bbbbbb00aeaeae00010101000000000011111200dfdfdd00ffffff00ffffff00
ffffff00ffffff00ffffff00efefef00a9a9a900cfcfcf00ffffff006767670067676700ffffff00cfcfcf00a9a9a900f1f1f100ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00f6f6f6001b1b1c001b1b1c00f6f6f600ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fffffd00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
i$4d               16  35 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff005d5dfe000000ff005d5dfe00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ddddff000000ff000101ff005d5dfe00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff005d5dfe000000ff000404ff005d5dfe00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ddddff000101ff000101ff000404ff005d5dfe00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff005d5dfe000101ff000000ff000000ff005d5dfe00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00efefff001212ff000000ff002727ff000000ff005d5dfe00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff008282fe000101ff005d5dfe003e3eff000404ff005d5dfe00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ededff001212ff000000ff00e3e3ff003e3eff000404ff005d5dfe00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff008282fe000101ff005d5dfe00fbfbff003e3eff000404ff005d5dfe00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff001212ff000000ff00b9b9fc00ffffff003e3eff000404ff005d5dfe00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff009797fe000101ff003e3eff00ffffff00ffffff003e3eff000404ff005d5dfe00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff005d5dfe000000ff000101ff000000ff000000ff000101ff000000ff000404ff000101ff00ddddff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff005d5dfe000404ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff00ddddff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff005d5dfe000404ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff00ddddff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff003e3eff000101ff005d5dfe00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff003e3eff000404ff005d5dfe00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff003e3eff000404ff005d5dfe00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00a8a8ff00fbfbff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c0c0fd000000ff009797fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00eaeafd001212ff000000ff000101ff00d7d7ff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00fbfbff003e3eff000000ff000000ff000000ff002727ff00f3f3ff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00fbfbff008282fe000000ff000000ff000000ff000000ff000000ff005d5dfe00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00a8a8ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff008282fe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00d7d7ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff00b3b3ff00ffffff00ffffff00ffffff00
ffffff00ffffff00f5f5ff003e3eff000000ff000000ff000000ff000000ff000000ff000000ff000000ff002727ff00e6e6fe00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00efefff002727ff000000ff000000ff000000ff000000ff000000ff000404ff00e6e6fe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ddddff000404ff000000ff000000ff000000ff000000ff00c7c7fd00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00b9b9fc000000ff000000ff000000ff008282fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff008282fe000000ff003e3eff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00fbfbff00efefff003e3eff00e3e3ff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fdfdff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
i$3c               16  35 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00a9a9a900262627000000000011111200a9a9a900ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00bbbbbb000000000000000000000000000000000000000000a9a9a900ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff0047474700000000000000000000000000000000000000000026262700ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00efeded00000000000000000052525500ffffff00767676000000000000000000dddddd00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00dddddd007676760000000000a9a9a900ffffff00a9a9a9000000000000000000dddddd00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00525255000000000011111200ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00000000000000000000000000a9a9a900ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dddddd00000000000000000037373800ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dddddd0000000000000000000000000037373800ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00efeded00ffffff00a9a9a900000000000000000097979700ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00373738000000000052525500ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00bbbbbb005252550000000000ffffff00ffffff00ffffff00474747000000000047474700ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00979797000000000000000000cfcfcf00ffffff00fdfdfd00373738000000000052525500ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00cfcfcf00000000000000000047474700ffffff009797970000000000000000008b8b8b00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff0047474700000000000000000000000000000000000000000000000000dddddd00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00cfcfcf0000000000000000000000000000000000000000008b8b8b00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00bbbbbb0026262700000000001111120097979700ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fdfdfd00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c8c8c80076767600bbbbbb00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00c0c0c000000000000000000000000000a9a9a900ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00f6f6f6001111120000000000000000000000000000000000f1eff100ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00f3f3f3000000000000000000000000000000000000000000e7e5e700ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff005252550000000000000000000000000037373800fbfdfd00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00f3f3f3006e6e6e0076767600a9a9a900373738000000000026262700a9a9a9007676760067676700ededed00ffffff00ffffff00ffffff00
ffffff00fafafa001b1b1c0000000000000000000707070026262700000000001b1b1c000c0c0c0000000000000000000c0c0c00f3f3f300ffffff00ffffff00
ffffff00a9a9a900000000000000000000000000000000000101010002030100010101000000000000000000000000000000000097979700ffffff00ffffff00
ffffff0084848400000000000000000000000000000000005d5d5d00a0a0a000767676000000000000000000000000000000000084848400ffffff00ffffff00
ffffff00ededed0000000000000000000000000000000000c8c8c8007c7c7c00cfcfcf0000000000000000000000000000000000dfdfdf00ffffff00ffffff00
ffffff00ffffff00c8c8c800262627001b1b1c00a0a0a000fafafa000c0c0c00f3f3f300b1b1b1001b1b1c001b1b1c00bdbebd00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00f9f7f700f6f6f600ffffff00e5e3e50000000000cfcfcf00ffffff00f6f6f600f6f6f600ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dddbdb0097979700cfcfcf00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
i$unseated0        2   6  
847f7c00837e7b00
85807d00847f7c00
86817e0085807d00
8883800087827f00
8984810088838000
8b8683008a858200
i$unseated5        2   6  
817d7c00817d7c00
7f7b7a007f7b7a00
7f7c78007f7c7800
7d7978007d797800
7c7975007c797500
7b7776007b777600
i$call             0   2  


i$Ks               16  35 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff008b8b8b000000000000000000ffffff00ffffff00ffffff00ffffff008b8b8b00000000000000000026262700ededed00ffffff00ffffff00
ffffff00ffffff008b8b8b000000000000000000ffffff00ffffff00ffffff00dddddd00000000000000000000000000c8c8c800ffffff00ffffff00ffffff00
ffffff00ffffff008b8b8b000000000000000000ffffff00ffffff00ffffff003737380000000000000000008b8b8b00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00848484000000000000000000ffffff00ffffff008b8b8b00000000000000000037373800ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff008b8b8b000000000000000000ffffff00cfcfcf00000000000000000011111200dddbdb00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff008b8b8b000000000000000000ffffff003737380000000000000000008b8b8b00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff008b8b8b0000000000000000008b8b8b00000000000000000037373800ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff0084848400000000000000000000000000000000000000000000000000dddddd00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff008b8b8b0000000000000000000000000000000000000000000000000067676700ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff008b8b8b0000000000000000000000000076767600262627000000000011111200efeded00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff008b8b8b00000000000000000026262700ffffff008484840000000000000000008b8b8b00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00848484000000000000000000c8c8c800ffffff00efeded00111112000000000011111200efeded00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff008b8b8b000000000000000000ffffff00ffffff00ffffff006767670000000000000000008b8b8b00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff008b8b8b000000000000000000ffffff00ffffff00ffffff00cfcfcf00000000000000000026262700ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff008b8b8b000000000000000000ffffff00ffffff00ffffff00ffffff0047474700000000000000000097979700ffffff00ffffff00ffffff00
ffffff00ffffff00848484000000000000000000ffffff00ffffff00ffffff00ffffff0097979700000000000000000026262700ffffff00ffffff00ffffff00
ffffff00ffffff008b8b8b000000000000000000ffffff00ffffff00ffffff00ffffff00ffffff00111112000000000000000000bbbbbb00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00cfcfcf000c0c0c0011111200cfcfcf00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00b6b6b60000000000000000000000000000000000b6b6b600ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00c8c8c800000000000000000000000000000000000000000000000000c8c8c800ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00efeded000303030000000000000000000000000000000000000000000000000007070700f1eff100ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff005252550000000000000000000000000000000000000000000000000000000000000000005d5d5d00ffffff00ffffff00ffffff00
ffffff00ffffff00d9d9d90000000000000000000000000000000000000000000000000000000000000000000000000000000000dddbdb00ffffff00ffffff00
ffffff00ffffff0076767600000000000000000000000000000000000000000000000000000000000000000000000000000000007c7c7c00ffffff00ffffff00
ffffff00ffffff00373738000000000000000000000000000303030026262700373738000203010000000000000000000000000037373800ffffff00ffffff00
ffffff00ffffff00373738000000000000000000000000001b1b1c00bdbebd00b6b6b6001b1b1c0000000000000000000101010037373800ffffff00ffffff00
ffffff00ffffff00dbd9db00111112000000000001010100b3b3b300bbbbbb00bbbbbb00aeaeae00010101000000000011111200dfdfdd00ffffff00ffffff00
ffffff00ffffff00ffffff00efefef00a9a9a900cfcfcf00ffffff006767670067676700ffffff00cfcfcf00a9a9a900f1f1f100ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00f6f6f6001b1b1c001b1b1c00f6f6f600ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fffffd00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
i$8c               16  35 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00a9a9a9002626270000000000000000008b8b8b00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00cfcfcf00000000000000000000000000000000000000000076767600ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff0047474700000000000000000000000000000000000000000000000000efeded00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00000000000000000047474700ffffff008b8b8b000000000000000000a9a9a900ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00dddddd000000000000000000a9a9a900ffffff00ffffff00000000000000000097979700ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff000000000000000000a9a9a900ffffff00ffffff000000000000000000a9a9a900ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00474747000000000052525500ffffff008b8b8b000000000000000000efeded00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00cfcfcf00000000000000000000000000000000000000000076767600ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff006767670000000000000000000000000026262700efeded00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff008b8b8b00000000000000000000000000000000000000000037373800ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00efeded00111112000000000076767600ffffff00bbbbbb00000000000000000097979700ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00bbbbbb000000000000000000efeded00ffffff00ffffff00474747000000000067676700ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00979797000000000000000000dddddd00ffffff00ffffff00474747000000000067676700ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00cfcfcf00000000000000000067676700ffffff00bbbbbb00000000000000000076767600ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff0011111200000000000000000000000000000000000000000000000000bbbbbb00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00a9a9a900000000000000000000000000000000000000000052525500ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00a9a9a90026262700000000000000000076767600ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c8c8c80076767600bbbbbb00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00c0c0c000000000000000000000000000a9a9a900ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00f6f6f6001111120000000000000000000000000000000000f1eff100ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00f3f3f3000000000000000000000000000000000000000000e7e5e700ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff005252550000000000000000000000000037373800fbfdfd00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00f3f3f3006e6e6e0076767600a9a9a900373738000000000026262700a9a9a9007676760067676700ededed00ffffff00ffffff00ffffff00
ffffff00fafafa001b1b1c0000000000000000000707070026262700000000001b1b1c000c0c0c0000000000000000000c0c0c00f3f3f300ffffff00ffffff00
ffffff00a9a9a900000000000000000000000000000000000101010002030100010101000000000000000000000000000000000097979700ffffff00ffffff00
ffffff0084848400000000000000000000000000000000005d5d5d00a0a0a000767676000000000000000000000000000000000084848400ffffff00ffffff00
ffffff00ededed0000000000000000000000000000000000c8c8c8007c7c7c00cfcfcf0000000000000000000000000000000000dfdfdf00ffffff00ffffff00
ffffff00ffffff00c8c8c800262627001b1b1c00a0a0a000fafafa000c0c0c00f3f3f300b1b1b1001b1b1c001b1b1c00bdbebd00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00f9f7f700f6f6f600ffffff00e5e3e50000000000cfcfcf00ffffff00f6f6f600f6f6f600ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dddbdb0097979700cfcfcf00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
i$unseated1        2   6  
4a4645004a464500
4a4645004a464500
4a4645004a464500
4a4645004a464500
4a4645004a464500
4a4645004a464500
i$8s               16  35 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00a9a9a9002626270000000000000000008b8b8b00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00cfcfcf00000000000000000000000000000000000000000076767600ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff0047474700000000000000000000000000000000000000000000000000efeded00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00000000000000000047474700ffffff008b8b8b000000000000000000a9a9a900ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00dddddd000000000000000000a9a9a900ffffff00ffffff00000000000000000097979700ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff000000000000000000a9a9a900ffffff00ffffff000000000000000000a9a9a900ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00474747000000000052525500ffffff008b8b8b000000000000000000efeded00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00cfcfcf00000000000000000000000000000000000000000076767600ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff006767670000000000000000000000000026262700ededed00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff008b8b8b00000000000000000000000000000000000000000037373800ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00efeded00111112000000000076767600ffffff00bbbbbb00000000000000000097979700ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00bbbbbb000000000000000000efeded00ffffff00ffffff00474747000000000067676700ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00979797000000000000000000dddddd00ffffff00ffffff00474747000000000067676700ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00cfcfcf00000000000000000067676700ffffff00bbbbbb00000000000000000076767600ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff0011111200000000000000000000000000000000000000000000000000bbbbbb00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00a9a9a900000000000000000000000000000000000000000052525500ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00a9a9a90026262700000000000000000076767600ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00cfcfcf000c0c0c0011111200cfcfcf00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00b6b6b60000000000000000000000000000000000b6b6b600ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00c8c8c800000000000000000000000000000000000000000000000000c8c8c800ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00efeded000303030000000000000000000000000000000000000000000000000007070700f1eff100ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff005252550000000000000000000000000000000000000000000000000000000000000000005d5d5d00ffffff00ffffff00ffffff00
ffffff00ffffff00d9d9d90000000000000000000000000000000000000000000000000000000000000000000000000000000000dddbdb00ffffff00ffffff00
ffffff00ffffff0076767600000000000000000000000000000000000000000000000000000000000000000000000000000000007c7c7c00ffffff00ffffff00
ffffff00ffffff00373738000000000000000000000000000303030026262700373738000203010000000000000000000000000037373800ffffff00ffffff00
ffffff00ffffff00373738000000000000000000000000001b1b1c00bdbebd00b6b6b6001b1b1c0000000000000000000101010037373800ffffff00ffffff00
ffffff00ffffff00dbd9db00111112000000000001010100b3b3b300bbbbbb00bbbbbb00aeaeae00010101000000000011111200dfdfdd00ffffff00ffffff00
ffffff00ffffff00ffffff00efefef00a9a9a900cfcfcf00ffffff006767670067676700ffffff00cfcfcf00a9a9a900f1f1f100ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00f6f6f6001b1b1c001b1b1c00f6f6f600ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fffffd00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
i$9d               16  35 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00a8a8ff001212ff000000ff003e3eff00b9b9fc00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00a8a8ff000000ff000000ff000000ff000000ff000000ff00ceceff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ededff001212ff000000ff000000ff000000ff000000ff000000ff003e3eff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00a8a8ff000000ff000000ff008282fe00ffffff003e3eff000000ff000101ff00ddddff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff008282fe000101ff000000ff00ffffff00ffffff00ceceff000000ff000000ff00a8a8ff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff008282fe000101ff002727ff00ffffff00ffffff00ffffff000000ff000000ff008282fe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff008282fe000000ff000000ff00ffffff00ffffff00ededff000000ff000404ff005d5dfe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00adadfb000101ff000101ff008282fe00fbfbff008282fe000000ff000404ff005d5dfe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ededff001212ff000000ff000101ff000000ff000000ff000000ff000000ff005d5dfe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff008282fe000101ff000000ff000000ff000000ff001212ff000101ff005d5dfe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff008282fe000000ff000000ff00a8a8ff002727ff000404ff005d5dfe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff000000ff000000ff008282fe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ddddff003e3eff000101ff00ceceff00ffffff00ceceff000000ff000000ff00adadfb00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ededff000101ff000000ff005d5dfe00ffffff005d5dfe000000ff000000ff00ededff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00fbfbff003e3eff000101ff000101ff000000ff000000ff000000ff005d5dfe00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff009797fe000000ff000000ff000000ff000000ff001212ff00dbdbfe00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff009797fe000000ff000000ff003e3eff00ceceff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00a8a8ff00fbfbff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c0c0fd000000ff009797fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00eaeafd001212ff000000ff000101ff00d7d7ff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00fbfbff003e3eff000000ff000000ff000000ff002727ff00f3f3ff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00fbfbff008282fe000000ff000000ff000000ff000000ff000000ff005d5dfe00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00a8a8ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff008282fe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00d7d7ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff00b3b3ff00ffffff00ffffff00ffffff00
ffffff00ffffff00f5f5ff003e3eff000000ff000000ff000000ff000000ff000000ff000000ff000000ff002727ff00e6e6fe00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00efefff002727ff000000ff000000ff000000ff000000ff000000ff000404ff00e6e6fe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ddddff000404ff000000ff000000ff000000ff000000ff00c7c7fd00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00b9b9fc000000ff000000ff000000ff008282fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff008282fe000000ff003e3eff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00fbfbff00efefff003e3eff00e3e3ff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fdfdff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
i$6s               16  35 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff0076767600000000000000000052525500efeded00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00525255000000000000000000000000000000000052525500ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00a9a9a900000000000000000000000000000000000000000000000000cfcfcf00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00525255000000000011111200dddddd00cfcfcf0000000000000000008b8b8b00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00111112000000000076767600ffffff00ffffff003737380011111200a9a9a900ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00dddddd00000000000000000097979700ffffff00ffffff00fdfdfd00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00bbbbbb00000000000000000097979700373738000000000047474700dddddd00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00bbbbbb0000000000000000001111120000000000000000000000000037373800ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00bbbbbb000000000000000000000000000000000000000000000000000000000097979700ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00bbbbbb00000000000000000026262700ededed00cfcfcf00111112000000000052525500ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00bbbbbb00000000000000000084848400ffffff00ffffff00676767000000000026262700ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00dddddd00000000000000000097979700ffffff00ffffff008b8b8b000000000026262700ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00000000000000000076767600ffffff00ffffff00676767000000000026262700fbfdfd00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00474747000000000011111200cfcfcf00dddbdb00111112000000000052525500ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff009797970000000000000000000000000000000000000000000000000097979700ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00373738000000000000000000000000000000000037373800ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ededed0067676700000000000000000067676700ededed00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00cfcfcf000c0c0c0011111200cfcfcf00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00b6b6b60000000000000000000000000000000000b6b6b600ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00c8c8c800000000000000000000000000000000000000000000000000c8c8c800ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00efeded000303030000000000000000000000000000000000000000000000000007070700f1eff100ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff005252550000000000000000000000000000000000000000000000000000000000000000005d5d5d00ffffff00ffffff00ffffff00
ffffff00ffffff00d9d9d90000000000000000000000000000000000000000000000000000000000000000000000000000000000dddbdb00ffffff00ffffff00
ffffff00ffffff0076767600000000000000000000000000000000000000000000000000000000000000000000000000000000007c7c7c00ffffff00ffffff00
ffffff00ffffff00373738000000000000000000000000000303030026262700373738000203010000000000000000000000000037373800ffffff00ffffff00
ffffff00ffffff00373738000000000000000000000000001b1b1c00bdbebd00b6b6b6001b1b1c0000000000000000000101010037373800ffffff00ffffff00
ffffff00ffffff00dbd9db00111112000000000001010100b3b3b300bbbbbb00bbbbbb00aeaeae00010101000000000011111200dfdfdd00ffffff00ffffff00
ffffff00ffffff00ffffff00efefef00a9a9a900cfcfcf00ffffff006767670067676700ffffff00cfcfcf00a9a9a900f1f1f100ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00f6f6f6001b1b1c001b1b1c00f6f6f600ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fffffd00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
i$Qd               16  35 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ededff005d5dfe000101ff000000ff000000ff005d5dfe00efefff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ededff002727ff000000ff000000ff000000ff000101ff000101ff002727ff00ddddff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff003e3eff000000ff000000ff000000ff000000ff000000ff000000ff000000ff003e3eff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00bcbcff000101ff000000ff003e3eff00ceceff00ffffff00c7c7fd003e3eff000000ff000000ff00a8a8ff00ffffff00ffffff00ffffff00
ffffff00ffffff005d5dfe000101ff001212ff00dbdbfe00fbfbff00ffffff00ffffff00dbdbfe001212ff000000ff005d5dfe00ffffff00ffffff00ffffff00
ffffff00ffffff003e3eff000000ff005d5dfe00ffffff00ffffff00ffffff00ffffff00ffffff005d5dfe000404ff002727ff00ffffff00ffffff00ffffff00
ffffff00fbfbff001212ff000404ff008282fe00ffffff00ffffff00ffffff00ffffff00ffffff008282fe000000ff001212ff00ffffff00ffffff00ffffff00
ffffff00ededff000000ff000000ff00a8a8ff00ffffff00ffffff00ffffff00ffffff00ffffff00adadfb000000ff000000ff00ededff00ffffff00ffffff00
ffffff00ededff000000ff000000ff00a8a8ff00ffffff00ffffff00ffffff00ffffff00ffffff00a8a8ff000000ff000000ff00ededff00ffffff00ffffff00
ffffff00ededff000000ff000000ff00a8a8ff00ffffff00ffffff00ffffff00ffffff00ffffff00a8a8ff000000ff001212ff00ffffff00ffffff00ffffff00
ffffff00ffffff001212ff000101ff008282fe00ffffff00ffffff00ffffff00ffffff00ffffff008282fe000000ff002727ff00ffffff00ffffff00ffffff00
ffffff00fbfbff003e3eff000000ff005d5dfe00ffffff00ffffff005d5dfe008282fe00ffffff003e3eff000404ff005d5dfe00ffffff00ffffff00ffffff00
ffffff00ffffff005d5dfe000000ff001212ff00dbdbfe00ffffff002727ff000000ff003e3eff000404ff000000ff00a8a8ff00ffffff00ffffff00ffffff00
ffffff00ffffff00bcbcff000000ff000000ff003e3eff00dbdbfe00a8a8ff003e3eff000000ff000000ff003e3eff00fdfdff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff003e3eff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff005d5dfe00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ededff002727ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff009797fe00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ededff008282fe000000ff000000ff000000ff005d5dfe003e3eff000101ff000000ff00ddddff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00fdfdff00fbfbff00ffffff00ffffff00ffffff00ffffff00ffffff008282fe003e3eff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00a8a8ff00fbfbff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c0c0fd000000ff009797fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00eaeafd001212ff000000ff000101ff00d7d7ff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00fbfbff003e3eff000000ff000000ff000000ff002727ff00f3f3ff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00fbfbff008282fe000000ff000000ff000000ff000000ff000000ff005d5dfe00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00a8a8ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff008282fe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00d7d7ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff00b3b3ff00ffffff00ffffff00ffffff00
ffffff00ffffff00f5f5ff003e3eff000000ff000000ff000000ff000000ff000000ff000000ff000000ff002727ff00e6e6fe00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00efefff002727ff000000ff000000ff000000ff000000ff000000ff000404ff00e6e6fe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ddddff000404ff000000ff000000ff000000ff000000ff00c7c7fd00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00b9b9fc000000ff000000ff000000ff008282fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff008282fe000000ff003e3eff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00fbfbff00efefff003e3eff00e3e3ff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fffffd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fdfdff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fdfdff00
i$4h               16  35 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fdfdff005d5dfe000101ff005d5dfe00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ddddff000000ff000101ff005d5dfe00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff005d5dfe000101ff000000ff005d5dfe00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ddddff000101ff000000ff000101ff005d5dfe00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff005d5dfe000000ff000000ff000404ff005d5dfe00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ededff001212ff000000ff002727ff000404ff005d5dfe00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff008282fe000101ff005d5dfe003e3eff000000ff005d5dfe00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ededff001212ff000101ff00dfdffc003e3eff000404ff005d5dfe00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff008282fe000000ff005d5dfe00ffffff003e3eff000404ff005d5dfe00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff001212ff000000ff00b9b9fc00ffffff003e3eff000404ff005d5dfe00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff009797fe000000ff003e3eff00ffffff00ffffff003e3eff000404ff005d5dfe00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff005d5dfe000404ff000101ff000000ff000101ff000101ff000101ff000404ff000000ff00ddddff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff005d5dfe000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff00ddddff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff005d5dfe000101ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff00ddddff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff003e3eff000101ff005d5dfe00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff003e3eff000404ff005d5dfe00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff003e3eff000404ff005d5dfe00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00f5f5ff009797fe009797fe00e6e6fe00ffffff00ffffff00d7d7ff009797fe00a8a8ff00fdfdff00ffffff00ffffff00ffffff00
ffffff00ffffff00f7f7ff005d5dfe000000ff000000ff002727ff00e3e3ff00c7c7fd001212ff000000ff000000ff008282fe00ffffff00ffffff00ffffff00
ffffff00ffffff00b0b0fe000000ff000000ff000000ff000000ff005d5dfe003e3eff000000ff000000ff000000ff000101ff00ceceff00ffffff00ffffff00
ffffff00ffffff008282fe000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff009797fe00ffffff00ffffff00
ffffff00fbfbff008282fe000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff00b0b0fe00ffffff00ffffff00
ffffff00ffffff00ceceff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff001212ff00eaeafd00ffffff00ffffff00
ffffff00ffffff00fdfdff005d5dfe000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff008282fe00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ededff002727ff000101ff000000ff000000ff000000ff000000ff000000ff005d5dfe00f3f3ff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ddddff002727ff000000ff000000ff000000ff000000ff003e3eff00eaeafd00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd000a0aff000000ff000000ff002727ff00ddddff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00b0b0fe000404ff000a0aff00ceceff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff005d5dfe008282fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e3e3ff00ededff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
i$6d               16  35 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff008282fe000000ff000000ff005d5dfe00ededff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff005d5dfe000000ff000000ff000000ff000000ff005d5dfe00fdfdff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00a8a8ff000000ff000000ff000000ff000000ff000000ff000000ff00ceceff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00fdfdff005d5dfe000000ff001212ff00ddddff00ceceff000000ff000101ff008282fe00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff001212ff000000ff008282fe00fbfbff00ffffff003e3eff000a0aff00adadfb00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ddddff000000ff000000ff009797fe00ffffff00fdfdff00fdfdff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00bcbcff000000ff000000ff009797fe003e3eff000101ff003e3eff00ddddff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00bcbcff000000ff000000ff001212ff000000ff000000ff000000ff003e3eff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00bcbcff000000ff000000ff000000ff000000ff000000ff000000ff000000ff009797fe00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00bcbcff000000ff000000ff002727ff00ededff00ceceff001212ff000000ff005d5dfe00fdfdff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00bcbcff000000ff000000ff008282fe00ffffff00ffffff005d5dfe000000ff002727ff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ddddff000000ff000000ff009797fe00ffffff00ffffff008282fe000000ff002727ff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff000000ff000000ff008282fe00fbfbff00ffffff005d5dfe000404ff002727ff00fbfbff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff003e3eff000000ff000a0aff00ceceff00ddddff001212ff000000ff005d5dfe00fbfbff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff009797fe000000ff000000ff000000ff000000ff000000ff000000ff009797fe00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00fdfdff003e3eff000000ff000000ff000000ff000000ff003e3eff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00eaeafd005d5dfe000101ff000000ff005d5dfe00ededff00fdfdff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00a8a8ff00fbfbff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c0c0fd000000ff009797fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00eaeafd001212ff000000ff000101ff00d7d7ff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00fbfbff003e3eff000000ff000000ff000000ff002727ff00f3f3ff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00fbfbff008282fe000000ff000000ff000000ff000000ff000000ff005d5dfe00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00a8a8ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff008282fe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00d7d7ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff00b3b3ff00ffffff00ffffff00ffffff00
ffffff00ffffff00f5f5ff003e3eff000000ff000000ff000000ff000000ff000000ff000000ff000000ff002727ff00e6e6fe00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00efefff002727ff000000ff000000ff000000ff000000ff000000ff000404ff00e6e6fe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ddddff000404ff000000ff000000ff000000ff000000ff00c7c7fd00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00b9b9fc000000ff000000ff000000ff008282fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff008282fe000000ff003e3eff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00fbfbff00efefff003e3eff00e3e3ff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fdfdff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
i$unseated8        2   6  
8984810089848100
8b8683008b868300
8c8784008c878400
8d8885008d888500
8e8986008e898600
918c8900918c8900
i$7c               16  35 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00bbbbbb000000000000000000000000000000000000000000000000000000000047474700ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00bbbbbb000000000000000000000000000000000000000000000000000000000047474700ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00bbbbbb000000000000000000000000000000000000000000000000000000000047474700ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00bbbbbb00000000000000000097979700ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00474747000000000047474700ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00bbbbbb000000000000000000bbbbbb00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00676767000000000037373800ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00efeded00111112000000000084848400ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00a9a9a9000000000000000000efeded00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00767676000000000047474700ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff0037373800000000008b8b8b00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00efeded000000000000000000bbbbbb00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00a9a9a9000000000000000000efeded00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff008b8b8b000000000026262700ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00767676000000000047474700fdfdfd00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00676767000000000067676700ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00474747000000000067676700ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c8c8c80076767600bbbbbb00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00c0c0c000000000000000000000000000a9a9a900ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00f6f6f6001111120000000000000000000000000000000000f1eff100ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00f3f3f3000000000000000000000000000000000000000000e7e5e700ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff005252550000000000000000000000000037373800fbfdfd00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00f3f3f3006e6e6e0076767600a9a9a900373738000000000026262700a9a9a9007676760067676700ededed00ffffff00ffffff00ffffff00
ffffff00fafafa001b1b1c0000000000000000000707070026262700000000001b1b1c000c0c0c0000000000000000000c0c0c00f3f3f300ffffff00ffffff00
ffffff00a9a9a900000000000000000000000000000000000101010002030100010101000000000000000000000000000000000097979700ffffff00ffffff00
ffffff0084848400000000000000000000000000000000005d5d5d00a0a0a000767676000000000000000000000000000000000084848400ffffff00ffffff00
ffffff00ededed0000000000000000000000000000000000c8c8c8007c7c7c00cfcfcf0000000000000000000000000000000000dfdfdf00ffffff00ffffff00
ffffff00ffffff00c8c8c800262627001b1b1c00a0a0a000fafafa000c0c0c00f3f3f300b1b1b1001b1b1c001b1b1c00bdbebd00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00f9f7f700f6f6f600ffffff00e5e3e50000000000cfcfcf00ffffff00f6f6f600f6f6f600ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dddbdb0097979700cfcfcf00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
i$6c               16  35 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff0076767600000000000000000052525500efeded00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00525255000000000000000000000000000101010052525500ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00a9a9a900000000000000000000000000000000000000000000000000cfcfcf00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00525255000000000011111200dddddd00cfcfcf0000000000000000008b8b8b00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00111112000000000076767600ffffff00fdfdfd003737380011111200a9a9a900ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00dddddd00000000000000000097979700ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00bbbbbb00000000000000000097979700373738000000000047474700dddbdb00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00bbbbbb0000000000000000001111120000000000000000000000000037373800ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00bbbbbb000000000000000000000000000000000000000000000000000000000097979700ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00bbbbbb00000000000000000026262700efeded00cfcfcf00111112000000000052525500ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00bbbbbb00000000000000000084848400ffffff00ffffff00676767000000000026262700ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00dddddd00000000000000000097979700ffffff00ffffff008b8b8b00000000001b1b1c00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00000000000000000076767600ffffff00ffffff00676767000000000026262700ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00474747000000000011111200cfcfcf00dddddd00111112000000000052525500ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff009797970000000000000000000000000000000000000000000000000097979700ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00373738000000000000000000000000000000000037373800ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ededed0067676700000000000000000067676700ededed00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c8c8c80076767600bbbbbb00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00c0c0c000000000000000000000000000a9a9a900ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00f6f6f6001111120000000000000000000000000000000000f1eff100ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00f3f3f3000000000000000000000000000000000000000000e7e5e700ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff005252550000000000000000000000000037373800fbfdfd00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00f3f3f3006e6e6e0076767600a9a9a900373738000000000026262700a9a9a9007676760067676700ededed00ffffff00ffffff00ffffff00
ffffff00fafafa001b1b1c0000000000000000000707070026262700000000001b1b1c000c0c0c0000000000000000000c0c0c00f3f3f300ffffff00ffffff00
ffffff00a9a9a900000000000000000000000000000000000101010002030100010101000000000000000000000000000000000097979700ffffff00ffffff00
ffffff0084848400000000000000000000000000000000005d5d5d00a0a0a000767676000000000000000000000000000000000084848400ffffff00ffffff00
ffffff00ededed0000000000000000000000000000000000c8c8c8007c7c7c00cfcfcf0000000000000000000000000000000000dfdfdf00ffffff00ffffff00
ffffff00ffffff00c8c8c800262627001b1b1c00a0a0a000fafafa000c0c0c00f3f3f300b1b1b1001b1b1c001b1b1c00bdbebd00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00f9f7f700f6f6f600ffffff00e5e3e50000000000cfcfcf00ffffff00f6f6f600f6f6f600ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dddbdb0097979700cfcfcf00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
i$out              5   7  
0707070071717100717171007171710007070700
7171710071717100030303000303030071717100
71717100717171000b0b0b000b0b0b000b0b0b00
0404040071717100717171007171710003030300
0f0f0f000f0f0f000f0f0f007171710071717100
7171710003030300030303007171710071717100
1414140071717100717171007171710014141400
i$Ad               16  35 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff003e3eff000000ff000000ff005d5dfe00fdfdff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ededff000000ff000000ff000000ff001212ff00ededff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00a8a8ff000000ff000000ff000000ff000000ff00a8a8ff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00fbfbff008282fe000000ff003e3eff001212ff000000ff008282fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff002727ff000000ff005d5dfe005d5dfe000000ff002727ff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00efefff000101ff000000ff008282fe008282fe000404ff000101ff00ededff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00a8a8ff000000ff000000ff00ceceff00ddddff000000ff000000ff00a8a8ff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00fbfbff008282fe000000ff001212ff00ffffff00fbfbff003e3eff000000ff008282fe00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff002727ff000000ff005d5dfe00fbfbff00ffffff008282fe000000ff002727ff00fbfbff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00efefff000101ff000000ff008282fe00ffffff00fbfbff00a8a8ff000000ff000000ff00ceceff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00a8a8ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff008282fe00ffffff00ffffff00ffffff00ffffff00
ffffff00fbfbff008282fe000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff005d5dfe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff002727ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff001212ff00fdfdff00ffffff00ffffff00ffffff00
ffffff00efefff000101ff000000ff008282fe00ffffff00ffffff00ffffff00ffffff008282fe000000ff000000ff00ceceff00ffffff00ffffff00ffffff00
ffffff00a8a8ff000000ff000000ff00bcbcff00ffffff00ffffff00ffffff00ffffff00ceceff000000ff000000ff008282fe00ffffff00ffffff00ffffff00
fbfbff008282fe000000ff000000ff00ededff00ffffff00ffffff00ffffff00ffffff00ffffff001212ff000101ff003e3eff00ffffff00ffffff00ffffff00
ffffff002727ff000000ff003e3eff00fbfbff00ffffff00ffffff00ffffff00ffffff00ffffff005d5dfe000000ff000101ff00ededff00ffffff00ffffff00
ffffff00ffffff00fdfdff00fbfbff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fdfdff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00a8a8ff00fbfbff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c0c0fd000000ff009797fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00eaeafd001212ff000000ff000101ff00d7d7ff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00fbfbff003e3eff000000ff000000ff000000ff002727ff00f3f3ff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00fbfbff008282fe000000ff000000ff000000ff000000ff000000ff005d5dfe00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00a8a8ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff008282fe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00d7d7ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff00b3b3ff00ffffff00ffffff00ffffff00
ffffff00ffffff00f5f5ff003e3eff000000ff000000ff000000ff000000ff000000ff000000ff000000ff002727ff00e6e6fe00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00efefff002727ff000000ff000000ff000000ff000000ff000000ff000404ff00e6e6fe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ddddff000404ff000000ff000000ff000000ff000000ff00c7c7fd00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00b9b9fc000000ff000000ff000000ff008282fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff008282fe000000ff003e3eff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00fbfbff00efefff003e3eff00e3e3ff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fdfdff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
i$3d               16  35 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00a8a8ff002727ff000000ff001212ff00a8a8ff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00bcbcff000000ff000000ff000000ff000000ff000000ff00a8a8ff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff003e3eff000000ff000000ff000000ff000000ff000000ff002727ff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ededff000101ff000000ff005d5dfe00fbfbff008282fe000000ff000000ff00ddddff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ddddff008282fe000000ff00a8a8ff00ffffff00a8a8ff000000ff000000ff00ddddff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00fdfdff00fbfbff00ffffff00ffffff00ffffff005d5dfe000000ff001212ff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff000000ff000000ff000000ff00a8a8ff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ddddff000000ff000000ff003e3eff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ddddff000000ff000000ff000000ff002727ff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ededff00ffffff00a8a8ff000101ff000404ff009797fe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fdfdff003e3eff000000ff005d5dfe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00b9b9fc005d5dfe000000ff00ffffff00ffffff00ffffff003e3eff000000ff003e3eff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff009797fe000000ff000000ff00ceceff00ffffff00ffffff003e3eff000000ff005d5dfe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ceceff000000ff000000ff003e3eff00ffffff009797fe000000ff000000ff008282fe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff003e3eff000000ff000101ff000000ff000000ff000000ff000000ff00ddddff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ceceff000000ff000000ff000000ff000000ff000101ff008282fe00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00bcbcff002727ff000000ff001212ff009797fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00a8a8ff00fbfbff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c0c0fd000000ff009797fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00eaeafd001212ff000000ff000101ff00d7d7ff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00fbfbff003e3eff000000ff000000ff000000ff002727ff00f3f3ff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00fbfbff008282fe000000ff000000ff000000ff000000ff000000ff005d5dfe00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00a8a8ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff008282fe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00d7d7ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff00b3b3ff00ffffff00ffffff00ffffff00
ffffff00ffffff00f5f5ff003e3eff000000ff000000ff000000ff000000ff000000ff000000ff000000ff002727ff00e6e6fe00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00efefff002727ff000000ff000000ff000000ff000000ff000000ff000404ff00e6e6fe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ddddff000404ff000000ff000000ff000000ff000000ff00c7c7fd00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00b9b9fc000000ff000000ff000000ff008282fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff008282fe000000ff003e3eff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00fbfbff00efefff003e3eff00e3e3ff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fdfdff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
i$Ac               16  35 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff0037373800000000000000000052525500ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00efeded0000000000000000000000000011111200efeded00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00a9a9a90000000000000000000000000000000000a9a9a900ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00767676000000000037373800111112000000000076767600ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00373738000000000067676700525255000000000037373800ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00efeded000000000000000000848484008b8b8b000000000000000000efeded00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00a9a9a9000000000000000000cfcfcf00dddbdb000000000000000000a9a9a900ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00767676000000000011111200ffffff00ffffff00373738000000000076767600ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00373738000000000052525500ffffff00ffffff00767676000000000026262700ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00efeded0000000000000000008b8b8b00ffffff00ffffff00a9a9a9000000000000000000cfcfcf00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00a9a9a90000000000000000000000000000000000000000000000000000000000000000008b8b8b00fffffd00ffffff00ffffff00ffffff00
ffffff00ffffff0076767600000000000000000000000000000000000000000000000000000000000000000052525500ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff0037373800000000000000000000000000000000000000000000000000000000000000000011111200ffffff00ffffff00ffffff00ffffff00
ffffff00efeded0000000000000000008b8b8b00ffffff00ffffff00ffffff00ffffff008b8b8b000000000000000000cfcfcf00ffffff00ffffff00ffffff00
ffffff00a9a9a9000000000000000000bbbbbb00ffffff00ffffff00ffffff00ffffff00cfcfcf0000000000000000008b8b8b00ffffff00ffffff00ffffff00
ffffff00767676000000000000000000efeded00ffffff00ffffff00ffffff00ffffff00ffffff00111112000000000047474700ffffff00ffffff00ffffff00
ffffff00373738000000000037373800ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00525255000000000000000000efeded00ffffff00ffffff00
ffffff00fdfdfd00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c8c8c80076767600bbbbbb00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00c0c0c000000000000000000000000000a9a9a900ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00f6f6f6001111120000000000000000000000000000000000f1eff100ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00f3f3f3000000000000000000000000000000000000000000e7e5e700ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff005252550000000000000000000000000037373800fbfdfd00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00f3f3f3006e6e6e0076767600a9a9a900373738000000000026262700a9a9a9007676760067676700ededed00ffffff00ffffff00ffffff00
ffffff00fafafa001b1b1c0000000000000000000707070026262700000000001b1b1c000c0c0c0000000000000000000c0c0c00f3f3f300ffffff00ffffff00
ffffff00a9a9a900000000000000000000000000000000000101010002030100010101000000000000000000000000000000000097979700ffffff00ffffff00
ffffff0084848400000000000000000000000000000000005d5d5d00a0a0a000767676000000000000000000000000000000000084848400ffffff00ffffff00
ffffff00ededed0000000000000000000000000000000000c8c8c8007c7c7c00cfcfcf0000000000000000000000000000000000dfdfdf00ffffff00ffffff00
ffffff00ffffff00c8c8c800262627001b1b1c00a0a0a000fafafa000c0c0c00f3f3f300b1b1b1001b1b1c001b1b1c00bdbebd00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00f9f7f700f6f6f600ffffff00e5e3e50000000000cfcfcf00ffffff00f6f6f600f6f6f600ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dddbdb0097979700cfcfcf00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
i$Js               16  35 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00979797000000000000000000ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00979797000000000000000000ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00979797000000000000000000ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00979797000000000000000000ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00979797000000000000000000ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00979797000000000000000000ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00979797000000000000000000ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00979797000000000000000000ffffff00ffffff00ffffff00ffffff00fdfdfd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00979797000000000000000000ffffff00ffffff00ffffff00ffffff00fdfdfd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00979797000000000000000000ffffff00ffffff00ffffff00ffffff00fbfdfd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00979797000000000000000000ffffff00ffffff00ffffff00ffffff00fafafa00
ffffff00ffffff00ffffff00474747000000000067676700ffffff00ffffff00979797000000000000000000ffffff00ffffff00ffffff00ffffff00fafafa00
ffffff00ffffff00ffffff00525255000000000037373800fdfdfd00ffffff008b8b8b000000000000000000ffffff00ffffff00ffffff00ffffff00fafafa00
ffffff00ffffff00ffffff00767676000000000000000000cfcfcf00ffffff00373738000000000037373800ffffff00ffffff00ffffff00ffffff00fafafa00
ffffff00ffffff00ffffff00bbbbbb0000000000000000000000000000000000000000000000000076767600ffffff00ffffff00ffffff00ffffff00f9f7f700
ffffff00ffffff00ffffff00ffffff00474747000000000000000000000000000000000011111200efeded00ffffff00ffffff00ffffff00ffffff00f9f7f700
ffffff00ffffff00ffffff00ffffff00efeded0052525500000000000000000047474700cfcfcf00ffffff00ffffff00ffffff00ffffff00ffffff00f6f6f600
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00f6f6f600
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00f6f6f600
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00cfcfcf000c0c0c0011111200cfcfcf00ffffff00ffffff00ffffff00ffffff00ffffff00f6f6f600
ffffff00ffffff00ffffff00ffffff00ffffff00b6b6b60000000000000000000000000000000000b6b6b600ffffff00ffffff00ffffff00ffffff00f3f3f300
ffffff00ffffff00ffffff00ffffff00c8c8c800000000000000000000000000000000000000000000000000c8c8c800ffffff00ffffff00ffffff00f3f3f300
ffffff00ffffff00ffffff00efeded000303030000000000000000000000000000000000000000000000000007070700f1eff100ffffff00ffffff00f1f1f100
ffffff00ffffff00ffffff005252550000000000000000000000000000000000000000000000000000000000000000005d5d5d00ffffff00ffffff00f1eff100
ffffff00ffffff00d9d9d90000000000000000000000000000000000000000000000000000000000000000000000000000000000dddbdb00ffffff00efefef00
ffffff00ffffff0076767600000000000000000000000000000000000000000000000000000000000000000000000000000000007c7c7c00ffffff00efeded00
ffffff00ffffff00373738000000000000000000000000000303030026262700373738000203010000000000000000000000000037373800ffffff00ededed00
ffffff00ffffff00373738000000000000000000000000001b1b1c00bdbebd00b6b6b6001b1b1c0000000000000000000101010037373800ffffff00ededed00
ffffff00ffffff00dbd9db00111112000000000001010100b3b3b300bbbbbb00bbbbbb00aeaeae00010101000000000011111200dfdfdd00ffffff00ebebeb00
ffffff00ffffff00ffffff00efefef00a9a9a900cfcfcf00ffffff006767670067676700ffffff00cfcfcf00a9a9a900f1f1f100ffffff00ffffff00ebe9eb00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00f6f6f6001b1b1c001b1b1c00f6f6f600ffffff00ffffff00ffffff00ffffff00ffffff00e9e9e900
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fffffd00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e7e7e700
i$2h               16  35 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ceceff003e3eff000000ff000101ff005d5dfe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ddddff001212ff000000ff000000ff000000ff000000ff005d5dfe00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff005d5dfe000101ff000000ff000000ff000000ff000000ff000000ff00bcbcff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff001212ff000000ff003e3eff00fbfbff00ceceff000000ff000000ff008282fe00fbfbff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ddddff000000ff000000ff00a8a8ff00ffffff00ffffff003e3eff000000ff005d5dfe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ddddff003e3eff000000ff00ddddff00ffffff00fdfdff005d5dfe000404ff005d5dfe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff002727ff000000ff008282fe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff009797fe000000ff000101ff00ddddff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ddddff001212ff000101ff005d5dfe00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fbfbff003e3eff000000ff002727ff00efefff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff008282fe000000ff001212ff00c7c7fd00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00b9b9fc000000ff000000ff00bcbcff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ededff002727ff000000ff009797fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff008282fe000000ff005d5dfe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff001212ff000000ff000101ff000101ff000000ff000000ff000000ff005d5dfe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ceceff000000ff000000ff000000ff000000ff000000ff000000ff000101ff005d5dfe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff008282fe000000ff000000ff000000ff000000ff000000ff000000ff000404ff005d5dfe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00f5f5ff009797fe009797fe00e6e6fe00ffffff00ffffff00d7d7ff009797fe00a8a8ff00fdfdff00ffffff00ffffff00ffffff00
ffffff00ffffff00f7f7ff005d5dfe000000ff000000ff002727ff00e3e3ff00c7c7fd001212ff000000ff000000ff008282fe00ffffff00ffffff00ffffff00
ffffff00ffffff00b0b0fe000000ff000000ff000000ff000000ff005d5dfe003e3eff000000ff000000ff000000ff000101ff00ceceff00ffffff00ffffff00
ffffff00ffffff008282fe000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff009797fe00ffffff00ffffff00
ffffff00fbfbff008282fe000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff00b0b0fe00ffffff00ffffff00
ffffff00ffffff00ceceff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff001212ff00eaeafd00ffffff00ffffff00
ffffff00ffffff00fdfdff005d5dfe000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff008282fe00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ededff002727ff000101ff000000ff000000ff000000ff000000ff000000ff005d5dfe00f3f3ff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ddddff002727ff000000ff000000ff000000ff000000ff003e3eff00eaeafd00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd000a0aff000000ff000000ff002727ff00ddddff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00b0b0fe000404ff000a0aff00ceceff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff005d5dfe008282fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e3e3ff00ededff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
i$5c               16  35 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff0052525500000000000000000000000000000000000000000097979700ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff0026262700000000000000000000000000000000000000000097979700ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff0000000000000000000000000000000000000000000000000097979700ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00cfcfcf000000000026262700ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00a9a9a9000000000047474700ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff008b8b8b000000000037373800111112000000000052525500efeded00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff0052525500000000000000000000000000000000000000000047474700ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff0037373800000000000000000000000000000000000000000000000000a9a9a900ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00fdfdfd0047474700000000008b8b8b00ffffff00cfcfcf00111112000000000052525500ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00fffffd00ffffff00ffffff00ffffff00ffffff00767676000000000026262700ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff008b8b8b000000000026262700fdfdfd00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00dddddd006767670000000000cfcfcf00ffffff00ffffff008b8b8b00000000001b1b1c00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00dddddd0000000000000000008b8b8b00ffffff00ffffff00676767000000000037373800ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00111112000000000026262700ededed00cfcfcf00000000000000000076767600ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff008b8b8b00000000000000000000000000000000000000000000000000dddddd00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00efeded00262627000000000000000000000000000000000076767600ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00dbdbdb004747470000000000000000008b8b8b00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c8c8c80076767600bbbbbb00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00c0c0c000000000000000000000000000a9a9a900ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00f6f6f6001111120000000000000000000000000000000000f1eff100ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00f3f3f3000000000000000000000000000000000000000000e7e5e700ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff005252550000000000000000000000000037373800fbfdfd00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00f3f3f3006e6e6e0076767600a9a9a900373738000000000026262700a9a9a9007676760067676700ededed00ffffff00ffffff00ffffff00
ffffff00fafafa001b1b1c0000000000000000000707070026262700000000001b1b1c000c0c0c0000000000000000000c0c0c00f3f3f300ffffff00ffffff00
ffffff00a9a9a900000000000000000000000000000000000101010002030100010101000000000000000000000000000000000097979700ffffff00ffffff00
ffffff0084848400000000000000000000000000000000005d5d5d00a0a0a000767676000000000000000000000000000000000084848400ffffff00ffffff00
ffffff00ededed0000000000000000000000000000000000c8c8c8007c7c7c00cfcfcf0000000000000000000000000000000000dfdfdf00ffffff00ffffff00
ffffff00ffffff00c8c8c800262627001b1b1c00a0a0a000fafafa000c0c0c00f3f3f300b1b1b1001b1b1c001b1b1c00bdbebd00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00f9f7f700f6f6f600ffffff00e5e3e50000000000cfcfcf00ffffff00f6f6f600f6f6f600ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dddbdb0097979700cfcfcf00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
i$7h               16  35 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00bcbcff000000ff000000ff000000ff000000ff000000ff000000ff000101ff003e3eff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00bcbcff000000ff000000ff000000ff000000ff000000ff000000ff000101ff003e3eff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00bcbcff000000ff000000ff000000ff000000ff000000ff000000ff000000ff003e3eff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00bcbcff000000ff000101ff009797fe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff003e3eff000101ff003e3eff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00bcbcff000101ff000000ff00bcbcff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff005d5dfe000101ff003e3eff00fbfbff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00efefff001212ff000000ff008282fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00a8a8ff000000ff000000ff00ededff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00fbfbff008282fe000101ff003e3eff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff002727ff000000ff008282fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00efefff000101ff000000ff00bcbcff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00a8a8ff000000ff000000ff00ededff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff008282fe000000ff002727ff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff008282fe000000ff003e3eff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00fdfdff005d5dfe000101ff005d5dfe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff003e3eff000404ff005d5dfe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00f5f5ff009797fe009797fe00e6e6fe00ffffff00ffffff00d7d7ff009797fe00a8a8ff00fdfdff00ffffff00ffffff00ffffff00
ffffff00ffffff00f7f7ff005d5dfe000000ff000000ff002727ff00e3e3ff00c7c7fd001212ff000000ff000000ff008282fe00ffffff00ffffff00ffffff00
ffffff00ffffff00b0b0fe000000ff000000ff000000ff000000ff005d5dfe003e3eff000000ff000000ff000000ff000101ff00ceceff00ffffff00ffffff00
ffffff00ffffff008282fe000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff009797fe00ffffff00ffffff00
ffffff00fbfbff008282fe000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff00b0b0fe00ffffff00ffffff00
ffffff00ffffff00ceceff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff001212ff00eaeafd00ffffff00ffffff00
ffffff00ffffff00fdfdff005d5dfe000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff008282fe00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ededff002727ff000101ff000000ff000000ff000000ff000000ff000000ff005d5dfe00f3f3ff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ddddff002727ff000000ff000000ff000000ff000000ff003e3eff00eaeafd00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd000a0aff000000ff000000ff002727ff00ddddff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00b0b0fe000404ff000a0aff00ceceff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff005d5dfe008282fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e3e3ff00ededff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
i$Tc               16  35 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00bbbbbb00000000008b8b8b00ffffff00ffffff00ffffff00474747000000000047474700efeded00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff0052525500000000008b8b8b00ffffff00ffffff008b8b8b0000000000000000000000000076767600ffffff00ffffff00ffffff00
ffffff00ffffff00bbbbbb0000000000000000008b8b8b00ffffff00ffffff002626270000000000000000000000000011111200ffffff00ffffff00ffffff00
ffffff00cfcfcf0011111200000000000000000084848400ffffff00dddddd000000000026262700ffffff003737380000000000bbbbbb00ffffff00ffffff00
fdfdfd00373738000000000011111200000000008b8b8b00ffffff00bbbbbb000000000067676700ffffff00848484000000000097979700ffffff00ffffff00
fdfdfd00373738002626270067676700000000008b8b8b00ffffff0097979700000000008b8b8b00ffffff0097979700000000008b8b8b00ffffff00ffffff00
ffffff0067676700dbd9db0076767600000000008b8b8b00ffffff008b8b8b00000000008b8b8b00ffffff00a9a9a9000000000076767600ffffff00ffffff00
ffffff00ffffff00ffffff00767676000000000084848400ffffff008b8b8b000000000084848400ffffff00a9a9a9000000000076767600ffffff00ffffff00
ffffff00ffffff00ffffff0076767600000000008b8b8b00ffffff008b8b8b00000000008b8b8b00ffffff00a9a9a9000000000076767600ffffff00ffffff00
ffffff00ffffff00ffffff0076767600000000008b8b8b00ffffff0084848400000000008b8b8b00ffffff00a9a9a9000000000076767600ffffff00ffffff00
ffffff00ffffff00ffffff0076767600000000008b8b8b00ffffff008b8b8b00000000008b8b8b00ffffff00a9a9a9000000000076767600ffffff00ffffff00
ffffff00ffffff00ffffff00767676000000000084848400ffffff008b8b8b000000000084848400ffffff0097979700000000008b8b8b00ffffff00ffffff00
ffffff00ffffff00ffffff0076767600000000008b8b8b00ffffff00a9a9a9000000000067676700ffffff008b8b8b000000000097979700ffffff00ffffff00
ffffff00ffffff00ffffff0076767600000000008b8b8b00ffffff00dddddd000000000026262700fdfdfd003737380000000000bbbbbb00ffffff00ffffff00
ffffff00ffffff00ffffff0076767600000000008b8b8b00ffffff00ffffff002626270000000000000000000000000011111200ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00767676000000000084848400ffffff00ffffff008484840000000000000000000000000076767600ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff0076767600000000008b8b8b00ffffff00ffffff00ffffff00525255000000000047474700efeded00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c8c8c80076767600bbbbbb00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00c0c0c000000000000000000000000000a9a9a900ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00f6f6f6001111120000000000000000000000000000000000f1eff100ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00f3f3f3000000000000000000000000000000000000000000e7e5e700ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff005252550000000000000000000000000037373800fbfdfd00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00f3f3f3006e6e6e0076767600a9a9a900373738000000000026262700a9a9a9007676760067676700ededed00ffffff00ffffff00ffffff00
ffffff00fafafa001b1b1c0000000000000000000707070026262700000000001b1b1c000c0c0c0000000000000000000c0c0c00f3f3f300ffffff00ffffff00
ffffff00a9a9a900000000000000000000000000000000000101010002030100010101000000000000000000000000000000000097979700ffffff00ffffff00
ffffff0084848400000000000000000000000000000000005d5d5d00a0a0a000767676000000000000000000000000000000000084848400ffffff00ffffff00
ffffff00ededed0000000000000000000000000000000000c8c8c8007c7c7c00cfcfcf0000000000000000000000000000000000dfdfdf00ffffff00ffffff00
ffffff00ffffff00c8c8c800262627001b1b1c00a0a0a000fafafa000c0c0c00f3f3f300b1b1b1001b1b1c001b1b1c00bdbebd00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00f9f7f700f6f6f600ffffff00e5e3e50000000000cfcfcf00ffffff00f6f6f600f6f6f600ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dddbdb0097979700cfcfcf00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
i$7d               16  35 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00bcbcff000000ff000000ff000000ff000000ff000000ff000000ff000000ff003e3eff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00bcbcff000000ff000000ff000000ff000000ff000000ff000000ff000000ff003e3eff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00bcbcff000000ff000000ff000000ff000000ff000000ff000000ff000404ff003e3eff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00bcbcff000000ff000000ff009797fe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff003e3eff000000ff003e3eff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00bcbcff000101ff000000ff00bcbcff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff005d5dfe000000ff003e3eff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ededff001212ff000101ff008282fe00fdfdff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00a8a8ff000000ff000000ff00ededff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff008282fe000000ff003e3eff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff002727ff000404ff008282fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ededff000101ff000101ff00bcbcff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00a8a8ff000000ff000000ff00ededff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff008282fe000101ff002727ff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00fdfdff008282fe000000ff003e3eff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff005d5dfe000000ff005d5dfe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff003e3eff000404ff005d5dfe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00a8a8ff00fbfbff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c0c0fd000000ff009797fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00eaeafd001212ff000000ff000101ff00d7d7ff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00fbfbff003e3eff000000ff000000ff000000ff002727ff00f3f3ff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00fbfbff008282fe000000ff000000ff000000ff000000ff000000ff005d5dfe00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00a8a8ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff008282fe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00d7d7ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff00b3b3ff00ffffff00ffffff00ffffff00
ffffff00ffffff00f5f5ff003e3eff000000ff000000ff000000ff000000ff000000ff000000ff000000ff002727ff00e6e6fe00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00efefff002727ff000000ff000000ff000000ff000000ff000000ff000404ff00e6e6fe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ddddff000404ff000000ff000000ff000000ff000000ff00c7c7fd00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00b9b9fc000000ff000000ff000000ff008282fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff008282fe000000ff003e3eff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00fbfbff00efefff003e3eff00e3e3ff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fdfdff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
i$unseated3        2   6  
7c7975007c797500
7b7776007b777600
7a76750079757400
7975740078747300
7874730077737200
7773720077737200
i$Qc               16  35 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00efeded006767670000000000000000000000000067676700efeded00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ededed0026262700000000000000000000000000000000000000000026262700dddddd00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00474747000000000000000000000000000000000000000000000000000000000037373800ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00bbbbbb00000000000000000037373800c8c8c800ffffff00cfcfcf00373738000000000000000000a9a9a900ffffff00ffffff00ffffff00
ffffff00ffffff00676767000000000011111200dddbdb00ffffff00ffffff00fffffd00dddbdb00111112000000000067676700ffffff00ffffff00ffffff00
ffffff00ffffff00373738000000000067676700ffffff00ffffff00ffffff00ffffff00ffffff00676767000000000026262700ffffff00ffffff00ffffff00
ffffff00ffffff0011111200000000008b8b8b00ffffff00ffffff00ffffff00ffffff00ffffff008b8b8b000000000011111200fffffd00ffffff00ffffff00
ffffff00efeded000000000000000000a9a9a900ffffff00ffffff00ffffff00ffffff00ffffff00a9a9a9000000000000000000efeded00ffffff00ffffff00
ffffff00efeded000000000000000000a9a9a900ffffff00ffffff00ffffff00ffffff00ffffff00a9a9a9000000000000000000efeded00ffffff00ffffff00
ffffff00efeded000000000000000000a9a9a900ffffff00ffffff00ffffff00ffffff00ffffff00a9a9a9000000000011111200ffffff00ffffff00ffffff00
ffffff00ffffff0011111200000000008b8b8b00ffffff00ffffff00ffffff00ffffff00ffffff008b8b8b000000000026262700fffffd00ffffff00ffffff00
ffffff00ffffff00373738000000000052525500ffffff00ffffff006767670076767600ffffff00474747000000000067676700ffffff00ffffff00ffffff00
ffffff00fffffd00676767000000000011111200dddddd00fdfdfd002626270000000000474747000000000000000000a9a9a900ffffff00ffffff00fbfdfd00
ffffff00ffffff00bbbbbb00000000000000000037373800dddddd00a9a9a90037373800000000000000000037373800ffffff00ffffff00ffffff00fffffd00
ffffff00ffffff00ffffff0047474700000000000000000000000000000000000000000000000000000000000000000052525500ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00efeded0026262700000000000000000000000000000000000000000000000000000000000000000097979700ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ededed007676760000000000000000000000000067676700474747000000000000000000dddddd00ffffff00fbfdfd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff008484840047474700ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c8c8c80076767600bbbbbb00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00c0c0c000000000000000000000000000a9a9a900ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00f6f6f6001111120000000000000000000000000000000000f1eff100ffffff00ffffff00ffffff00ffffff00fdfdfd00
ffffff00ffffff00ffffff00ffffff00f3f3f3000000000000000000000000000000000000000000e7e5e700ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff005252550000000000000000000000000037373800fbfdfd00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00f3f3f3006e6e6e0076767600a9a9a900373738000000000026262700a9a9a9007676760067676700ededed00ffffff00ffffff00ffffff00
ffffff00fafafa001b1b1c0000000000000000000707070026262700000000001b1b1c000c0c0c0000000000000000000c0c0c00f3f3f300ffffff00ffffff00
ffffff00a9a9a900000000000000000000000000000000000101010002030100010101000000000000000000000000000000000097979700ffffff00ffffff00
ffffff0084848400000000000000000000000000000000005d5d5d00a0a0a000767676000000000000000000000000000000000084848400ffffff00ffffff00
ffffff00ededed0000000000000000000000000000000000c8c8c8007c7c7c00cfcfcf0000000000000000000000000000000000dfdfdf00ffffff00ffffff00
ffffff00ffffff00c8c8c800262627001b1b1c00a0a0a000fafafa000c0c0c00f3f3f300b1b1b1001b1b1c001b1b1c00bdbebd00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00f9f7f700f6f6f600ffffff00e5e3e50000000000cfcfcf00ffffff00f6f6f600f6f6f600ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dddbdb0097979700cfcfcf00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
i$3s               16  35 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00a9a9a900262627000000000011111200a9a9a900ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00bbbbbb000000000000000000000000000000000000000000a9a9a900ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff0047474700000000000000000000000000000000000000000026262700ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00efeded00000000000000000052525500ffffff00767676000000000000000000dddddd00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00dddddd007676760000000000a9a9a900ffffff00a9a9a9000000000000000000dddddd00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00525255000000000011111200ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00000000000000000000000000a9a9a900ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dddddd00000000000000000037373800ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dddddd0000000000000000000000000037373800ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00efeded00ffffff00a9a9a900000000000000000097979700ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00373738000000000052525500ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00bbbbbb005252550000000000ffffff00ffffff00ffffff00474747000000000047474700ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00979797000101010000000000cfcfcf00ffffff00ffffff00373738000000000052525500ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00cfcfcf00000000000000000047474700ffffff009797970000000000000000008b8b8b00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff0047474700000000000000000000000000000000000000000000000000dddddd00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00c8c8c80000000000000000000000000000000000000000008b8b8b00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00bbbbbb0026262700000000001111120097979700ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00cfcfcf000c0c0c0011111200cfcfcf00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00b6b6b60000000000000000000000000000000000b6b6b600ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00c8c8c800000000000000000000000000000000000000000000000000c8c8c800ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00efeded000303030000000000000000000000000000000000000000000000000007070700f1eff100ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff005252550000000000000000000000000000000000000000000000000000000000000000005d5d5d00ffffff00ffffff00ffffff00
ffffff00ffffff00d9d9d90000000000000000000000000000000000000000000000000000000000000000000000000000000000dddbdb00ffffff00ffffff00
ffffff00ffffff0076767600000000000000000000000000000000000000000000000000000000000000000000000000000000007c7c7c00ffffff00ffffff00
ffffff00ffffff00373738000000000000000000000000000303030026262700373738000203010000000000000000000000000037373800ffffff00ffffff00
ffffff00ffffff00373738000000000000000000000000001b1b1c00bdbebd00b6b6b6001b1b1c0000000000000000000101010037373800ffffff00ffffff00
ffffff00ffffff00dbd9db00111112000000000001010100b3b3b300bbbbbb00bbbbbb00aeaeae00010101000000000011111200dfdfdd00ffffff00ffffff00
ffffff00ffffff00ffffff00efefef00a9a9a900cfcfcf00ffffff006767670067676700ffffff00cfcfcf00a9a9a900f1f1f100ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00f6f6f6001b1b1c001b1b1c00f6f6f600ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fffffd00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
i$sitin            1   1  
ffffff00
i$2d               16  35 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ceceff003e3eff000101ff000000ff005d5dfe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ddddff001212ff000101ff000000ff000101ff000101ff005d5dfe00fdfdff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff005d5dfe000000ff000000ff000000ff000000ff000000ff000000ff00bcbcff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff001212ff000000ff003e3eff00ffffff00ceceff000000ff000000ff008282fe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ddddff000000ff000000ff00a8a8ff00fdfdff00ffffff003e3eff000101ff005d5dfe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ddddff003e3eff000000ff00ddddff00ffffff00ffffff005d5dfe000000ff005d5dfe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fbfbff002727ff000404ff008282fe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff009797fe000000ff000000ff00ddddff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ddddff001212ff000000ff005d5dfe00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff003e3eff000000ff002727ff00ededff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00fbfbff008282fe000000ff001212ff00ceceff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00bcbcff000000ff000000ff00bcbcff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ededff002727ff000000ff009797fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff008282fe000000ff005d5dfe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff001212ff000101ff000101ff000000ff000000ff000000ff000101ff005d5dfe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ceceff000000ff000000ff000000ff000000ff000000ff000000ff000404ff005d5dfe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff008282fe000000ff000000ff000000ff000000ff000000ff000000ff000000ff005d5dfe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00a8a8ff00fbfbff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c0c0fd000000ff009797fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00eaeafd001212ff000000ff000101ff00d7d7ff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00fbfbff003e3eff000000ff000000ff000000ff002727ff00f3f3ff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00fbfbff008282fe000000ff000000ff000000ff000000ff000000ff005d5dfe00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00a8a8ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff008282fe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00d7d7ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff00b3b3ff00ffffff00ffffff00ffffff00
ffffff00ffffff00f5f5ff003e3eff000000ff000000ff000000ff000000ff000000ff000000ff000000ff002727ff00e6e6fe00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00efefff002727ff000000ff000000ff000000ff000000ff000000ff000404ff00e6e6fe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ddddff000404ff000000ff000000ff000000ff000000ff00c7c7fd00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00b9b9fc000000ff000000ff000000ff008282fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff008282fe000000ff003e3eff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00fbfbff00efefff003e3eff00e3e3ff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fdfdff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
i$9h               16  35 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00a8a8ff001212ff000000ff003e3eff00bcbcff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00a8a8ff000000ff000000ff000000ff000000ff000000ff00ceceff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ededff001212ff000000ff000000ff000000ff000000ff000000ff003e3eff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00a8a8ff000000ff000000ff008282fe00ffffff003e3eff000101ff000101ff00ddddff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff008282fe000000ff000000ff00ffffff00ffffff00ceceff000000ff000000ff00a8a8ff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff008282fe000000ff002727ff00ffffff00ffffff00ffffff000000ff000101ff008282fe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff008282fe000404ff000000ff00ffffff00ffffff00ededff000000ff000000ff005d5dfe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00a8a8ff000000ff000000ff008282fe00ffffff008282fe000000ff000101ff005d5dfe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ededff001212ff000000ff000101ff000000ff000000ff000000ff000404ff005d5dfe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff008282fe000000ff000000ff000000ff000000ff001212ff000404ff005d5dfe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff008282fe000101ff000000ff00a8a8ff002727ff000000ff005d5dfe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff000000ff000101ff008282fe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ddddff003e3eff000000ff00ceceff00ffffff00ceceff000000ff000101ff00adadfb00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ededff000101ff000404ff005d5dfe00fbfbff005d5dfe000000ff000000ff00ededff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff003e3eff000000ff000101ff000101ff000000ff000000ff005d5dfe00fdfdff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00fdfdff009797fe000000ff000000ff000000ff000000ff001212ff00dbdbfe00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff009797fe000000ff000101ff003e3eff00ceceff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00f5f5ff009797fe009797fe00e6e6fe00ffffff00ffffff00d7d7ff009797fe00a8a8ff00fdfdff00ffffff00ffffff00ffffff00
ffffff00ffffff00f7f7ff005d5dfe000000ff000000ff002727ff00e3e3ff00c7c7fd001212ff000000ff000000ff008282fe00ffffff00ffffff00ffffff00
ffffff00ffffff00b0b0fe000000ff000000ff000000ff000000ff005d5dfe003e3eff000000ff000000ff000000ff000101ff00ceceff00ffffff00ffffff00
ffffff00ffffff008282fe000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff009797fe00ffffff00ffffff00
ffffff00fbfbff008282fe000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff00b0b0fe00ffffff00ffffff00
ffffff00ffffff00ceceff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff001212ff00eaeafd00ffffff00ffffff00
ffffff00ffffff00fdfdff005d5dfe000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff008282fe00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ededff002727ff000101ff000000ff000000ff000000ff000000ff000000ff005d5dfe00f3f3ff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ddddff002727ff000000ff000000ff000000ff000000ff003e3eff00eaeafd00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd000a0aff000000ff000000ff002727ff00ddddff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00b0b0fe000404ff000a0aff00ceceff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff005d5dfe008282fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e3e3ff00ededff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
i$5s               16  35 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff0052525500000000000000000000000000000000000000000097979700ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff0026262700000000000000000000000000000000000000000097979700ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff0000000000000000000000000000000000000000000000000097979700ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00cfcfcf000000000026262700ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00a9a9a9000000000047474700fdfdfd00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff008b8b8b000000000037373800111112000000000052525500efeded00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff0052525500000000000000000000000000000000000000000047474700ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff0037373800000000000000000000000000000000000000000000000000a9a9a900ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff0047474700000000008b8b8b00ffffff00cfcfcf00111112000000000052525500ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00767676000000000026262700ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff008b8b8b00000000001b1b1c00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00dddddd006767670000000000cfcfcf00ffffff00ffffff008b8b8b000000000026262700ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00dddddd0000000000000000008b8b8b00ffffff00ffffff00676767000000000037373800fdfdfd00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00111112000000000026262700ededed00cfcfcf00000000000000000076767600ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff008b8b8b00000000000000000000000000000000000000000000000000dddddd00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ebebeb00262627000000000000000000000000000000000076767600ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00dddbdb004747470000000000000000008b8b8b00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00cfcfcf000c0c0c0011111200cfcfcf00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00b6b6b60000000000000000000000000000000000b6b6b600ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00c8c8c800000000000000000000000000000000000000000000000000c8c8c800ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00efeded000303030000000000000000000000000000000000000000000000000007070700f1eff100ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff005252550000000000000000000000000000000000000000000000000000000000000000005d5d5d00ffffff00ffffff00ffffff00
ffffff00ffffff00d9d9d90000000000000000000000000000000000000000000000000000000000000000000000000000000000dddbdb00ffffff00ffffff00
ffffff00ffffff0076767600000000000000000000000000000000000000000000000000000000000000000000000000000000007c7c7c00ffffff00ffffff00
ffffff00ffffff00373738000000000000000000000000000303030026262700373738000203010000000000000000000000000037373800ffffff00ffffff00
ffffff00ffffff00373738000000000000000000000000001b1b1c00bdbebd00b6b6b6001b1b1c0000000000000000000101010037373800ffffff00ffffff00
ffffff00ffffff00dbd9db00111112000000000001010100b3b3b300bbbbbb00bbbbbb00aeaeae00010101000000000011111200dfdfdd00ffffff00ffffff00
ffffff00ffffff00ffffff00efefef00a9a9a900cfcfcf00ffffff006767670067676700ffffff00cfcfcf00a9a9a900f1f1f100ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00f6f6f6001b1b1c001b1b1c00f6f6f600ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fffffd00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
i$7s               16  35 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00bbbbbb000000000000000000000000000000000000000000000000000000000047474700ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00bbbbbb000000000000000000000000000000000000000000000000000000000047474700ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00bbbbbb000000000000000000000000000000000000000000000000000000000047474700ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00bbbbbb00000000000000000097979700ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00474747000000000047474700ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00bbbbbb000000000000000000bbbbbb00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00676767000000000037373800ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00efeded00111112000000000084848400ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00a9a9a9000000000000000000efeded00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00767676000000000047474700ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff0037373800000000008b8b8b00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00efeded000000000000000000bbbbbb00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00a9a9a9000000000000000000efeded00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff008b8b8b000000000026262700ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00767676000000000047474700ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00676767000000000067676700ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00474747000000000067676700ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00cfcfcf000c0c0c0011111200cfcfcf00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00b6b6b60000000000000000000000000000000000b6b6b600ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00c8c8c800000000000000000000000000000000000000000000000000c8c8c800ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00efeded000303030000000000000000000000000000000000000000000000000007070700f1eff100ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff005252550000000000000000000000000000000000000000000000000000000000000000005d5d5d00ffffff00ffffff00ffffff00
ffffff00ffffff00d9d9d90000000000000000000000000000000000000000000000000000000000000000000000000000000000dddbdb00ffffff00ffffff00
ffffff00ffffff0076767600000000000000000000000000000000000000000000000000000000000000000000000000000000007c7c7c00ffffff00ffffff00
ffffff00ffffff00373738000000000000000000000000000303030026262700373738000203010000000000000000000000000037373800ffffff00ffffff00
ffffff00ffffff00373738000000000000000000000000001b1b1c00bdbebd00b6b6b6001b1b1c0000000000000000000101010037373800ffffff00ffffff00
ffffff00ffffff00dbd9db00111112000000000001010100b3b3b300bbbbbb00bbbbbb00aeaeae00010101000000000011111200dfdfdd00ffffff00ffffff00
ffffff00ffffff00ffffff00efefef00a9a9a900cfcfcf00ffffff006767670067676700ffffff00cfcfcf00a9a9a900f1f1f100ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00f6f6f6001b1b1c001b1b1c00f6f6f600ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fffffd00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
i$Kh               16  35 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff008282fe000000ff000000ff00ffffff00ffffff00ffffff00ffffff008282fe000000ff000000ff002727ff00ededff00ffffff00ffffff00
ffffff00ffffff008282fe000000ff000000ff00ffffff00ffffff00ffffff00ddddff000101ff000000ff000000ff00ceceff00ffffff00ffffff00ffffff00
ffffff00ffffff008282fe000404ff000000ff00ffffff00ffffff00ffffff003e3eff000000ff000000ff008282fe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff008282fe000101ff000000ff00ffffff00ffffff008282fe000000ff000000ff003e3eff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff008282fe000000ff000000ff00ffffff00ceceff000000ff000000ff001212ff00dbdbfe00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff008282fe000101ff000000ff00fbfbff003e3eff000000ff000101ff008282fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff008282fe000404ff000000ff008282fe000000ff000000ff003e3eff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff008282fe000101ff000000ff000101ff000000ff000000ff000000ff00ddddff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff008282fe000000ff000000ff000000ff000000ff000000ff000101ff005d5dfe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff008282fe000101ff000000ff000000ff008282fe002727ff000000ff001212ff00efefff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff008282fe000404ff000000ff002727ff00f9f9ff008282fe000000ff000000ff008282fe00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff008282fe000101ff000000ff00ceceff00ffffff00ededff001212ff000000ff001212ff00ededff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff008282fe000000ff000000ff00ffffff00ffffff00ffffff005d5dfe000000ff000101ff008282fe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff008282fe000101ff000000ff00ffffff00ffffff00ffffff00ceceff000101ff000000ff002727ff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff008282fe000404ff000000ff00ffffff00ffffff00ffffff00ffffff003e3eff000101ff000000ff009797fe00ffffff00ffffff00ffffff00
ffffff00ffffff008282fe000101ff000000ff00ffffff00ffffff00ffffff00ffffff009797fe000000ff000000ff002727ff00ffffff00ffffff00ffffff00
ffffff00ffffff008282fe000000ff000000ff00ffffff00ffffff00ffffff00ffffff00ffffff001212ff000000ff000000ff00bcbcff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00f5f5ff009797fe009797fe00e6e6fe00ffffff00ffffff00d7d7ff009797fe00a8a8ff00fdfdff00ffffff00ffffff00ffffff00
ffffff00ffffff00f7f7ff005d5dfe000000ff000000ff002727ff00e3e3ff00c7c7fd001212ff000000ff000000ff008282fe00ffffff00ffffff00ffffff00
ffffff00ffffff00b0b0fe000000ff000000ff000000ff000000ff005d5dfe003e3eff000000ff000000ff000000ff000101ff00ceceff00ffffff00ffffff00
ffffff00ffffff008282fe000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff009797fe00ffffff00ffffff00
ffffff00fbfbff008282fe000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff00b0b0fe00ffffff00ffffff00
ffffff00ffffff00ceceff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff001212ff00eaeafd00ffffff00ffffff00
ffffff00ffffff00fdfdff005d5dfe000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff008282fe00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ededff002727ff000101ff000000ff000000ff000000ff000000ff000000ff005d5dfe00f3f3ff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ddddff002727ff000000ff000000ff000000ff000000ff003e3eff00eaeafd00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd000a0aff000000ff000000ff002727ff00ddddff00ffffff00ffffff00ffffff00ffffff00fffffd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00b0b0fe000404ff000a0aff00ceceff00ffffff00ffffff00ffffff00ffffff00ffffff00fdfdff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff005d5dfe008282fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e3e3ff00ededff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
i$Qh               16  35 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00efefff005d5dfe000000ff000000ff000101ff005d5dfe00ededff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ededff002727ff000101ff000101ff000000ff000000ff000000ff002727ff00ddddff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff003e3eff000000ff000000ff000000ff000000ff000000ff000000ff000000ff003e3eff00fbfbff00ffffff00ffffff00ffffff00
ffffff00ffffff00bcbcff000101ff000000ff003e3eff00c7c7fd00ffffff00ceceff003e3eff000000ff000000ff00a8a8ff00ffffff00ffffff00ffffff00
ffffff00ffffff005d5dfe000000ff001212ff00dbdbfe00ffffff00ffffff00fbfbff00dbdbfe001212ff000101ff005d5dfe00ffffff00ffffff00ffffff00
ffffff00fdfdff003e3eff000404ff005d5dfe00ffffff00ffffff00ffffff00ffffff00ffffff005d5dfe000000ff002727ff00ffffff00ffffff00ffffff00
ffffff00ffffff001212ff000000ff008282fe00ffffff00ffffff00ffffff00ffffff00ffffff008282fe000404ff001212ff00ffffff00ffffff00ffffff00
ffffff00ededff000000ff000000ff00adadfb00ffffff00ffffff00ffffff00ffffff00ffffff00a8a8ff000000ff000000ff00ededff00ffffff00ffffff00
ffffff00ededff000000ff000000ff00a8a8ff00ffffff00ffffff00ffffff00ffffff00ffffff00a8a8ff000000ff000000ff00ededff00ffffff00ffffff00
ffffff00ededff000000ff000000ff00a8a8ff00ffffff00ffffff00ffffff00ffffff00ffffff00a8a8ff000000ff001212ff00ffffff00ffffff00ffffff00
ffffff00ffffff001212ff000000ff008282fe00ffffff00ffffff00ffffff00ffffff00ffffff008282fe000101ff002727ff00fdfdff00ffffff00ffffff00
ffffff00ffffff003e3eff000000ff005d5dfe00fdfdff00ffffff005d5dfe008282fe00ffffff003e3eff000000ff005d5dfe00ffffff00ffffff00ffffff00
ffffff00ffffff005d5dfe000000ff001212ff00ddddff00ffffff002727ff000000ff003e3eff000101ff000101ff00adadfb00ffffff00ffffff00ffffff00
ffffff00ffffff00bcbcff000000ff000000ff003e3eff00ddddff00a8a8ff002727ff000101ff000000ff003e3eff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff003e3eff000101ff000000ff000000ff000000ff000404ff000101ff000000ff000000ff005d5dfe00fbfbff00ffffff00ffffff00
ffffff00ffffff00ffffff00efefff002727ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff009797fe00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00eaeafd008282fe000000ff000000ff000404ff005d5dfe003e3eff000000ff000000ff00ddddff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00fbfbff00fdfdff00ffffff00ffffff00ffffff00ffffff008282fe003e3eff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00f5f5ff009797fe009797fe00e6e6fe00ffffff00ffffff00d7d7ff009797fe00a8a8ff00fdfdff00ffffff00ffffff00ffffff00
ffffff00ffffff00f7f7ff005d5dfe000000ff000000ff002727ff00e3e3ff00c7c7fd001212ff000000ff000000ff008282fe00ffffff00ffffff00ffffff00
ffffff00ffffff00b0b0fe000000ff000000ff000000ff000000ff005d5dfe003e3eff000000ff000000ff000000ff000101ff00ceceff00ffffff00ffffff00
ffffff00ffffff008282fe000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff009797fe00ffffff00ffffff00
ffffff00fbfbff008282fe000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff00b0b0fe00ffffff00ffffff00
ffffff00ffffff00ceceff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff001212ff00eaeafd00ffffff00ffffff00
ffffff00ffffff00fdfdff005d5dfe000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff008282fe00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ededff002727ff000101ff000000ff000000ff000000ff000000ff000000ff005d5dfe00f3f3ff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ddddff002727ff000000ff000000ff000000ff000000ff003e3eff00eaeafd00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd000a0aff000000ff000000ff002727ff00ddddff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00b0b0fe000404ff000a0aff00ceceff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff005d5dfe008282fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e3e3ff00ededff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fbfbff00
i$Ts               16  35 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00bbbbbb00000000008b8b8b00ffffff00ffffff00ffffff00474747000000000047474700efeded00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff0052525500000000008b8b8b00ffffff00ffffff008b8b8b0000000000000000000000000076767600ffffff00ffffff00ffffff00
ffffff00ffffff00bbbbbb0000000000000000008b8b8b00ffffff00fdfdfd002626270000000000000000000000000011111200ffffff00ffffff00ffffff00
ffffff00cfcfcf001111120000000000000000008b8b8b00ffffff00dddddd000000000026262700ffffff003737380000000000bbbbbb00ffffff00ffffff00
ffffff00373738000000000011111200000000008b8b8b00ffffff00bbbbbb000000000067676700fffffd00848484000000000097979700ffffff00ffffff00
ffffff00373738001b1b1c00676767000000000084848400ffffff00979797000000000084848400ffffff0097979700000000008b8b8b00ffffff00ffffff00
ffffff0067676700dfdfdd0076767600000000008b8b8b00ffffff008b8b8b00000000008b8b8b00ffffff00a9a9a9000000000076767600ffffff00ffffff00
ffffff00ffffff00ffffff0076767600000000008b8b8b00ffffff008b8b8b00000000008b8b8b00ffffff00a9a9a9000000000076767600ffffff00ffffff00
ffffff00ffffff00ffffff0076767600000000008b8b8b00ffffff0084848400000000008b8b8b00ffffff00a9a9a9000000000076767600ffffff00ffffff00
ffffff00ffffff00ffffff00767676000000000084848400ffffff008b8b8b000000000084848400ffffff00a9a9a9000000000076767600ffffff00ffffff00
ffffff00ffffff00ffffff0076767600000000008b8b8b00ffffff008b8b8b00000000008b8b8b00ffffff00a9a9a9000000000076767600ffffff00ffffff00
ffffff00ffffff00ffffff0076767600000000008b8b8b00ffffff0084848400000000008b8b8b00ffffff0097979700000000008b8b8b00ffffff00ffffff00
ffffff00ffffff00ffffff0076767600000000008b8b8b00ffffff00a9a9a9000000000067676700ffffff008b8b8b000000000097979700ffffff00ffffff00
ffffff00ffffff00ffffff00767676000000000084848400ffffff00dddddd000000000026262700fdfdfd003737380000000000bbbbbb00ffffff00ffffff00
ffffff00ffffff00ffffff0076767600000000008b8b8b00ffffff00ffffff002626270000000000000000000000000011111200ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff0076767600000000008b8b8b00ffffff00ffffff008484840000000000000000000000000076767600ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff0076767600000000008b8b8b00ffffff00ffffff00ffffff00525255000000000047474700efeded00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00cfcfcf000c0c0c0011111200cfcfcf00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00b6b6b60000000000000000000000000000000000b6b6b600ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00c8c8c800000000000000000000000000000000000000000000000000c8c8c800ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00efeded000303030000000000000000000000000000000000000000000000000007070700f1eff100ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff005252550000000000000000000000000000000000000000000000000000000000000000005d5d5d00ffffff00ffffff00ffffff00
ffffff00ffffff00d9d9d90000000000000000000000000000000000000000000000000000000000000000000000000000000000dddbdb00ffffff00ffffff00
ffffff00ffffff0076767600000000000000000000000000000000000000000000000000000000000000000000000000000000007c7c7c00ffffff00ffffff00
ffffff00ffffff00373738000000000000000000000000000303030026262700373738000203010000000000000000000000000037373800ffffff00ffffff00
ffffff00ffffff00373738000000000000000000000000001b1b1c00bdbebd00b6b6b6001b1b1c0000000000000000000101010037373800ffffff00ffffff00
ffffff00ffffff00dbd9db00111112000000000001010100b3b3b300bbbbbb00bbbbbb00aeaeae00010101000000000011111200dfdfdd00ffffff00ffffff00
ffffff00ffffff00ffffff00efefef00a9a9a900cfcfcf00ffffff006767670067676700ffffff00cfcfcf00a9a9a900f1f1f100ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00f6f6f6001b1b1c001b1b1c00f6f6f600ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fffffd00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
i$Jd               16  35 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff009797fe000000ff000000ff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff009797fe000000ff000000ff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff009797fe000000ff000000ff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff009797fe000000ff000000ff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff009797fe000000ff000000ff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff009797fe000000ff000000ff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff009797fe000000ff000000ff00ffffff00ffffff00ffffff00ffffff00fdfdff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff009797fe000000ff000000ff00ffffff00ffffff00ffffff00ffffff00fdfdff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff009797fe000000ff000000ff00ffffff00ffffff00ffffff00ffffff00fbfdfd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff009797fe000000ff000000ff00ffffff00ffffff00ffffff00ffffff00fbfbff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff009797fe000000ff000000ff00ffffff00ffffff00ffffff00ffffff00f9f9ff00
ffffff00ffffff00ffffff003e3eff000101ff005d5dfe00ffffff00ffffff009797fe000000ff000000ff00ffffff00ffffff00ffffff00ffffff00f9f9ff00
ffffff00ffffff00ffffff005d5dfe000000ff003e3eff00ffffff00ffffff008282fe000101ff000000ff00ffffff00ffffff00ffffff00ffffff00f9f9ff00
ffffff00ffffff00ffffff008282fe000000ff000000ff00ceceff00ffffff003e3eff000000ff003e3eff00fbfbff00ffffff00ffffff00ffffff00f7f7ff00
ffffff00ffffff00fbfbff00b9b9fc000000ff000000ff000000ff000000ff000000ff000000ff008282fe00ffffff00ffffff00ffffff00ffffff00f7f7fd00
ffffff00ffffff00ffffff00ffffff003e3eff000000ff000000ff000000ff000000ff001212ff00eaeafd00fbfbff00ffffff00ffffff00ffffff00f5f5fd00
ffffff00ffffff00ffffff00ffffff00efefff005d5dfe000000ff000000ff003e3eff00ceceff00ffffff00ffffff00ffffff00ffffff00ffffff00f5f5fd00
ffffff00ffffff00ffffff00ffffff00ffffff00fdfdff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00f3f3fd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00a8a8ff00fbfbff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00f3f3fd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c0c0fd000000ff009797fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00f1f1fd00
ffffff00ffffff00ffffff00ffffff00ffffff00eaeafd001212ff000000ff000101ff00d7d7ff00ffffff00ffffff00ffffff00ffffff00ffffff00f1f1fd00
ffffff00ffffff00ffffff00ffffff00fbfbff003e3eff000000ff000000ff000000ff002727ff00f3f3ff00ffffff00ffffff00ffffff00ffffff00efeffd00
ffffff00ffffff00ffffff00fbfbff008282fe000000ff000000ff000000ff000000ff000000ff005d5dfe00ffffff00ffffff00ffffff00ffffff00efeffd00
ffffff00ffffff00ffffff00a8a8ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff008282fe00ffffff00ffffff00ffffff00ededfa00
ffffff00ffffff00d7d7ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff00b3b3ff00ffffff00ffffff00ededfa00
ffffff00ffffff00f5f5ff003e3eff000000ff000000ff000000ff000000ff000000ff000000ff000000ff002727ff00e6e6fe00ffffff00ffffff00eaeafd00
ffffff00ffffff00ffffff00efefff002727ff000000ff000000ff000000ff000000ff000000ff000404ff00e6e6fe00ffffff00ffffff00ffffff00e9e9fb00
ffffff00ffffff00ffffff00ffffff00ddddff000404ff000000ff000000ff000000ff000000ff00c7c7fd00ffffff00ffffff00ffffff00ffffff00eaeafd00
ffffff00ffffff00ffffff00ffffff00ffffff00b9b9fc000000ff000000ff000000ff008282fe00ffffff00ffffff00ffffff00ffffff00ffffff00e7e7fb00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff008282fe000000ff003e3eff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e5e5fb00
ffffff00ffffff00ffffff00ffffff00ffffff00fbfbff00efefff003e3eff00e3e3ff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e5e5fb00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fdfdff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e3e3f900
i$Td               16  35 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00bcbcff000000ff008282fe00ffffff00ffffff00ffffff003e3eff000101ff003e3eff00efefff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff005d5dfe000000ff008282fe00ffffff00ffffff008282fe000404ff000000ff000101ff008282fe00fbfbff00ffffff00ffffff00
ffffff00ffffff00bcbcff000000ff000404ff008282fe00ffffff00ffffff002727ff000000ff000000ff000000ff000a0aff00ffffff00ffffff00ffffff00
ffffff00ceceff001212ff000000ff000101ff008282fe00ffffff00ddddff000000ff002727ff00fbfbff003e3eff000000ff00bcbcff00ffffff00ffffff00
ffffff003e3eff000000ff001212ff000000ff008282fe00ffffff00bcbcff000000ff005d5dfe00ffffff008282fe000000ff009797fe00ffffff00ffffff00
fdfdff002727ff002727ff005d5dfe000404ff008282fe00ffffff009797fe000101ff008282fe00ffffff009797fe000000ff008282fe00ffffff00ffffff00
ffffff005d5dfe00dbdbfe008282fe000404ff008282fe00ffffff008282fe000404ff008282fe00ffffff00a8a8ff000000ff008282fe00ffffff00ffffff00
ffffff00ffffff00fdfdff008282fe000000ff008282fe00ffffff008282fe000404ff008282fe00ffffff00a8a8ff000000ff008282fe00fdfdff00ffffff00
ffffff00ffffff00ffffff008282fe000000ff008282fe00ffffff008282fe000404ff008282fe00ffffff00a8a8ff000000ff008282fe00f9f9ff00ffffff00
ffffff00ffffff00fbfbff008282fe000000ff008282fe00ffffff008282fe000404ff008282fe00ffffff00a8a8ff000000ff008282fe00fbfbff00ffffff00
ffffff00ffffff00f9f9ff008282fe000404ff008282fe00ffffff008282fe000404ff008282fe00ffffff00a8a8ff000000ff008282fe00ffffff00ffffff00
ffffff00ffffff00fbfbff008282fe000000ff008282fe00ffffff008282fe000404ff008282fe00ffffff009797fe000000ff008282fe00fbfbff00ffffff00
ffffff00ffffff00ffffff008282fe000000ff008282fe00ffffff00adadfb000101ff005d5dfe00ffffff008282fe000101ff009797fe00ffffff00ffffff00
ffffff00ffffff00fbfbff008282fe000000ff008282fe00ffffff00ddddff000101ff002727ff00ffffff003e3eff000000ff00bcbcff00ffffff00ffffff00
ffffff00ffffff00f9f9ff008282fe000404ff008282fe00ffffff00ffffff002727ff000000ff000000ff000000ff001212ff00ffffff00ffffff00ffffff00
ffffff00ffffff00fbfbff008282fe000000ff008282fe00ffffff00ffffff008282fe000000ff000000ff000000ff008282fe00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff008282fe000000ff008282fe00ffffff00ffffff00ffffff005d5dfe000000ff003e3eff00ededff00fdfdff00ffffff00ffffff00
ffffff00ffffff00fbfbff00fbfbff00ffffff00ffffff00ffffff00ffffff00ffffff00fdfdff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00a8a8ff00fbfbff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c0c0fd000000ff009797fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00eaeafd001212ff000000ff000101ff00d7d7ff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00fbfbff003e3eff000000ff000000ff000000ff002727ff00f3f3ff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00fbfbff008282fe000000ff000000ff000000ff000000ff000000ff005d5dfe00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00a8a8ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff008282fe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00d7d7ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff00b3b3ff00ffffff00ffffff00ffffff00
ffffff00ffffff00f5f5ff003e3eff000000ff000000ff000000ff000000ff000000ff000000ff000000ff002727ff00e6e6fe00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00efefff002727ff000000ff000000ff000000ff000000ff000000ff000404ff00e6e6fe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ddddff000404ff000000ff000000ff000000ff000000ff00c7c7fd00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00b9b9fc000000ff000000ff000000ff008282fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff008282fe000000ff003e3eff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00fbfbff00efefff003e3eff00e3e3ff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fdfdff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
i$Jh               16  35 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff009797fe000000ff000000ff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff009797fe000000ff000000ff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff009797fe000000ff000000ff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff009797fe000000ff000000ff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff009797fe000000ff000000ff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff009797fe000000ff000000ff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff009797fe000000ff000000ff00ffffff00ffffff00ffffff00ffffff00fdfdff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff009797fe000000ff000000ff00ffffff00ffffff00ffffff00ffffff00fdfdff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff009797fe000000ff000000ff00ffffff00ffffff00ffffff00ffffff00f7f7fd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff009797fe000000ff000000ff00ffffff00ffffff00ffffff00ffffff00f9f9ff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff009797fe000000ff000000ff00ffffff00ffffff00ffffff00ffffff00f9f9ff00
ffffff00ffffff00ffffff003e3eff000101ff005d5dfe00ffffff00ffffff009797fe000000ff000000ff00ffffff00ffffff00ffffff00ffffff00f9f9ff00
ffffff00ffffff00ffffff005d5dfe000000ff003e3eff00ffffff00ffffff008282fe000000ff000000ff00ffffff00ffffff00ffffff00ffffff00f9f9ff00
ffffff00ffffff00fbfbff008282fe000000ff000000ff00c7c7fd00fbfbff003e3eff000000ff003e3eff00ffffff00ffffff00ffffff00ffffff00f7f7ff00
ffffff00ffffff00ffffff00b9b9fc000000ff000000ff000000ff000000ff000000ff000000ff008282fe00f9f9ff00ffffff00ffffff00ffffff00f7f7fd00
ffffff00ffffff00ffffff00ffffff003e3eff000101ff000000ff000000ff000000ff000a0aff00eaeafd00ffffff00ffffff00ffffff00ffffff00f5f5fd00
ffffff00ffffff00ffffff00ffffff00ededff005d5dfe000000ff000101ff003e3eff00ceceff00ffffff00ffffff00ffffff00ffffff00ffffff00f5f5fd00
ffffff00ffffff00ffffff00ffffff00ffffff00fdfdff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00f3f3fd00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00f3f3fd00
ffffff00ffffff00ffffff00f5f5ff009797fe009797fe00e6e6fe00ffffff00ffffff00d7d7ff009797fe00a8a8ff00fdfdff00ffffff00ffffff00f1f1fd00
ffffff00ffffff00f7f7ff005d5dfe000000ff000000ff002727ff00e3e3ff00c7c7fd001212ff000000ff000000ff008282fe00ffffff00ffffff00f1f1fa00
ffffff00ffffff00b0b0fe000000ff000000ff000000ff000000ff005d5dfe003e3eff000000ff000000ff000000ff000101ff00ceceff00ffffff00efeffd00
ffffff00ffffff008282fe000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff009797fe00ffffff00efeffd00
ffffff00fbfbff008282fe000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff00b0b0fe00ffffff00ededfa00
ffffff00ffffff00ceceff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff001212ff00eaeafd00ffffff00ededfa00
ffffff00ffffff00fdfdff005d5dfe000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff008282fe00ffffff00ffffff00eaeafd00
ffffff00ffffff00ffffff00ededff002727ff000101ff000000ff000000ff000000ff000000ff000000ff005d5dfe00f3f3ff00ffffff00ffffff00e9e9fb00
ffffff00ffffff00ffffff00ffffff00ddddff002727ff000000ff000000ff000000ff000000ff003e3eff00eaeafd00ffffff00ffffff00ffffff00e9e9fb00
ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd000a0aff000000ff000000ff002727ff00ddddff00ffffff00ffffff00ffffff00ffffff00e7e7fb00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00b0b0fe000404ff000a0aff00ceceff00ffffff00ffffff00ffffff00ffffff00ffffff00e5e5fb00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff005d5dfe008282fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e5e5fb00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e3e3ff00ededff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e3e3f900
i$Kc               16  35 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff008b8b8b000000000000000000ffffff00ffffff00ffffff00ffffff008b8b8b00000000000000000026262700efeded00ffffff00ffffff00
ffffff00ffffff008b8b8b000000000000000000ffffff00ffffff00ffffff00dddddd00000000000000000000000000c8c8c800ffffff00ffffff00ffffff00
ffffff00ffffff008b8b8b000000000000000000ffffff00ffffff00ffffff003737380000000000000000008b8b8b00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff008b8b8b000000000000000000ffffff00ffffff008b8b8b00000000000000000037373800fdfdfd00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff008b8b8b000000000000000000ffffff00cfcfcf00000000000000000011111200dddbdb00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00848484000000000000000000ffffff003737380000000000000000008b8b8b00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff008b8b8b0000000000000000008b8b8b00000000000000000037373800fdfdfd00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff008b8b8b00000000000000000000000000000000000000000000000000dddddd00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff008b8b8b0000000000000000000000000000000000000000000000000067676700ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff008484840000000000000000000000000076767600262627000000000011111200efeded00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff008b8b8b00000000000000000026262700ffffff008484840000000000000000008b8b8b00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff008b8b8b000000000000000000c8c8c800ffffff00efeded00111112000000000011111200efeded00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff008b8b8b000000000000000000ffffff00ffffff00ffffff006767670000000000000000008b8b8b00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00848484000000000000000000ffffff00ffffff00ffffff00cfcfcf00000000000000000026262700ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff008b8b8b000000000000000000ffffff00ffffff00ffffff00ffffff0047474700000000000000000097979700ffffff00ffffff00ffffff00
ffffff00ffffff008b8b8b000000000000000000ffffff00ffffff00ffffff00ffffff0097979700000000000000000026262700ffffff00ffffff00ffffff00
ffffff00ffffff008b8b8b000000000000000000ffffff00ffffff00ffffff00ffffff00ffffff00111112000000000000000000bbbbbb00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c8c8c80076767600bbbbbb00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00c0c0c000000000000000000000000000a9a9a900ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00f6f6f6001111120000000000000000000000000000000000f1eff100ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00f3f3f3000000000000000000000000000000000000000000e7e5e700ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff005252550000000000000000000000000037373800fbfdfd00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00f3f3f3006e6e6e0076767600a9a9a900373738000000000026262700a9a9a9007676760067676700ededed00ffffff00ffffff00ffffff00
ffffff00fafafa001b1b1c0000000000000000000707070026262700000000001b1b1c000c0c0c0000000000000000000c0c0c00f3f3f300ffffff00ffffff00
ffffff00a9a9a900000000000000000000000000000000000101010002030100010101000000000000000000000000000000000097979700ffffff00ffffff00
ffffff0084848400000000000000000000000000000000005d5d5d00a0a0a000767676000000000000000000000000000000000084848400ffffff00ffffff00
ffffff00ededed0000000000000000000000000000000000c8c8c8007c7c7c00cfcfcf0000000000000000000000000000000000dfdfdf00ffffff00ffffff00
ffffff00ffffff00c8c8c800262627001b1b1c00a0a0a000fafafa000c0c0c00f3f3f300b1b1b1001b1b1c001b1b1c00bdbebd00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00f9f7f700f6f6f600ffffff00e5e3e50000000000cfcfcf00ffffff00f6f6f600f6f6f600ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dddbdb0097979700cfcfcf00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
i$6h               16  35 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00fbfbff008282fe000000ff000000ff005d5dfe00ededff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00fdfdff005d5dfe000000ff000000ff000000ff000000ff005d5dfe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00a8a8ff000000ff000000ff000000ff000000ff000000ff000000ff00c7c7fd00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff005d5dfe000000ff001212ff00ddddff00ceceff000000ff000000ff008282fe00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00fdfdff001212ff000000ff008282fe00ffffff00fbfbff003e3eff001212ff00a8a8ff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ddddff000000ff000000ff009797fe00fdfdff00ffffff00fbfbff00fdfdff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00bcbcff000000ff000000ff009797fe003e3eff000000ff003e3eff00ddddff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00bcbcff000000ff000000ff001212ff000000ff000000ff000101ff003e3eff00fbfbff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00bcbcff000000ff000000ff000000ff000000ff000000ff000000ff000000ff009797fe00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00bcbcff000000ff000000ff002727ff00ededff00ceceff001212ff000000ff005d5dfe00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00bcbcff000000ff000000ff008282fe00ffffff00ffffff005d5dfe000101ff002727ff00fbfbff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ddddff000000ff000000ff009797fe00ffffff00ffffff008282fe000000ff002727ff00fdfdff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff000000ff000000ff008282fe00ffffff00ffffff005d5dfe000000ff002727ff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff003e3eff000000ff001212ff00c7c7fd00ddddff001212ff000000ff005d5dfe00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff009797fe000101ff000000ff000000ff000000ff000000ff000000ff009797fe00fdfdff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff003e3eff000000ff000000ff000000ff000000ff003e3eff00fbfbff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00fdfdff00ededff005d5dfe000000ff000101ff005d5dfe00eaeafd00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00f5f5ff009797fe009797fe00e6e6fe00ffffff00ffffff00d7d7ff009797fe00a8a8ff00fdfdff00ffffff00ffffff00ffffff00
ffffff00ffffff00f7f7ff005d5dfe000000ff000000ff002727ff00e3e3ff00c7c7fd001212ff000000ff000000ff008282fe00ffffff00ffffff00ffffff00
ffffff00ffffff00b0b0fe000000ff000000ff000000ff000000ff005d5dfe003e3eff000000ff000000ff000000ff000101ff00ceceff00ffffff00ffffff00
ffffff00ffffff008282fe000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff009797fe00ffffff00ffffff00
ffffff00fbfbff008282fe000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff00b0b0fe00ffffff00ffffff00
ffffff00ffffff00ceceff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff001212ff00eaeafd00ffffff00ffffff00
ffffff00ffffff00fdfdff005d5dfe000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff008282fe00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ededff002727ff000101ff000000ff000000ff000000ff000000ff000000ff005d5dfe00f3f3ff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ddddff002727ff000000ff000000ff000000ff000000ff003e3eff00eaeafd00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd000a0aff000000ff000000ff002727ff00ddddff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00b0b0fe000404ff000a0aff00ceceff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff005d5dfe008282fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e3e3ff00ededff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
i$5h               16  35 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00fdfdff005d5dfe000000ff000000ff000000ff000000ff000000ff009797fe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00fdfdff002727ff000000ff000000ff000000ff000000ff000000ff009797fe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff000000ff000000ff000000ff000000ff000000ff000000ff009797fe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ceceff000000ff002727ff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00a8a8ff000000ff003e3eff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff008282fe000000ff003e3eff001212ff000000ff005d5dfe00ededff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff005d5dfe000000ff000000ff000000ff000000ff000000ff003e3eff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff002727ff000404ff000000ff000000ff000000ff000000ff000101ff00a8a8ff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff003e3eff000101ff008282fe00ffffff00ceceff001212ff000000ff005d5dfe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fbfbff008282fe000000ff002727ff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff008282fe000000ff002727ff00fbfbff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ddddff005d5dfe000101ff00ceceff00ffffff00ffffff008282fe000000ff002727ff00fdfdff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ddddff000101ff000000ff008282fe00ffffff00ffffff005d5dfe000101ff002727ff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff001212ff000000ff002727ff00ededff00ceceff000404ff000000ff008282fe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff008282fe000000ff000000ff000000ff000000ff000000ff000000ff00dbdbfe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00efefff002727ff000000ff000000ff000000ff000000ff008282fe00fbfbff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ddddff003e3eff000000ff000000ff008282fe00fbfbff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00f5f5ff009797fe009797fe00e6e6fe00ffffff00ffffff00d7d7ff009797fe00a8a8ff00fdfdff00ffffff00ffffff00ffffff00
ffffff00ffffff00f7f7ff005d5dfe000000ff000000ff002727ff00e3e3ff00c7c7fd001212ff000000ff000000ff008282fe00ffffff00ffffff00ffffff00
ffffff00ffffff00b0b0fe000000ff000000ff000000ff000000ff005d5dfe003e3eff000000ff000000ff000000ff000101ff00ceceff00ffffff00ffffff00
ffffff00ffffff008282fe000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff009797fe00ffffff00ffffff00
ffffff00fbfbff008282fe000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff00b0b0fe00ffffff00ffffff00
ffffff00ffffff00ceceff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff001212ff00eaeafd00ffffff00ffffff00
ffffff00ffffff00fdfdff005d5dfe000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff008282fe00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ededff002727ff000101ff000000ff000000ff000000ff000000ff000000ff005d5dfe00f3f3ff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ddddff002727ff000000ff000000ff000000ff000000ff003e3eff00eaeafd00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd000a0aff000000ff000000ff002727ff00ddddff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00b0b0fe000404ff000a0aff00ceceff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff005d5dfe008282fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e3e3ff00ededff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
i$3h               16  35 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00a8a8ff002727ff000000ff001212ff00a8a8ff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00bcbcff000000ff000000ff000000ff000000ff000000ff00a8a8ff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff003e3eff000000ff000000ff000000ff000000ff000000ff002727ff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ededff000101ff000000ff005d5dfe00fdfdff008282fe000000ff000000ff00ddddff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00dbdbfe008282fe000000ff00a8a8ff00fdfdff00a8a8ff000000ff000000ff00ddddff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00fbfbff00fdfdff00ffffff00fdfdff005d5dfe000000ff001212ff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff000000ff000000ff000000ff00a8a8ff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ddddff000000ff000000ff003e3eff00fdfdff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ddddff000000ff000000ff000000ff003e3eff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ededff00ffffff00a8a8ff000000ff000000ff009797fe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff003e3eff000000ff005d5dfe00fdfdff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00bcbcff005d5dfe000000ff00ffffff00ffffff00fdfdff003e3eff000000ff003e3eff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff009797fe000000ff000000ff00ceceff00ffffff00fbfbff003e3eff000000ff005d5dfe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ceceff000000ff000101ff003e3eff00ffffff009797fe000000ff000000ff008282fe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff003e3eff000101ff000101ff000000ff000000ff000000ff000000ff00ddddff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ceceff000000ff000000ff000000ff000000ff000000ff008282fe00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00bcbcff002727ff000000ff001212ff009797fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fdfdff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00f5f5ff009797fe009797fe00e6e6fe00ffffff00ffffff00d7d7ff009797fe00a8a8ff00fdfdff00ffffff00ffffff00ffffff00
ffffff00ffffff00f7f7ff005d5dfe000000ff000000ff002727ff00e3e3ff00c7c7fd001212ff000000ff000000ff008282fe00ffffff00ffffff00ffffff00
ffffff00ffffff00b0b0fe000000ff000000ff000000ff000000ff005d5dfe003e3eff000000ff000000ff000000ff000101ff00ceceff00ffffff00ffffff00
ffffff00ffffff008282fe000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff009797fe00ffffff00ffffff00
ffffff00fbfbff008282fe000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff00b0b0fe00ffffff00ffffff00
ffffff00ffffff00ceceff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff001212ff00eaeafd00ffffff00ffffff00
ffffff00ffffff00fdfdff005d5dfe000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff008282fe00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ededff002727ff000101ff000000ff000000ff000000ff000000ff000000ff005d5dfe00f3f3ff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ddddff002727ff000000ff000000ff000000ff000000ff003e3eff00eaeafd00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd000a0aff000000ff000000ff002727ff00ddddff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00b0b0fe000404ff000a0aff00ceceff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff005d5dfe008282fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e3e3ff00ededff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
i$9s               16  35 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00a9a9a900111112000000000037373800bbbbbb00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00a9a9a9000000000000000000000000000000000000000000cfcfcf00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00efeded0011111200000000000000000000000000000000000000000047474700ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00a9a9a90000000000000000008b8b8b00ffffff00474747000000000000000000dddddd00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff008b8b8b000000000000000000ffffff00ffffff00cfcfcf000000000000000000a9a9a900ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff008b8b8b000000000026262700ffffff00ffffff00ffffff0000000000000000008b8b8b00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff008b8b8b000000000000000000ffffff00ffffff00efeded00000000000000000067676700ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00a9a9a90000000000000000008b8b8b00ffffff0076767600000000000000000067676700ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00efeded001111120000000000000000000000000000000000000000000000000067676700ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff008b8b8b0000000000000000000000000000000000111112000000000067676700ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff008b8b8b000000000000000000a9a9a900262627000000000067676700ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff0000000000000000008b8b8b00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00dddddd004747470000000000cfcfcf00ffffff00cfcfcf000000000000000000a9a9a900ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00efeded00000000000000000067676700ffffff00525255000000000000000000efeded00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff0037373800000000000000000000000000000000000000000052525500ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00979797000000000000000000000000000000000011111200dddddd00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff0097979700000000000000000047474700cfcfcf00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00cfcfcf000c0c0c0011111200cfcfcf00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00b6b6b60000000000000000000000000000000000b6b6b600ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00c8c8c800000000000000000000000000000000000000000000000000c8c8c800ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00efeded000303030000000000000000000000000000000000000000000000000007070700f1eff100ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff005252550000000000000000000000000000000000000000000000000000000000000000005d5d5d00ffffff00ffffff00ffffff00
ffffff00ffffff00d9d9d90000000000000000000000000000000000000000000000000000000000000000000000000000000000dddbdb00ffffff00ffffff00
ffffff00ffffff0076767600000000000000000000000000000000000000000000000000000000000000000000000000000000007c7c7c00ffffff00ffffff00
ffffff00ffffff00373738000000000000000000000000000303030026262700373738000203010000000000000000000000000037373800ffffff00ffffff00
ffffff00ffffff00373738000000000000000000000000001b1b1c00bdbebd00b6b6b6001b1b1c0000000000000000000101010037373800ffffff00ffffff00
ffffff00ffffff00dbd9db00111112000000000001010100b3b3b300bbbbbb00bbbbbb00aeaeae00010101000000000011111200dfdfdd00ffffff00ffffff00
ffffff00ffffff00ffffff00efefef00a9a9a900cfcfcf00ffffff006767670067676700ffffff00cfcfcf00a9a9a900f1f1f100ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00f6f6f6001b1b1c001b1b1c00f6f6f600ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fffffd00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
i$autopost         6   9  
ffffff00ffffff00ffffff00ffffff00ffffff0072737100
ffffff0072727200727272007272720072727200ffffff00
ffffff0072727200727272007272720072727200ffffff00
ffffff0072727200727272007272720072727200ffffff00
ffffff0072727200727272007272720072727200ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff0072727200
ffffff007272720072727200727272007272720072727200
ffffff007272720072727200727272007272720072727200
ffffff007272720072727200727272007272720072727200
i$4s               16  35 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00525255000000000067676700ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dddddd00000000000000000067676700ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff0067676700000000000000000067676700ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dddddd0000000000000000000000000067676700ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff006767670000000000000000000000000067676700ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00efeded001111120000000000262627000000000067676700ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff008b8b8b000000000067676700474747000000000067676700ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00efeded001111120000000000dbdbd900474747000000000067676700ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff008b8b8b000000000052525500ffffff00474747000000000067676700ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff001111120000000000bbbbbb00ffffff00474747000000000067676700ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00979797000000000047474700ffffff00fdfdfd00474747000000000067676700ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00676767000000000000000000000000000000000000000000000000000000000000000000dddddd00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00676767000000000000000000000000000000000000000000000000000000000000000000dddddd00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00676767000000000000000000000000000000000000000000000000000000000000000000dddddd00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00474747000000000067676700ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00474747000000000067676700ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00474747000000000067676700ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00cfcfcf000c0c0c0011111200cfcfcf00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00b6b6b60000000000000000000000000000000000b6b6b600ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00c8c8c800000000000000000000000000000000000000000000000000c8c8c800ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00efeded000303030000000000000000000000000000000000000000000000000007070700f1eff100ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff005252550000000000000000000000000000000000000000000000000000000000000000005d5d5d00ffffff00ffffff00ffffff00
ffffff00ffffff00d9d9d90000000000000000000000000000000000000000000000000000000000000000000000000000000000dddbdb00ffffff00ffffff00
ffffff00ffffff0076767600000000000000000000000000000000000000000000000000000000000000000000000000000000007c7c7c00ffffff00ffffff00
ffffff00ffffff00373738000000000000000000000000000303030026262700373738000203010000000000000000000000000037373800ffffff00ffffff00
ffffff00ffffff00373738000000000000000000000000001b1b1c00bdbebd00b6b6b6001b1b1c0000000000000000000101010037373800ffffff00ffffff00
ffffff00ffffff00dbd9db00111112000000000001010100b3b3b300bbbbbb00bbbbbb00aeaeae00010101000000000011111200dfdfdd00ffffff00ffffff00
ffffff00ffffff00ffffff00efefef00a9a9a900cfcfcf00ffffff006767670067676700ffffff00cfcfcf00a9a9a900f1f1f100ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00f6f6f6001b1b1c001b1b1c00f6f6f600ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fffffd00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
i$Ah               16  35 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00fbfbff003e3eff000000ff000000ff005d5dfe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ededff000000ff000000ff000000ff001212ff00ededff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00a8a8ff000000ff000000ff000000ff000000ff00a8a8ff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff008282fe000000ff003e3eff000a0aff000000ff008282fe00fbfbff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff002727ff000000ff005d5dfe005d5dfe000000ff002727ff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ededff000101ff000404ff008282fe008282fe000000ff000101ff00efefff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00a8a8ff000000ff000000ff00ceceff00dfdffc000000ff000000ff00a8a8ff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff008282fe000000ff001212ff00ffffff00ffffff003e3eff000000ff008282fe00fbfbff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff002727ff000000ff005d5dfe00ffffff00f9f9ff008282fe000000ff002727ff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ededff000101ff000101ff008282fe00ffffff00ffffff00a8a8ff000000ff000000ff00ceceff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00a8a8ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff008282fe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff008282fe000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff005d5dfe00fdfdff00ffffff00ffffff00ffffff00
ffffff00ffffff002727ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff001212ff00ffffff00ffffff00ffffff00ffffff00
ffffff00ededff000101ff000404ff008282fe00ffffff00ffffff00ffffff00ffffff008282fe000000ff000000ff00ceceff00ffffff00ffffff00ffffff00
ffffff00a8a8ff000000ff000000ff00bcbcff00ffffff00ffffff00ffffff00ffffff00ceceff000000ff000000ff008282fe00ffffff00ffffff00ffffff00
ffffff008282fe000000ff000000ff00ededff00ffffff00ffffff00ffffff00ffffff00ffffff001212ff000000ff003e3eff00ffffff00ffffff00ffffff00
ffffff002727ff000000ff003e3eff00ffffff00ffffff00ffffff00ffffff00ffffff00fbfbff005d5dfe000000ff000101ff00ededff00ffffff00ffffff00
ffffff00ffffff00ffffff00fbfbff00fdfdff00ffffff00ffffff00ffffff00ffffff00ffffff00fdfdff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00f5f5ff009797fe009797fe00e6e6fe00ffffff00ffffff00d7d7ff009797fe00a8a8ff00fdfdff00ffffff00ffffff00ffffff00
ffffff00ffffff00f7f7ff005d5dfe000000ff000000ff002727ff00e3e3ff00c7c7fd001212ff000000ff000000ff008282fe00ffffff00ffffff00ffffff00
ffffff00ffffff00b0b0fe000000ff000000ff000000ff000000ff005d5dfe003e3eff000000ff000000ff000000ff000101ff00ceceff00ffffff00ffffff00
ffffff00ffffff008282fe000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff009797fe00ffffff00ffffff00
ffffff00fbfbff008282fe000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff00b0b0fe00ffffff00ffffff00
ffffff00ffffff00ceceff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff001212ff00eaeafd00ffffff00ffffff00
ffffff00ffffff00fdfdff005d5dfe000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff008282fe00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ededff002727ff000101ff000000ff000000ff000000ff000000ff000000ff005d5dfe00f3f3ff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ddddff002727ff000000ff000000ff000000ff000000ff003e3eff00eaeafd00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd000a0aff000000ff000000ff002727ff00ddddff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00b0b0fe000404ff000a0aff00ceceff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff005d5dfe008282fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e3e3ff00ededff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
i$4c               16  35 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00525255000000000067676700ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dddddd00000000000000000067676700ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff0067676700000000000000000067676700ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dddddd0000000000000000000000000067676700ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff006767670000000000000000000000000067676700ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00efeded001111120000000000262627000000000067676700ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff008b8b8b000000000067676700474747000000000067676700ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00efeded001111120000000000dddddd00474747000000000067676700ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff008b8b8b000000000052525500fdfdfd00474747000000000067676700ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff001111120000000000bbbbbb00ffffff00474747000000000067676700ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00979797000000000047474700ffffff00ffffff00474747000000000067676700ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00676767000000000000000000000000000000000000000000000000000000000000000000dddddd00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00676767000000000000000000000000000000000000000000000000000000000000000000dddddd00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00676767000000000000000000000000000000000000000000000000000000000000000000dddddd00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00474747000000000067676700ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00474747000000000067676700ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00474747000000000067676700ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c8c8c80076767600bbbbbb00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00c0c0c000000000000000000000000000a9a9a900ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00f6f6f6001111120000000000000000000000000000000000f1eff100ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00f3f3f3000000000000000000000000000000000000000000e7e5e700ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff005252550000000000000000000000000037373800fbfdfd00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00f3f3f3006e6e6e0076767600a9a9a900373738000000000026262700a9a9a9007676760067676700ededed00ffffff00ffffff00ffffff00
ffffff00fafafa001b1b1c0000000000000000000707070026262700000000001b1b1c000c0c0c0000000000000000000c0c0c00f3f3f300ffffff00ffffff00
ffffff00a9a9a900000000000000000000000000000000000101010002030100010101000000000000000000000000000000000097979700ffffff00ffffff00
ffffff0084848400000000000000000000000000000000005d5d5d00a0a0a000767676000000000000000000000000000000000084848400ffffff00ffffff00
ffffff00ededed0000000000000000000000000000000000c8c8c8007c7c7c00cfcfcf0000000000000000000000000000000000dfdfdf00ffffff00ffffff00
ffffff00ffffff00c8c8c800262627001b1b1c00a0a0a000fafafa000c0c0c00f3f3f300b1b1b1001b1b1c001b1b1c00bdbebd00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00f9f7f700f6f6f600ffffff00e5e3e50000000000cfcfcf00ffffff00f6f6f600f6f6f600ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00dddbdb0097979700cfcfcf00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
i$8h               16  35 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00a8a8ff002727ff000000ff000101ff008282fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ceceff000000ff000000ff000000ff000000ff000000ff008282fe00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff003e3eff000000ff000000ff000000ff000000ff000000ff000000ff00ededff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff000101ff000000ff003e3eff00ffffff008282fe000101ff000000ff00a8a8ff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ddddff000000ff000101ff00a8a8ff00ffffff00ffffff000000ff000000ff009797fe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff000000ff000000ff00a8a8ff00ffffff00ffffff000000ff000000ff00a8a8ff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff003e3eff000101ff005d5dfe00fdfdff008282fe000101ff000000ff00ededff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ceceff000000ff000000ff000000ff000101ff000000ff008282fe00fbfbff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff005d5dfe000000ff000000ff000000ff002727ff00eaeafd00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff008282fe000101ff000101ff000000ff000000ff000000ff002727ff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ededff001212ff000000ff008282fe00fbfbff00bcbcff000000ff000404ff009797fe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00bcbcff000000ff000000ff00eaeafd00ffffff00ffffff003e3eff000101ff005d5dfe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff009797fe000000ff000000ff00ddddff00ffffff00ffffff003e3eff000404ff005d5dfe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ceceff000000ff000101ff005d5dfe00ffffff00bcbcff000101ff000000ff008282fe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff001212ff000000ff000101ff000101ff000000ff000000ff000000ff00b9b9fc00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00a8a8ff000000ff000000ff000000ff000000ff000000ff005d5dfe00fbfbff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00a8a8ff002727ff000000ff000000ff008282fe00fdfdff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fbfbff00fdfdff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00f5f5ff009797fe009797fe00e6e6fe00ffffff00ffffff00d7d7ff009797fe00a8a8ff00fdfdff00ffffff00ffffff00ffffff00
ffffff00ffffff00f7f7ff005d5dfe000000ff000000ff002727ff00e3e3ff00c7c7fd001212ff000000ff000000ff008282fe00ffffff00ffffff00ffffff00
ffffff00ffffff00b0b0fe000000ff000000ff000000ff000000ff005d5dfe003e3eff000000ff000000ff000000ff000101ff00ceceff00ffffff00ffffff00
ffffff00ffffff008282fe000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff009797fe00ffffff00ffffff00
ffffff00fbfbff008282fe000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff00b0b0fe00ffffff00ffffff00
ffffff00ffffff00ceceff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff001212ff00eaeafd00ffffff00ffffff00
ffffff00ffffff00fdfdff005d5dfe000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff008282fe00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ededff002727ff000101ff000000ff000000ff000000ff000000ff000000ff005d5dfe00f3f3ff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ddddff002727ff000000ff000000ff000000ff000000ff003e3eff00eaeafd00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00c7c7fd000a0aff000000ff000000ff002727ff00ddddff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00b0b0fe000404ff000a0aff00ceceff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff005d5dfe008282fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00e3e3ff00ededff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
i$unseated6        2   6  
2d2b29002d2b2b00
302f2d00312f2e00
2d2b29002d2b2b00
31302e0032302f00
2d2b29002d2b2b00
33312f0033313100
i$5d               16  35 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff005d5dfe000000ff000000ff000000ff000000ff000000ff009797fe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff002727ff000000ff000000ff000000ff000000ff000000ff009797fe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff000000ff000000ff000000ff000000ff000000ff000000ff009797fe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ceceff000000ff002727ff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00a8a8ff000000ff003e3eff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff008282fe000101ff003e3eff000a0aff000000ff005d5dfe00ededff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff005d5dfe000000ff000000ff000000ff000000ff000000ff003e3eff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff002727ff000000ff000000ff000000ff000000ff000000ff000101ff00a8a8ff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff003e3eff000404ff008282fe00ffffff00ceceff001212ff000000ff005d5dfe00fdfdff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff008282fe000000ff002727ff00fdfdff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff008282fe000000ff002727ff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ddddff005d5dfe000000ff00ceceff00ffffff00ffffff008282fe000000ff002727ff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ddddff000101ff000101ff008282fe00ffffff00ffffff005d5dfe000404ff002727ff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff001212ff000000ff002727ff00ededff00ceceff000101ff000101ff008282fe00fdfdff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff008282fe000000ff000000ff000000ff000000ff000000ff000000ff00dbdbfe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ededff002727ff000000ff000000ff000000ff000000ff008282fe00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ddddff003e3eff000101ff000000ff008282fe00ffffff00fdfdff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00a8a8ff00fbfbff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c0c0fd000000ff009797fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00eaeafd001212ff000000ff000101ff00d7d7ff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00fbfbff003e3eff000000ff000000ff000000ff002727ff00f3f3ff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00fbfbff008282fe000000ff000000ff000000ff000000ff000000ff005d5dfe00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00a8a8ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff008282fe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00d7d7ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff00b3b3ff00ffffff00ffffff00ffffff00
ffffff00ffffff00f5f5ff003e3eff000000ff000000ff000000ff000000ff000000ff000000ff000000ff002727ff00e6e6fe00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00efefff002727ff000000ff000000ff000000ff000000ff000000ff000404ff00e6e6fe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ddddff000404ff000000ff000000ff000000ff000000ff00c7c7fd00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00b9b9fc000000ff000000ff000000ff008282fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff008282fe000000ff003e3eff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00fbfbff00efefff003e3eff00e3e3ff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fdfdff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
i$Kd               16  35 
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff008282fe000101ff000000ff00ffffff00ffffff00ffffff00ffffff008282fe000101ff000000ff002727ff00ededff00ffffff00ffffff00
ffffff00ffffff008282fe000101ff000000ff00ffffff00ffffff00ffffff00ddddff000101ff000000ff000000ff00ceceff00ffffff00ffffff00ffffff00
ffffff00ffffff008282fe000000ff000000ff00ffffff00ffffff00fbfbff003e3eff000000ff000101ff008282fe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff008282fe000101ff000000ff00ffffff00ffffff008282fe000000ff000000ff003e3eff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff008282fe000404ff000000ff00ffffff00ceceff000000ff000000ff001212ff00dbdbfe00fdfdff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff008282fe000101ff000000ff00ffffff003e3eff000000ff000000ff008282fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff008282fe000000ff000000ff008282fe000000ff000000ff003e3eff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff008282fe000101ff000000ff000000ff000000ff000000ff000000ff00dbdbfe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff008282fe000404ff000000ff000000ff000000ff000000ff000000ff005d5dfe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff008282fe000101ff000000ff000000ff008282fe002727ff000000ff001212ff00ededff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff008282fe000000ff000000ff002727ff00fbfbff008282fe000000ff000101ff008282fe00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff008282fe000101ff000000ff00c7c7fd00ffffff00ededff001212ff000000ff001212ff00ededff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff008282fe000404ff000000ff00ffffff00ffffff00ffffff005d5dfe000101ff000000ff008282fe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff008282fe000101ff000000ff00ffffff00ffffff00ffffff00ceceff000000ff000000ff002727ff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff008282fe000000ff000000ff00ffffff00ffffff00ffffff00ffffff003e3eff000000ff000000ff009797fe00ffffff00ffffff00ffffff00
ffffff00ffffff008282fe000101ff000000ff00ffffff00ffffff00ffffff00ffffff009797fe000000ff000000ff002727ff00ffffff00ffffff00ffffff00
ffffff00ffffff008282fe000404ff000000ff00ffffff00ffffff00ffffff00ffffff00ffffff001212ff000000ff000000ff00bcbcff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00a8a8ff00fbfbff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00c0c0fd000000ff009797fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00eaeafd001212ff000000ff000101ff00d7d7ff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00fbfbff003e3eff000000ff000000ff000000ff002727ff00f3f3ff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00fbfbff008282fe000000ff000000ff000000ff000000ff000000ff005d5dfe00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00a8a8ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff008282fe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00d7d7ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff00b3b3ff00ffffff00ffffff00ffffff00
ffffff00ffffff00f5f5ff003e3eff000000ff000000ff000000ff000000ff000000ff000000ff000000ff002727ff00e6e6fe00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00efefff002727ff000000ff000000ff000000ff000000ff000000ff000404ff00e6e6fe00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ddddff000404ff000000ff000000ff000000ff000000ff00c7c7fd00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00b9b9fc000000ff000000ff000000ff008282fe00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff008282fe000000ff003e3eff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00fbfbff00efefff003e3eff00e3e3ff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00
ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00fdfdff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00ffffff00

