.osdb2

// OpenScrape 5.0.3

// 2014-08-13 17:32:32
// 32 bits per pixel

//
// sizes
//

z$clientsize       480  360

//
// strings
//

s$activemethod              2
s$allinmethod               0
s$nchairs                   6
s$network                   iPoker
s$sitename                  ipoker
s$swagconfirmationmethod    Click Bet
s$swagdeletionmethod        Delete
s$swagtextmethod            2
s$t0type                    0.0
s$t1type                    0.0
s$t2type                    0.0
s$t3type                    fuzzy
s$titletext                 Hold'em
s$ttlimits                  ^*, ^L - ^s/^b
s$ttlimits0                 ^*, ^L - �^s/�^b
s$ttlimits1                 ^*, ^L - �^s/�^b

//
// regions
//

r$CHAR               144  28 151  36 ffffffff    0 T0
r$c0cardface0nocard  152 170 152 173 ff2f861e    3 C
r$c0cardface0rank    154 130 170 150 fff1f1f1  100 T3
r$c0cardface0suit    174 151 184 173 ffffffff  100 T3
r$c0cardface1nocard  188 170 188 173 ff308e1f    3 C
r$c0cardface1rank    190 130 206 150 ffffffff  100 T3
r$c0cardface1suit    210 151 220 173 ffffffff  100 T3
r$c0cardface2nocard  223 170 223 173 ff32961f    3 C
r$c0cardface2rank    225 130 241 150 ffffffff  100 T3
r$c0cardface2suit    245 151 255 173 ffffffff  100 T3
r$c0cardface3nocard  259 170 259 173 ff30921e    3 C
r$c0cardface3rank    261 130 277 150 ffffffff  100 T3
r$c0cardface3suit    280 151 290 173 ffffffff  100 T3
r$c0cardface4nocard  294 170 294 173 ff308b1e    3 C
r$c0cardface4rank    296 130 312 150 ffffffff  100 T3
r$c0cardface4suit    316 151 326 173 ffffffff  100 T3
r$c0handnumber        61  27 119  35 ffffffff    0 T0
r$c0pot0             210 196 254 202 fff0f0f0    0 T1
r$i0button           259 319 320 340        0    0 N
r$i0label            276 326 302 335 ffffffff    0 T2
r$i0state            264 343 264 343 ff5c5c5d    1 C
r$i1button           330 319 391 340        0    0 N
r$i1label            344 326 376 336 ffffffff    0 T2
r$i1state            389 343 389 343 ff5c5c5d    1 C
r$i2button           400 319 461 340        0    0 N
r$i2label            408 320 440 329 ffffffff    0 T2
r$i2state            460 343 460 343 ff5c5c5d    1 C
r$i3button           400 319 461 340        0    0 N
r$i3edit             457 304 459 306        0    0 N
r$i3label            257 300 259 302        0    0 H0
r$i3state            462 299 462 299 ffffffff    0 C
r$i4button           342 349 345 354        0    0 N
r$i4label            343 353 345 354 ffdddddd   -1 H0
r$i4state            343 353 345 353 ffdddddd    1 C
r$i5button           263 321 266 324        0    0 N
r$i5label            263 322 265 323        0    0 H0
r$i5state            270 326 270 327 ffffffff    0 C
r$i6button           362 302 372 315        0    0 N
r$i6label            366 308 371 310        0    0 H0
r$i6state            368 309 369 309 ffebebf0    1 C
r$p0active           309  28 309  28 ff3c3326   -1 C
r$p0balance          286  39 325  45 ffffffff    0 T1
r$p0bet              324 114 363 120 fff0f0f0    0 T1
r$p0cardback         321  79 321  79 ff205815   -1 C
r$p0cardface0nocard  319  79 319  80 ff1f5714    5 C
r$p0cardface0rank    337  42 351  58 ffffffff  100 T3
r$p0cardface0suit    349  58 361  77 ffffffff  100 T3
r$p0cardface1nocard  322  79 322  80 ff215a15    5 C
r$p0cardface1rank    364  42 378  58 ffffffff  100 T3
r$p0cardface1suit    377  58 389  77 ffffffff  100 T3
r$p0dealer           353  90 353  90 ff26661a   -1 C
r$p0name             277  27 331  36 ffffffff    0 T0
r$p0seated           330  28 330  28 ff2f261b   -1 C
r$p1active           412 179 412 179 ff3f372d   -1 C
r$p1balance          417 183 456 189 fff0f0f0    0 T1
r$p1bet              351 177 390 183 fff0f0f0    0 T1
r$p1cardback         395 148 395 148 ff1f5814   -1 C
r$p1cardface0nocard  383 148 383 149 ff29731b    5 C
r$p1cardface0rank    408 129 422 145 ffffffff  100 T3
r$p1cardface0suit    422 146 434 165 ffffffff  100 T3
r$p1cardface1nocard  385 148 385 149 ff28711b    5 C
r$p1cardface1rank    436 129 450 145 ffffffff  100 T3
r$p1cardface1suit    450 146 462 165 ffffffff  100 T3
r$p1dealer           388 175 388 175 ff236418   -1 C
r$p1name             408 171 462 180 ffffffff    0 T0
r$p1seated           461 171 461 171 ff443b31   -1 C
r$p2active           281 240 281 240 ff141414   -1 C
r$p2balance          287 250 326 256 fff0f0f0    0 T1
r$p2bet              308 209 347 215 fff0f0f0    0 T1
r$p2cardback         362 216 362 216 ff215916   -1 C
r$p2cardface0nocard  335 231 335 232 fff7f7f7   -5 C
r$p2cardface0rank    336 224 350 240 ffffffff  100 T3
r$p2cardface0suit    348 241 360 260 ffffffff  100 T3
r$p2cardface1nocard  374 230 374 231 fff1f1f1   -5 C
r$p2cardface1rank    363 224 377 240 ffffffff  100 T3
r$p2cardface1suit    376 241 388 260 ffffffff  100 T3
r$p2dealer           321 226 321 226 ff1f5415   -1 C
r$p2name             278 238 332 247 ffffffff    0 T0
r$p2seated           331 256 331 256 ff1e1811   -1 C
r$p3active           163 245 163 245 ff14110d   -1 C
r$p3balance          155 252 194 258 fff0f0f0    0 T1
r$p3bet              129 209 168 215 fff0f0f0    0 T1
r$p3cardback         125 216 125 216 ff235f16   -1 C
r$p3cardface0nocard  113 204 113 205 ff266819    5 C
r$p3cardface0rank     89 224 103 240 ffffffff  100 T3
r$p3cardface0suit    101 241 113 260 ffffffff  100 T3
r$p3cardface1nocard  115 204 115 205 ff256818    5 C
r$p3cardface1rank    116 224 130 240 ffffffff  100 T3
r$p3cardface1suit    128 241 140 260 ffffffff  100 T3
r$p3dealer           151 226 151 226 ff1f5417   -1 C
r$p3name             145 240 199 249 ffffffff    0 T0
r$p3seated           144 258 144 258 ff28221b   -1 C
r$p4active            58 177  58 177 ff40372d   -1 C
r$p4balance           23 183  62 189 fff0f0f0    0 T1
r$p4bet               96 177 135 183 fff0f0f0    0 T1
r$p4cardback          81 148  81 148 ff194911   -3 C
r$p4cardface0nocard   83 148  83 149 ff205316    5 C
r$p4cardface0rank     16 130  30 146 ffffffff  100 T3
r$p4cardface0suit     30 146  42 165 ffffffff  100 T3
r$p4cardface1nocard   83 148  83 149 ff1b4d13    5 C
r$p4cardface1rank     44 130  58 146 ffffffff  100 T3
r$p4cardface1suit     57 146  69 165 ffffffff  100 T3
r$p4dealer            86 175  86 175 ff215715   -3 C
r$p4name              16 171  70 180 ffffffff    0 T0
r$p4seated            16 190  16 190 ff3e352b   -1 C
r$p5active           154  43 154  43 ff2f261c   -1 C
r$p5balance          153  40 192  46 fff0f0f0    0 T1
r$p5bet              126 115 165 121 fff0f0f0    0 T1
r$p5cardback         150  79 150  79 ff215914   -1 C
r$p5cardface0nocard  138  79 138  80 ff1d5114    5 C
r$p5cardface0rank     89  42 103  58 ffffffff  100 T3
r$p5cardface0suit    102  58 114  77 ffffffff  100 T3
r$p5cardface1nocard  141  79 141  80 ff205516    5 C
r$p5cardface1rank    116  42 130  58 ffffffff  100 T3
r$p5cardface1suit    129  58 141  77 ffffffff  100 T3
r$p5dealer           121  91 121  91 ff226117   -1 C
r$p5name             145  29 199  38 ffffffff    0 T0
r$p5seated           145  46 145  46 ff332a20   -1 C
r$u0active           361  68 361  68 ffffffff   -1 C
r$u0balance          286  39 325  45 ffffffff    0 T1
r$u0name             275  27 331  36 ffffffff    0 T0
r$u1active           434 157 434 157 ffffffff   -1 C
r$u1balance          417 183 456 189 ff00ffff    0 T1
r$u1name             408 171 462 180 ffffffff    0 T0
r$u2active           362 249 362 249 ffffffff   -1 C
r$u2balance          287 250 326 256 ffffffff    0 T1
r$u2name             278 238 332 247 ffffffff    0 T0
r$u3active           126 251 126 251 ffffffff   -1 C
r$u3balance          155 252 194 258 ffffffff    0 T1
r$u3name             145 240 199 249 ffffffff    0 T0
r$u4active            52 157  52 157 ffffffff   -1 C
r$u4balance           23 183  62 189 ffffffff   50 T1
r$u4name              16 171  70 180 ffffffff    0 T0
r$u5active           127  67 127  67 ffffffff   -1 C
r$u5balance          150  40 189  46 ffffffff    0 T1
r$u5name             141  29 199  38 ffffffff    0 T0

//
// fonts
//

t0$v 10 e 1 e 10
t0$z 13 15 19
t0$J 1 41 7e
t0$s 19 15 13
t0$C 1c 22 41 41 41 22
t0$O 1c 22 41 41 41 22 1c
t0$G 1c 22 41 49 49 2f
t0$w 1c 3 6 18 6 3 1c
t0$u 1e 1 1 1f
t0$n 1f 10 10 f
t0$m 1f 10 10 f 10 10 f
t0$r 1f 8 10
t0$a 2 15 15 f
t0$1 21 7f 1
t0$. 3
t0$9 30 49 49 3e
t0$S 31 49 49 49 46
t0$8 36 49 49 36
t0$q 38 44 44 7f
t0$g 38 45 45 7e
t0$t 3e 11
t0$W 3e 1 e 30 e 1 3e
t0$0 3e 41 41 3e
t0$6 3e 49 49 6
t0$Y 40 30 f 30 40
t0$y 40 39 6 38 40
t0$T 40 40 7f 40 40
t0$T 40 7f 40 40
t0$j 41 17e
t0$7 41 46 58 60
t0$3 41 49 49 36
t0$I 41 7f 41
t0$2 43 45 49 31
t0$Z 43 4d 51 61
t0$i 5f
t0$X 63 1c 1c 63
t0$V 70 c 3 3 c 70
t0$A 7 1c 64 64 1c 7
t0$5 79 49 49 46
t0$W 7c 3 1c 60 1c 3 7c
t0$U 7e 1 1 1 1 7e
t0$L 7f 1 1 1
t0$b 7f 11 11 e
t0$N 7f 20 10 8 4 7f
t0$D 7f 41 41 41 22 1c
t0$p 7f 44 44 38
t0$P 7f 44 44 44 38
t0$F 7f 48 48 48 40
t0$R 7f 48 4c 4a 31
t0$B 7f 49 49 39 6
t0$E 7f 49 49 49 41
t0$M 7f 60 18 6 18 60 7f
t0$K 7f 8 14 22 41
t0$H 7f 8 8 8 8 7f
t0$f 7f 90 90
t0$4 c 14 24 7f
t0$4 c 14 24 7f 4
t0$c e 11 11
t0$o e 11 11 e
t0$d e 11 11 ff
t0$e e 15 15 d
t0$l ff
t0$h ff 10 10 f
t0$b ff 11 11 e
t0$k ff 4 a 11
t1$. 1
t1$$ 11 29 7f 25 22
t1$1 21 7f 1
t1$. 3
t1$9 30 49 49 3e
t1$8 36 49 49 36
t1$0 3e 41 41 3e
t1$6 3e 49 49 6
t1$$ 3e 55 55 55
t1$7 41 46 58 60
t1$3 41 49 49 36
t1$2 43 45 49 31
t1$$ 4 a 1f 9 8
t1$5 79 49 49 46
t1$$ 9 3f 49 41
t1$4 c 14 24 7f 4
t1$0 f 10 10 f
t2$s 19 1d 17 13
t2$C 1c 3e 63 41 41 63 22
t2$a 2 17 15 1f f
t2$t 3e 3f 11
t2$i 5f 5f
t2$F 7f 7f 48 48 48 40
t2$B 7f 7f 49 79 3f 6
t2$R 7f 7f 4c 4e 7b 31
t2$c e 1f 11 11
t2$o e 1f 11 1f e
t2$d e 1f 11 ff ff
t2$e e 1f 15 1d d
t2$l ff ff
t2$h ff ff 10 1f f
t2$k ff ff e 1b 11
t3$K 1001f 1001f 1001f 1e1ff 1807f 10c1f 13e1f 17f9f 1ffff 1fffe 1fffc 1fff8 1fff8 1fff8 1fff8
t3$c 102 2 c2 72 3c 10 10010 f0010 f0010 f0030 f8070 fc0e1 fffff
t3$d 18 5 3 1 c 18 30 e0 1e0 c03e0 e0fe0 f1fe0 fffe1
t3$T 1cfff 1cfff 1001f 1001f 1f1ff 1803f 1061f 13f9f 11f9f 1801e 1c07c 1fff8 1fff8 1fff8 1fff8
t3$T 1cffff 1cffff 10007f 10007f 10007f 1f07ff 1800ff 10007f 3fc7f 3fe7f 1fc7f 10007c 1801f8 1ffff0 1ffff0 1fffe0 1fffe0
t3$Q 1e07ff 1801ff 1000ff 1f87f 3fc7f 3f87f 3f07f 1f87f 10007f 18003f 1e077f 1ffffc 1ffff8 1ffff0 1ffff0 1fffe0 1fffe0
t3$Q 1e0ff 1803f 1041f 13f9f 13f9f 13e1f 10e1f 1800f 1e0df 1ffff 1fffe 1fff8 1fff0 1ffe0 1ffc0
t3$c 1e2 2 2 62 38 3c 10 30010 f0010 f0010 f0030 f8060 ff3e1
t3$c 1e f c 2000c 1e0004 1e0004 1e000c 1e000c 1f001c 1f8038 1fe1f0
t3$A 1ff9f 1fc1f 1e03f 1007f 10e7f 1007f 1e01f 1fe1f 1ffdf 1ffff 1fffe 1fff8 1fff0 1ffe0 1ffc0
t3$A 1ffe7f 1ff07f 1f807f 1c03ff 23ff f3ff 1003ff 1c00ff 1fc07f 1ff87f 1fffff 1fffff 1fffff 1ffffe 1ffffc 1ffff8 1ffff0
t3$K 1ffff 1001f 1001f 1f1ff 1c1ff 1807f 11c1f 13f1e 1ffde 1fffe 1ffff 1ffff 1ffff 1ffff 1ffff
t3$Q 1ffff 1c07f 1803f 11f1f 13f9f 13e1f 11e1f 1801e 1c04e 1fffe 1ffff 1ffff 1ffff 1ffff 1ffff
t3$2 1ffff 1cf9f 1861f 11c1f 1389f 1019f 1839f 1ffdf 1ffff 1ffff 1fffe 1fff8 1fff0 1ffe0 1ffc0
t3$6 1ffff 1f0ff 1803f 1001f 1339f 1339f 1101f 1dc7e 1fffe 1fffe 1ffff 1ffff 1ffff 1ffff 1ffff
t3$6 1ffff 1f0ff 1803f 1001f 1339f 1339f 1901f 1dc7f 1ffff 1ffff 1ffff 1fffe 1fffc 1fff8 1fff8
t3$4 1ffff 1fcff 1f87f 1e27f 18e7f 1001f 1001f 1fe7f 1ffff 1ffff 1fffe 1fff8 1fff0 1ffe0 1ffc0
t3$J 1ffff 1fe3f 1fe1f 1ff9f 1ff9f 1001f 1003f 1ffff 1ffff 1ffff 1ffff 1fffe 1fffc 1fff8 1fff8
t3$3 1ffff 1fe7f 18e1f 10f1f 1339f 1119f 1801f 1cc7e 1fffe 1fffe 1ffff 1ffff 1ffff 1ffff 1ffff
t3$A 1ffff 1ff1f 1f81f 1c07f 1067f 1067f 1807f 1f81f 1ff9f 1ffff 1ffff 1ffff 1fffe 1fffc 1fff8
t3$5 1ffff 1ff7f 1831f 1031f 1339f 1339f 1301f 1f87f 1ffff 1ffff 1ffff 1fffe 1fffc 1fff8 1fff8
t3$7 1ffff 1ffff 13fff 13c1f 1301f 103ff 10fff 13fff 1ffff 1ffff 1ffff 1fffe 1fffc 1fff0 1fff8
t3$9 1ffff 1ffff 1813f 1011f 13c9f 1399f 1001f 1c07e 1fffe 1fffe 1ffff 1ffff 1ffff 1ffff 1ffff
t3$9 1ffff 1ffff 1813f 1011f 13c9f 1399f 1801f 1c07f 1ffff 1ffff 1ffff 1fffe 1fffc 1fff8 1fff8
t3$8 1ffff 1ffff 1843f 1001f 1339f 1339f 1001f 1cc3e 1fffe 1fffe 1ffff 1ffff 1ffff 1ffff 1ffff
t3$8 1ffff 1ffff 1843f 1001f 1339f 1339f 1001f 1cc3f 1ffff 1ffff 1ffff 1fffe 1fffc 1fff8 1fff8
t3$2 1ffff 1ffff 1c71f 1061f 13c1f 1389f 1019f 1839f 1ffff 1ffff 1ffff 1ffff 1fffe 1fffc 1fff8
t3$3 1ffff 1ffff 1ce3f 18e1f 13b9f 1339f 1001f 1803e 1fffe 1fffe 1ffff 1ffff 1ffff 1ffff 1ffff
t3$3 1ffff 1ffff 1ce3f 18e1f 13b9f 1339f 1001f 1803f 1ffff 1ffff 1fffe 1fffc 1fff8 1fff0 1ffc0
t3$6 1ffff 1ffff 1e07f 1801f 1339f 1339f 1101f 1983e 1fffe 1fffe 1ffff 1ffff 1ffff 1ffff 1ffff
t3$6 1ffff 1ffff 1e07f 1801f 1339f 1339f 1101f 1983f 1ffff 1ffff 1fffe 1fffc 1fff8 1fff0 1ffc0
t3$9 1ffff 1ffff 1e7ff 1811f 1199f 13c9f 1191f 1803f 1e0ff 1fffe 1fffc 1fff8 1fff8 1fff8 1fff8
t3$5 1ffff 1ffff 1fa7f 1023f 1279f 1279f 1201f 1303f 1ffff 1ffff 1ffff 1fffe 1fffc 1fff8 1fff0
t3$5 1ffff 1ffff 1fb3f 1031f 1139f 1339f 1301f 1303e 1fffe 1fffe 1ffff 1ffff 1ffff 1ffff 1ffff
t3$5 1ffff 1ffff 1fb3f 1031f 1139f 1339f 1301f 1303f 1ffff 1ffff 1fffe 1fffc 1fff8 1fff0 1ffc0
t3$J 1ffff 1ffff 1fe3f 1fe1f 1ff9f 1001f 1001f 1007e 1fffe 1fffe 1ffff 1ffff 1ffff 1ffff 1ffff
t3$J 1ffff 1ffff 1fe3f 1fe1f 1ff9f 1001f 1001f 100ff 1ffff 1ffff 1fffe 1fffc 1fff8 1fff0 1ffc0
t3$8 1ffff 1ffff 1fe7f 1801f 1119f 1339f 1319f 1801e 1fc7e 1fffe 1ffff 1ffff 1ffff 1ffff 1ffff
t3$8 1ffff 1ffff 1fe7f 1801f 1119f 1339f 1319f 1801f 1fc7f 1ffff 1fffe 1fffc 1fff8 1fff0 1ffc0
t3$6 1fffff 1f8fff 1801ff 10007f 31c7f 33e7f 31c7e 11007e 1980fc 1ffffc 1ffffc 1ffffc 1ffffe 1ffffe 1fffff 1fffff 1fffff
t3$4 1fffff 1fe3ff 1f83ff 1f03ff 1c33ff 10e3ff 10007f 10007f 1fe3ff 1fffff 1ffffe 1ffff8 1ffff0 1ffff0 1ffff0 1fffe0 1fffe0
t3$J 1fffff 1ff1ff 1ff07f 1ff07f 1ffc7f 1ffc7f 7f ff 1003ff 1fffff 1fffff 1ffffe 1ffffc 1ffff8 1ffff0 1fffc0 1fff80
t3$5 1fffff 1ffbff 1c18ff 10187f 113c7f 133e7f 131c7f 13007f 1380ff 1fffff 1fffff 1fffff 1ffffe 1ffffe 1ffffc 1ffff0 1fffe0
t3$2 1fffff 1ffe7f 18787f 10707f 3e07f 3c47f 18c7e 101c7e 183c7e 1ffffc 1ffffc 1ffffc 1ffffe 1ffffe 1fffff 1fffff 1fffff
t3$2 1fffff 1ffe7f 18787f 10707f 3e07f 3c47f 18c7f 101c7f 183c7f 1fffff 1fffff 1ffffe 1ffffc 1ffff8 1ffff0 1fffc0 1fff80
t3$7 1fffff 1fffff 13ffff 13fe7f 13c07f 13007f 1007fe 107ffe 11fffc 1ffffc 1ffffc 1ffffc 1ffffe 1ffffe 1fffff 1fffff 1fffff
t3$8 1fffff 1fffff 1860ff 10007f 10c7f 39e7f 39e7e 10007e 1800fc 1fe1fc 1ffffc 1ffffc 1ffffe 1ffffe 1fffff 1fffff 1fffff
t3$3 1fffff 1fffff 18f0ff 10f07f 1fc7f 39e7f 31c7e 10007e 1800fc 1fe3fc 1ffffc 1ffffc 1ffffe 1ffffe 1fffff 1fffff 1fffff
t3$3 1fffff 1fffff 18f0ff 10f07f 1fc7f 39e7f 39c7f 10007f 1840ff 1ff3ff 1fffff 1ffffe 1ffffc 1ffff8 1ffff0 1fffc0 1fff80
t3$6 1fffff 1fffff 1c01ff 10007f 11087f 33e7f 31c7e 11007e 1980fc 1fe3fc 1ffffc 1ffffc 1ffffe 1ffffe 1fffff 1fffff 1fffff
t3$6 1fffff 1fffff 1c01ff 10007f 11087f 33e7f 31c7f 11007f 1980ff 1fe3ff 1fffff 1ffffe 1ffffc 1ffff8 1ffff0 1fffc0 1fff80
t3$5 1fffff 1fffff 1c18ff 10187f 101c7f 133e7f 131c7e 13007e 1380fc 1fe3fc 1ffffc 1ffffc 1ffffe 1ffffe 1fffff 1fffff 1fffff
t3$9 1fffff 1fffff 1c19ff 10087f 10047f 3c67f 3c67e 10007e 1000fc 1c03fc 1ffffc 1ffffc 1ffffe 1ffffe 1fffff 1fffff 1fffff
t3$9 1fffff 1fffff 1c19ff 10087f 1847f 3c67f 3cc7f 10007f 1800ff 1f07ff 1fffff 1ffffe 1ffffc 1ffff0 1fffe0 1fffc0 1fff00
t3$9 1fffff 1fffff 1c3dff 1808ff 108c7f 3c67f 3ce7f 10007f 1800ff 1c03ff 1fffff 1fffff 1fffff 1ffffe 1ffffc 1ffff8 1ffff0
t3$8 1fffff 1fffff 1c60ff 10007f 10c7f 39e7f 39e7f 10007f 1840ff 1fe1ff 1fffff 1ffffe 1ffffc 1ffff0 1fffe0 1fffc0 1fff00
t3$8 1fffff 1fffff 1ce1ff 1000ff 100c7f 39e7f 39e7f 100c7f 10007f 1ce1ff 1fffff 1fffff 1ffffc 1ffff8 1ffff0 1fffe0 1fff80
t3$5 1fffff 1fffff 1e18ff 10187f 101c7f 133e7f 131c7f 13007f 1380ff 1fe3ff 1fffff 1ffffe 1ffffc 1ffff8 1ffff0 1fffc0 1fff80
t3$s 3 3 43 33 19 80010 c0010 c0010 e0030 f0030 f8060 fc1e1 fffff
t3$d 3 7 c 3c 78 f0 1001f0 1807f0 1c0ff0 1f1ff0 1ffff0
t3$c 38 1f 1c 20018 1c0008 3c0008 3c0018 3c0018 3c0038 3f0070 3fc3e0
t3$s 63 3 3 63 3b 1d 80010 c0010 c0010 e0030 f0060 f80e0 fffe1
t3$h 7 6 e 1c 38 38 78 f8 801f8 e03f8 ffff8
t3$s 7 7 6 80006 c0006 c0006 e0006 f000e f001c fc03c ffff8
t3$K 7f 7f 7f 1f0fff 1c0fff 1803ff 10c0ff 1f07f 7f87f ffe7f 1fffff 1ffffe 1ffffc 1ffff8 1ffff0 1fffc0 1fff80
t3$h 8000c c0001 c0001 8000c 18 30 70 60 e0 1e0 801e0 c07e0 f0fe1
t3$h c0001 c0001 8000c 18 30 70 60 e0 1e0 801e0 c07e0 f0fe1 fffff
t3$d c 5 3 5 18 30 70 1e0 803e0 e07e0 f1fe0 fbfe1 fffff
t3$s c e f 10000c 10000c 18000c 1c000c 1e001c 1e0038 1f8078 1ff7f0

//
// points
//


//
// hash
//

h0$sitin              392181d4
h0$allin              9f031642
h0$prefold            f875bd23
h0$sitout             ffc236cb

//
// images
//

i$prefold          3   2  
ddddddffddddddffddddddff
ddddddffddddddffddddddff
i$allin            3   3  
ffffffffffffffffffffffff
ffffffffffffffffffffffff
ffffffffffffffffffffffff
i$sitin            6   3  
828282ff828282ff828282ff828282ff828282ff828282ff
7e7e7effebebf0ffebebf0ffebebf0ffebebf0ff7e7e7eff
7a7a7aff7a7a7affebebf0ffebebf0ff7a7a7aff7a7a7aff
i$sitout           3   2  
ddddddffddddddffddddddff
dcdcdcffdcdcdcffdcdcdcff

